# Visualization in Motion A Research Agenda and Two Evaluations

This is a repository for all frameworks and analysis scripts used in the TVCG paper "Visualization in Motion: A Research Agenda and Two Evaluations". To cite the paper:

* Lijie Yao, Romain Vuillemot, Anastasia Bezerianos, Petra Isenberg. Visualization in motion: A Research Agenda and Two Evaluations. IEEE Transactions on Visualization and Computer Graphics, vol. 28, no. 10, pp. 3546-3562, 1 Oct. 2022, doi: https://doi.org/10.1109/TVCG.2022.3184993.

Our recent work provided a technology probe to design and embed the **visualization in motion** in real application scenarions, please check the project at https://gitlab.inria.fr/lyao/swimflow.

## Getting started

To get started, either clone this repository, or if you stay within github, use the ''use this template'' function to generate your own version of this repository from which you can then edit. Then start a php server with the main directory of the repo as the root directory (so that index.php is at the base level), and you should be able to view the example project in your browser under an address like [localhost:8080/?debug].

Test your study design extensively. Locally, you can use a development environment such as MAMP to serve the study pages and to test whether the logging works as it should. If you use MAMP, set the server directory in the preferences to the base directory of your study, that is, the level at which you can find this read.me file. Then start the server and open the page:

* Study1 speed donut: http://localhost:8080/study1_speed_donut/index.php?PROLIFIC=False

* Study1 speed bar: http://localhost:8080/study1_speed_bar/index.php?PROLIFIC=False

* Study2 trajectory donut: http://localhost:8080/study2_trajectory_donut/index.php?PROLIFIC=False

* Study2 trajectory bar: http://localhost:8080/study2_trajectory_bar/index.php?PROLIFIC=False

 in your browser (Please check if **YOUR GATE NUMBER** is 8080 or not. If not, replace 8080 by **YOUR GATE NUMBER**). The GET parameter ?PROLIFIC=False deactivates the reload check and replaces the participant ID in the log files with the name DEBUG.

 If you use a php server, please firstly CD to the director where you can find this readme file. Then enter

 * php -S localhost:8080

 in the terminal. Same as before, change "8080" with **YOUR GATE NUMBER**. Now please enter the link mentioned above in your browser.

Attention, our experiments require a minimum screen size of **13.3-inches** (please let the experiment window be **full screen** and do not zoom in or zoom out). If your screen size cannot meet our requirements, you will get a page telling you your screen is not big enough. In that case, you will not be allowed to try our codes.

## Description
All participants' data will be recorded in "results" folder.
"results/format" records each participant's answer according to different categories such as "agreed", "answer", "duration", "excluded", "survey", and "training".
"results/individual" records all participants' answer in a single .csv file.

## Analysis Scripts
To learn how to analysis all results, please refer to the read me file in "Analysis_Scripts" folder.
