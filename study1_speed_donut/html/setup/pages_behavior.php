<?php
/*  This file contains the behavior description for all pages in the experiment. 
    To define an experiment flow, create an array variable as below for each page and then indicate the order additionally in the variable $PAGE_ORDER defined at the bottom of the file.
    The meaning of the array elements is as following:
      - id: the div id used to find the page (must be unique)
      - next: the page to be shown after the one being currently defined 
      - button: the text written in the next button at the bottom of the page
      - page: the relative url to the page being currently defined
      - disabled: boolean indicating whether the button should be disabled when the page is shown
 */

  // Pages definition
  $WELCOME_PAGE = array(
    "id"    => "welcome",
    "next"    => "consent",
    "button"  => "You can click the button after 5s",
    "page"    => "pages/newvis/welcome.php",
    "disabled"  => false
  );

  $CONSENT_PAGE = array(
    "id"    => "consent",
    "next"    => "calibration",
    "button"  => "You can click the button after 5s",
    "page"    => "pages/newvis/consent_form.php",
    "disabled"  => false
  );

  $CALIBRATION_PAGE = array(
    "id"    => "calibration",
    "next"    => "prequestionnaire",
    "button"  => "Go to Pre-questionnaire",
    "page"    => "pages/calibration/calibration_page.php",
    "disabled"  => false
  );

  $CALIBRATION_FAILED_PAGE = array(
  	"id"    => "excluded",
    "next"    => "none",
    "button"  => " ",
    "page"    => "pages/calibration/calibration_failed_page.php",
    "disabled"  => true
  );

  $ATTENTION_CHECK = array(
    "id"    => "attentioncheck",
    "next"    => "none",
    "button"  => " ",
    "page"    => "pages/newvis/attention_check.php",
    "disabled"  => true
  );

  $PRE_QUESTIONNAIRE = array(
    "id"    => "prequestionnaire",
    "next"    => "bgexplanation",
    "button"  => "Please answer the questions",
    "page"    => "pages/questionnaire/pre_questionnaire.php",
    "disabled"  => true
  );

  $BACKGROUND_EXPLANATION_PAGE = array(
    "id"    => "bgexplanation",
    "next"    => "taskexplanation",
    "button"  => "You can click the button after 5s",
    "page"    => "pages/newvis/background_explanation.php",
    "disabled"  => false
  );

  $TASK_DESCRIPTION_PAGE = array(
    "id"    => "taskexplanation",
    "next"    => "new_visualization",
    "button"  => "Go to Training",
    "page"    => "pages/newvis/task_description.php",
    "disabled"  => false
  );

  $NEW_VISUALIZATION_PAGE = array(
  	"id"    => "new_visualization",
    "next"    => "postquestionnaire",
    "button"  => " ",
    "page"    => "pages/newvis/new_visualization_page.php",
    "disabled"  => true
  );

  $POST_QUESTIONNAIRE = array(
    "id"    => "postquestionnaire",
    "next"    => "thankyou",
    "button"  => "Please answer the questions",
    "page"    => "pages/questionnaire/post_questionnaire.php",
    "disabled"  => true
  );

  $THANK_YOU = array(
    "id"    => "thankyou",
    "next"    => "none",
    "button"  => " ",
    "page"    => "pages/newvis/thank_you.php",
    "disabled"  => true
  );


  // This array defines the order of the pages. 
  // You can temporarily modify this order for debugging. Just move the page you want first at the first position.
  $PAGE_ORDER = array(   

        $WELCOME_PAGE,
        $CONSENT_PAGE,

        $CALIBRATION_PAGE,
        $CALIBRATION_FAILED_PAGE,

        $PRE_QUESTIONNAIRE,
        $BACKGROUND_EXPLANATION_PAGE, 
        $TASK_DESCRIPTION_PAGE,
        
        $NEW_VISUALIZATION_PAGE,
        $ATTENTION_CHECK,  
        $POST_QUESTIONNAIRE,
        $THANK_YOU

        );
  
 ?>
