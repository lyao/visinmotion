<?php

	$data = json_decode(file_get_contents('php://input'), true);

	$excluded_filename = "../../results/format/excluded/excluded.csv";
	$exists = file_exists($excluded_filename);
	$excluded_file = fopen($excluded_filename, "a+");

	if (!$exists){
	fwrite($excluded_file, "timestamp,participant_id, study_id,session_id, color_condition, speed_condition, browser_name,browser_version,os,Calibration_check_oneCM_value, pixel_size_check_vis_moving_need_pixel_one_way, attention_check_errors_number, reloaded_page_number");
	}

	fwrite($excluded_file,
		PHP_EOL .
		date(DateTime::ISO8601) . ',' .
		$data['participant_id'] . ',' .
		$data['study_id']       . ',' .
		$data['session_id']     . ',' .

		$data['color_condiiton']. ',' .
		$data['speed_condiiton']. ',' .

		$data['browser_name']   . ',' .
		$data["browser_version"]. ',' .
		$data["os"] . ',' .

		$data["oneCM"]. ',' .
		$data["pixel_size_check"]. ',' .

		$data["attention_check_errors_number"]. ',' .

		$data["reloaded_page_number"]
		// ($data["reloaded"] ? ",0,1" : ",1,0"); // tests whether the user reloaded or didnt pass the att. check
	);

	fclose($excluded_file);
	exit;

?>
