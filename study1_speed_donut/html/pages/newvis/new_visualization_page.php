<style type="text/css">
	/* Training part */
	#training_drawn{
		position: relative;
		text-align: center;
		margin: auto;
	}

	#canvas_0{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#training_afterImage{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#training_afterImage_1{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#training_afterImage_2{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#training_afterImage_3{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#training_blank{
		position: relative;
		text-align: center;
		margin: auto;	
	}
	#training_answer{
		position: relative;
		text-align: center;
		margin: auto;
	}
	/* ------------------------------ */

	/* Trial part */
	#trial_drawn{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#canvas_1{
		/*position: fixed;  /* Fixed: position relative to the broswer, won't move with the scrolling of mouse; Relative: position relative to the previous element;*/
		/*top: 15%; */ /* top: 15%; position at the 15% far away from the top border of broswer */
		position: relative;
		text-align: center; /* let the elements inside this div align-center */
		/*left: 0px; */ /*set left = right = X px; margin: auto; is to make the div at the center of vertical line */
		/*right: 0px; */
		margin: auto;
	}
	#trial_afterImage{
		/*position: fixed;
		top: 15%;
		left: 0px;
		right: 0px;
		margin: auto;*/
		position: relative;
		text-align: center;
		margin: auto;
	}
	#trial_afterImage_1{
		/*position: fixed;
		top: 15%;
		left: 0px;
		right: 0px;
		margin: auto;*/
		position: relative;
		text-align: center;
		margin: auto;
	}
	#trial_afterImage_2{
		/*position: fixed;
		top: 15%;
		left: 0px;
		right: 0px;
		margin: auto;*/
		position: relative;
		text-align: center;
		margin: auto;
	}
	#trial_afterImage_3{
		/*position: fixed;
		top: 15%;
		left: 0px;
		right: 0px;
		margin: auto;*/
		position: relative;
		text-align: center;
		margin: auto;
	}
	#trial_blank{
		/*position: fixed;
		top: 15%;
		left: 0px;
		right: 0px;
		margin: auto;*/
		position: relative;
		text-align: center;
		margin: auto;	
	}
	#trial_answer{
		/*position: fixed;
		width: 1100px;
		top: 30%;*/
		position: relative;
		text-align: center;
		margin: auto;
	}
	/* ------------------------------ */

	/* input = number, scrolling the mouse or click up & down are not allowed*/
	input::-webkit-outer-spin-button,input::-webkit-inner-spin-button{
        -webkit-appearance:textfield;
  	}

  	input[type="number"]{
        -moz-appearance:textfield;
	}
	/* ------------------------------ */

</style>

<div id="row">
	<div id="col">
		<!-- Training part -->
		<div class = "page" id = "training_part" style="display: block;">

			<div class = "section" id ="training_explanation" style="display: block;">
				<h2 id = "training_title_1"></h2>
				<p id = "training_first_sentence" style = "font-weight: bold;"></p>
				<ul>
					<li> <input type="checkbox" class="training_checkbox"> &nbsp;&nbsp; You will be shown the same donut chart  as before but <span id="training_bold_1" style="font-weight: bold;">the  percentage (%) in <span style="color: #E90738; font-weight: bold;">red</span> will vary each time</span>.</li>
					<li>  <input type="checkbox" class="training_checkbox"> &nbsp;&nbsp; In this training, you will see the donut chart <span id = "training_speed_discription" style="font-weight: bold;"></span> on the screen, it will <span id="training_bold_2" style="font-weight: bold;">display only 1 time</span>.</li>
					<li>  <input type="checkbox" class="training_checkbox"> &nbsp;&nbsp; Only during training - each time you submit an answer we will tell you what the exact answer would have been. It is ok if you did not get the answer exactly correct, but you should be somewhere close. </li>
					<li>  <input type="checkbox" class="training_checkbox"> &nbsp;&nbsp; To pass this training, you need to get <span id="training_bold_3" style = "font-weight: bold;">at least 6 of the questions correct</span>.</li>
					<li> <input type="checkbox" class="training_checkbox"> &nbsp;&nbsp; Again, <span id="training_bold_4" style = "font-weight: bold;">make a quick but good guess of the right answer</span>, but do not try to measure or calculate correct answers. We want to learn what numbers you intuitively see.</li>
				</ul>
			</div>

			<div class = "section" id = "training_button_1" style="display: none;">
				<p style = "font-weight: bold;"> If you are ready, please click the button below: </p>
				<button id = "start_training_button" style="background-color: #006400; color: #FFFFFF;" onclick = "startTraining(this)"></button>	
			</div>

			<div class = "section" id = "training_stimuli" style="display: none;">
				<h2 id = "training_title_2"></h2>
				<div clas = "section" id = "training_drawn">
					<canvas id = "canvas_0"></canvas>
					<canvas id = "training_blank" style="display: none; background-color: #FFFFFF"></canvas>
					<canvas id = "training_afterImage" style="display: none; background-color: #FFFFFF"></canvas>
					<canvas id = "training_afterImage_1" style="display: none; background-color: #FFFFFF"></canvas>
					<canvas id = "training_afterImage_2" style="display: none; background-color: #FFFFFF"></canvas>
					<canvas id = "training_afterImage_3" style="display: none; background-color: #FFFFFF"></canvas>
				</div>
				<div class = "section" id = "training_answer">
					<p>Tell us at what percentage (%) the <span style="color: #E90738; font-weight: bold;">red</span> slice had:</p>
					<input type="number" id="training_answer_given" onmousewheel="return false;" onkeyup="value=value.replace(/[^\d]+/g,'')" placeholder="Please enter your answer here" step="1" min="0" max="100" style="width:250px; text-align: center" required autofocus oninput="checkDoneButton(this.value, true)">
					
					<!-- <input type="button" id="training_done_button" value="Please give an answer" disabled="disabled" onclick="nextTraining(this)"> -->
					<input type="button" id="training_done_button" value="Please give an answer" disabled="disabled" data-toggle="modal" data-target="#training_answer_check" onclick="checkAnswer(this)">
				</div>
		
				<!-- ------------------------------------ -->

				<!-- Check answer module -->
				<div class="modal fade" id="training_answer_check" tabindex="-1" role="dialog" aria-labelledby="training_answer_check_label" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title" id="training_answer_check_label">Answer Feedback</h4>
							</div>

							<div class="modal-body">
								<p id = "answer_feedback"></p>
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-success" data-dismiss="modal" id="training_answer_check_button" onclick="nextTraining(this)">Close and Continue Training</button>
							</div>
								
						</div>
					</div>
				</div>
				<!-- ------------------------------------ -->

				<div class = "pages" id = "training_button_2" style="display: none; border: solid; border-color: red; border-width: 2px; height: 115px;margin-top: 20px; margin-bottom: 20px; padding-top: 5px;">
					<ul>
						<li> If you want to do <span style="font-weight: bold;">more training</span>, you can <span style="font-weight: bold;">stay on this page</span> and keep training until you are ready to go into the experiment. </li>
						<li style = "font-weight: bold;"> If you are ready to start the experiment, please click the button below: </li>
					</ul>
					<button id = "start_experiment_button" style="background-color: #006400; color: #FFFFFF; margin-right: 20px;" onclick = "startExperiment(this)"></button>
				</div>
			</div>
				
		</div>
		
		<!-- ------------------------------------------------------------------------------------------------------------------------------------------------- -->

		<!-- Stimuli part -->
        <!-- The width of #trial_stimuli = #trial_drawn = #trial_answer -->
        <!-- #trial_stimuli is align-left, #trial_drawn and #trial_answer are align-center -->
		<div class = "page" id = "trial_stimuli" style="display: none;">
			<h2 id = "experiment_title"></h2>
			<!-- The height of #trial_drawn is 1.5 mutiples higher than the height of .canvas, to avoid the tiny scale offset -->
			<div class = "section" id = "trial_drawn">
				<canvas id = "canvas_1"></canvas>
				<canvas id = "trial_blank" style="display: none; background-color: #FFFFFF"></canvas>
				<canvas id = "trial_afterImage" style="display: none; background-color: #FFFFFF"></canvas>
				<canvas id = "trial_afterImage_1" style="display: none; background-color: #FFFFFF"></canvas>
				<canvas id = "trial_afterImage_2" style="display: none; background-color: #FFFFFF"></canvas>
				<canvas id = "trial_afterImage_3" style="display: none; background-color: #FFFFFF"></canvas>
			</div>
			<div class = "section" id = "trial_answer">
				<p>Tell us at what percentage (%) the <span style="color: #E90738; font-weight: bold;">red</span> slice had:</p>
					<input type="number" id="trial_answer_given" onmousewheel="return false;" onkeyup="value=value.replace(/[^\d]+/g,'')" placeholder="Please enter your answer here" step="1" min="0" max="100" style="width:250px; text-align: center" required autofocus oninput="checkDoneButton(this.value, false)">
					
					<input type="button" id="trial_done_button" value="Please give an answer" disabled="disabled" onclick="nextTrial(this)">
			</div>					
		</div>
		<!-- ------------------------------------------------------------------------------------------------------------------------------------------------- -->

		<!-- Confidence survey part -->
		<div class = "page" id = "confidence_survey" style="display: none;">
			<h2 id = "confidence_survey_title"></h2>
			<div class = "section" id = "confidence_survey_discription">
				<p>You have completed the <span id = "confidence_survey_count"></span> set of trial. Please rate your confidence in the correctness of your responses for the last block of answers.</p>
				<p>How confidence are you in the correctness of your responses?</p>

				<table>
					<thead>
						<tr>
							<th></th>
							<th class="likert">1</th>
							<th class="likert">2</th>
							<th class="likert">3</th>
							<th class="likert">4</th>
							<th class="likert">5</th>
							<th></th>
						</tr>			
					</thead>
					<tbody>
						<tr>
							<td>Not at all confident&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<td class="likert">
								<input type="radio" name="condifence_survey_radio" value="1" class="condifence_survey_radio validates-required validates">
							</td>
							<td class="likert">
								<input type="radio" name="condifence_survey_radio" value="2" class="condifence_survey_radio validates-required validates">
							</td>
							<td class="likert">
								<input type="radio" name="condifence_survey_radio" value="3" class="condifence_survey_radio validates-required validates">
							</td>
							<td class="likert">
								<input type="radio" name="condifence_survey_radio" value="4" class="condifence_survey_radio validates-required validates">
							</td>
							<td class="likert">
								<input type="radio" name="condifence_survey_radio" value="5" class="condifence_survey_radio validates-required validates">
							</td>
							<td>&nbsp;&nbsp;Very confident</td>
						</tr>					
					</tbody>				
				</table>
			</div>

			<div class = "section" id = "confidence_survey_button" style="display: none;">
				<button id = "submit_confidence_survey_button" style="background-color: #006400; color: #FFFFFF" onclick = "submitConfidenceSurvey(this)"></button>
			</div>
			
		</div>
		<!-- ------------------------------------------------------------------------------------------------------------------------------------------------- -->

		<!-- Vision focus part -->
		<div class = "page" id = "vision_focus" style="display: none; position: relative; top: 78px; width: 1100px; text-align: center;">
			<div id = "vision_focus_box_parent" style="text-align: center; width: 1100px;">
				<div id = "vision_focus_box_son" style="border: solid; background: #FFFFFF; color: #000000; text-align: center;">
					<p style="margin: 0px;">The donut will display here only one time within a brief amount of time. Please pay attention and let your vision focus on this area.</p>
				</div>
			</div>

			<input type="button" id="vision_focus_button" disabled="disabled" value = "You can click the button after 5s" style="background-color: #EDEDED; color: #A3A3A3;">
		</div>
		<!-- ------------------------------------------------------------------------------------------------------------------------------------------------- -->

	</div>
</div>

<script type="text/javascript">

//disable the next button for the moment
document.addEventListener("DOMContentLoaded", function(){
                                                  			let nextbutton = document.getElementById("btn_<?php echo $id;?>");
                                                  			nextbutton.style.display = 'none';
                                                		});

//Vision part variables 
var waitTime_vis = 4;
var btn_vis = false;
var training_trial_count = 0;

// vision_focus_button can be clicked after X second(s)
var callback_id_vis = setInterval(function(){
  if(btn_vis){
                  if(waitTime_vis == 0){
                    let nextButton = document.getElementById("vision_focus_button");
                    nextButton.style.background = "#006400";
                    nextButton.style.color = "#FFFFFF";
                    nextButton.value = "I'm ready."
                    nextButton.disabled = false;
                  }else{
                  	let nextButton = document.getElementById("vision_focus_button");
                    nextButton.value = "You can click the button after "+ waitTime_vis +"s";
                    waitTime_vis--;
                  }
  }
}, 1000);

// When click the vision_focus_button
$(document).on('click','#vision_focus_button',function(){
	// console.log("click one vision_focus_button");
	// Clean the setInterval
    clearInterval(callback_id_vis);
    // btn_vis = false
    btn_vis = false;

    // Start Training or trial
    if(training_trial_count == 1 || training_trial_count == 3 || training_trial_count == 5){
    // For Training 
    	document.getElementById("vision_focus").style.display = 'none';
    	document.getElementById("training_part").style.display = 'block';

    	/* Prepare the training stimuli and call for animation */
		training_percentage = randomNumber(0, 100, original_percentage_array);
		// console.log("training_percentage  = " + training_percentage);
		
		/* Call for animation */
		lastFinishedTime = initializationTime = Date.now();
		callback_id = requestAnimationFrame(function (){timeBasedAnimation(myCanvas_training, context_training, random_speed_array[current_training_turn], training_percentage)});
	// For trial
    }else if(training_trial_count == 2 || training_trial_count == 4 || training_trial_count == 6){
		// Reset waitDuration
		waitDuration = 200;
		// Clear the previous draw
		context_trial.clearRect(0,0,myCanvas_trial.width,myCanvas_trial.height);
		// Reset canvas
		resetCanvasDisplay(myCanvas_trial, myCanvas_blank, myCanvas_afterimage, myCanvas_afterimage_1, myCanvas_afterimage_2, myCanvas_afterimage_3);

    	// For trial
    	document.getElementById("vision_focus").style.display = 'none';
    	document.getElementById("trial_stimuli").style.display = 'block';

    	/* Prepare the trial stimuli and call for animation */
		start_answer_time = lastFinishedTime = initializationTime = Date.now();
		// console.log("start_answer_time: " + start_answer_time);
		callback_id = requestAnimationFrame(function (){timeBasedAnimation(myCanvas_trial, context_trial, random_speed_array[current_part], random_percentage_array[current_repetition][current_trial_per_repetition])}); 	
    }    
});


$(".training_checkbox").click(function(){
	// console.log("click one training_checkbox");
	let len = $(".training_checkbox:checked").length;
	// console.log("len = " + len);
	if(len==5){
		document.getElementById("training_button_1").style.display = "block";
	}else{
		document.getElementById("training_button_1").style.display = "none";
	}
})

function cleanTrainingCheckbox(){
	let x = document.getElementsByClassName("training_checkbox");
	for(let i=0; i<x.length; i++){
		if(x[1].checked){
			x[1].checked = false;
		}
	}
	// Disable the start training button
	document.getElementById("training_button_1").style.display = "none";
}

/* Canvas, Frame independent, and Time based Animation */
// This funciton based on requestAnimationFrame() API, which gives hand to the broswer to update the content when frame refreshes, inside of setting the framerate in the code(in this case, sometimes the real framerate cannot achieve to the setting value because of the equipement). And then it cut the time interval by 1ms, which means the minmum step considered in this function is 1ms.
// canvas, ctx are parameters indicate sketching on which canvas
// speed is how fast the visualization will move at, in CM/S
// percentage is how much the percentage is of the target slice

function timeBasedAnimation(canvas, ctx, speed, percentage){

	currentTime = Date.now();

	waitDeltaTime =  currentTime - initializationTime;
	deltaTime = currentTime - lastFinishedTime;

	// Clear the previous draw
	ctx.clearRect(0,0,canvas.width,canvas.height);

	if(waitDeltaTime < waitDuration){
		lastFinishedTime = Date.now();
		callback_id = requestAnimationFrame(function (){timeBasedAnimation(canvas, ctx, speed, percentage)});
	}else{

	
	// Redraw with the new coordinates
	donutGroup.draw(ctx, breakpointValue(percentage, start_point), speed);
	
	// Get the current time
	// currentTime = Date.now();
	// Calculate the delta time (ms) between last time call and present time call
	// deltaTime is used to move the dount
	// deltaTime = currentTime - lastFinishedTime;

	// For moving case, that the x coordianate will be changed according to the time, at the end of a round trip, Blink() fires, and then the moving direction and x coordinate will be reset
	// The fast motion case, when the donut chart touches the far left border, go back 
	if(speed == 30){
		// Set the moving flag, this parameter is used to identify the donut chart is moving or is static
		movingFlag = true;
		// Set the x coordinate and the moving direction according to the different speeds
		if ( donutGroup.direction == 1 ){
		    // (speed) is X cm/s, (speed*oneCM) is X pixel/s, (speed*oneCM/1000) is X pixel/ms, 
		    // (speed*oneCM/1000*frequencyDelta) is the displacement ( = X pixel) during 1 ms 
		    // (speed*oneCM/1000*frequencyDelta*deltaTime) is the displacement ( = X*deltaTime pixel) during one frame
		    // The duration of one frame = deltaTime = currentTime - lastFinishedTime
		    donutGroup.x += speed*oneCM/1000*frequencyDelta*deltaTime;
		}else if ( donutGroup.direction == 0 ){
		    donutGroup.x -= speed*oneCM/1000*frequencyDelta*deltaTime;
		}   	

		// Set the right border to change the moving direction
		if ( donutGroup.x > (vis_travel_distance_pixel_needed - circleOuterRadius)){
		    donutGroup.x = (vis_travel_distance_pixel_needed - circleOuterRadius);
		    donutGroup.direction = 0;
		}

		// When the stimuli is in process of moving, callback this function itself according to the broswer's frame refresh
		if(donutGroup.x > circleOuterRadius && donutGroup.x <= (vis_travel_distance_pixel_needed - circleOuterRadius)){
			// Refresh the lastFinishedTime, because for the moving case, the time is cut into micro step based on 1ms, the donut chart moves on for a certain distance according to this deltaTime.
			
			    lastFinishedTime = Date.now();
			    callback_id = requestAnimationFrame(function (){timeBasedAnimation(canvas, ctx, speed, percentage)});
			
		//When the stimuli returns back to the left border, disabled the canvas_0/canavs_1, enable the afterimage block
		}else if(donutGroup.x <= circleOuterRadius){
			// console.log("waitDuration = " + waitDuration);
			// Record the display duration
			display_millisecond = Date.now() - initializationTime - waitDuration;
			// console.log("display_millisecond = " + display_millisecond);

		    if(display_millisecond >= 1600){
		    	// Clear the previous draw
				ctx.clearRect(0,0,canvas.width,canvas.height);
			    // For trial case
			    if(canvas == document.getElementById("canvas_1")){
			    	document.getElementById("canvas_1").style.display = 'none';
					document.getElementById("trial_afterImage").style.display = 'block';			
				// For training case
				}else if(canvas == document.getElementById("canvas_0")){
					document.getElementById("canvas_0").style.display = 'none';
				    document.getElementById("training_afterImage").style.display = 'block';
				}
				// Afterimage blocks
				Blink(canvas, 4, 20);
			}
			// Reset the x coordinate and direction
			donutGroup.x = circleOuterRadius;
			donutGroup.direction = 1;

			if(display_millisecond < 1600){
				// Clear the previous draw
				ctx.clearRect(0,0,canvas.width,canvas.height);
				start_answer_time = lastFinishedTime = initializationTime = Date.now();
				// console.log("start_answer_time: " + start_answer_time);
			    callback_id = requestAnimationFrame(function (){timeBasedAnimation(canvas, ctx, speed, percentage)});
			}
			
		}
	// The slow motion case, when the donut chart touches the middle left border, go back 
	}else if(speed == 15){
		// Set the moving flag, this parameter is used to identify the donut chart is moving or is static
		movingFlag = true;
		// Start point, end point
		let start_x = vis_travel_distance_pixel_needed/4 + circleOuterDiameter/4, stop_x = 3*vis_travel_distance_pixel_needed/4 - circleOuterDiameter/4;
		// Set the x coordinate and the moving direction according to the different speeds
		if ( donutGroup.direction == 1 ){
		    donutGroup.x_slow_motion += speed*oneCM/1000*frequencyDelta*deltaTime;
		}else if ( donutGroup.direction == 0 ){
		    donutGroup.x_slow_motion -= speed*oneCM/1000*frequencyDelta*deltaTime;
		}

		// Set the border to change the moving direction
		if ( donutGroup.x_slow_motion > stop_x){
		    donutGroup.x_slow_motion = stop_x;
		    donutGroup.direction = 0;
		}

		// When the stimuli is in process of moving, callback this function itself according to the broswer's frame refresh
		if( donutGroup.x_slow_motion > start_x && donutGroup.x_slow_motion <= stop_x ){
			// Refresh the lastFinishedTime, because for the moving case, the time is cut into micro step based on 1ms, the donut chart moves on for a certain distance according to this deltaTime.
			
			lastFinishedTime = Date.now();
			callback_id = requestAnimationFrame(function (){timeBasedAnimation(canvas, ctx, speed, percentage)});
			
		//When the stimuli returns back to the left border, disabled the canvas_0/canavs_1, enable the afterimage block
		}else if(donutGroup.x_slow_motion <= start_x ){
			// console.log("waitDuration = " + waitDuration);
			// Record the display duration
			display_millisecond = Date.now() - initializationTime - waitDuration;
			// console.log("display_millisecond = " + display_millisecond);

		    if(display_millisecond >= 1600){
		    	// Clear the previous draw
				ctx.clearRect(0,0,canvas.width,canvas.height);
			    // For trial case
			    if(canvas == document.getElementById("canvas_1")){
				    document.getElementById("canvas_1").style.display = 'none';
				    document.getElementById("trial_afterImage").style.display = 'block';
				// For training case
				}else if(canvas == document.getElementById("canvas_0")){
					document.getElementById("canvas_0").style.display = 'none';
				    document.getElementById("training_afterImage").style.display = 'block';
				}
				// Afterimage blocks
				Blink(canvas, 4, 20);
			}
		
			// Reset the x coordinate and direction
			donutGroup.x_slow_motion = start_x;
			donutGroup.direction = 1;

			if(display_millisecond < 1600){
				// Clear the previous draw
				ctx.clearRect(0,0,canvas.width,canvas.height);
				start_answer_time = lastFinishedTime = initializationTime = Date.now();
				// console.log("start_answer_time: " + start_answer_time);
			    callback_id = requestAnimationFrame(function (){timeBasedAnimation(canvas, ctx, speed, percentage)});
			}
			
		}


	// For static case, the donut chart stays in the center of canvas for X ms (X == static_displaying_time_1), and then afterimage block fires
	}else if(speed == 0){
		movingFlag = false;
		let dlt = deltaTime - waitDuration;
		if(dlt < static_displaying_time_1){
			// Do not refresh the lastFinishedTime, the role of deltaTime here is a timer, to count for static_displaying_time_1 ms.
			
				callback_id = requestAnimationFrame(function (){timeBasedAnimation(canvas, ctx, speed, percentage)});
			
		}else if(dlt >= static_displaying_time_1){
				// console.log("waitDuration = " + waitDuration);
				// Record the display duration
				display_millisecond = dlt;
				// console.log("display_millisecond = " + display_millisecond);

			if(display_millisecond >= 1600){
				// Clear the previous draw
				ctx.clearRect(0,0,canvas.width,canvas.height);

				// For trial case
			    if(canvas == document.getElementById("canvas_1")){
				    document.getElementById("canvas_1").style.display = 'none';
				    document.getElementById("trial_afterImage").style.display = 'block';
				// For training case
				}else if(canvas == document.getElementById("canvas_0")){
					document.getElementById("canvas_0").style.display = 'none';
				    document.getElementById("training_afterImage").style.display = 'block';
				}
				// Afterimage blocks
				Blink(canvas, 4, 20);
			}

			if(display_millisecond < 1600){
				// Clear the previous draw
				ctx.clearRect(0,0,canvas.width,canvas.height);
				
				start_answer_time = lastFinishedTime = initializationTime = Date.now();
				// console.log("start_answer_time: " + start_answer_time);
			    callback_id = requestAnimationFrame(function (){timeBasedAnimation(canvas, ctx, speed, percentage)});
			}			
		}
	}
	}		
}


/* After image blink function */
// Once the stimuli go from left to right, and then return back from right to left, reach the far left border, the stimuli's canvas will disappear, the afterimage will appear during a very short time, perhaps only 0.5s, and repeat for several times.
// canvas is you want to apply this function on which canvas
// blink_times is how many times you want to repeat the afterimage
// interval_ms is how long time you want to let the afterimage appear for one time, in MS
function Blink(canvas, blink_times, interval_ms){
	// console.log("Blink fires");
	// For trial
	if(canvas == document.getElementById("canvas_1")){
		// After the stimuli display finishing, let blank canvas display for 1s
		// timeout_id[0] = setTimeout(function(){document.getElementById("trial_blank").style.display = 'none';document.getElementById("trial_afterImage").style.display = 'block';}, 1000);
		
		// Dispalay the 4 afterimages one by one, each one stays for interval_ms
		for(let i=1;i<=blink_times; i++){
			if(i == 1){
				timeout_id[i] = setTimeout(function(){document.getElementById("trial_afterImage").style.display = 'none';document.getElementById("trial_afterImage_" + i).style.display = 'block';}, interval_ms*i);
			}else if(i == blink_times){
				timeout_id[i] = setTimeout(function(){document.getElementById("trial_afterImage_" + (i-1) ).style.display = 'none';document.getElementById("trial_blank").style.display = 'block'; checkDoneButtonFire = true; activeDoneButton(false);}, interval_ms*i);
			}else{
				timeout_id[i] = setTimeout(function(){document.getElementById("trial_afterImage_" + (i-1) ).style.display = 'none';document.getElementById("trial_afterImage_" + i).style.display = 'block';}, interval_ms*i);
			}
		}
	// For training
	}else if(canvas == document.getElementById("canvas_0")){
		// After the stimuli display finishing, let blank canvas display for 1s
		// timeout_id[0] = setTimeout(function(){document.getElementById("training_blank").style.display = 'none';document.getElementById("training_afterImage").style.display = 'block';}, 1000);
		// Dispalay the 4 afterimages one by one, each one stays for interval_ms
		for(let i=1;i<=blink_times; i++){
			if(i == 1){
				timeout_id[i] = setTimeout(function(){document.getElementById("training_afterImage").style.display = 'none';document.getElementById("training_afterImage_" + i).style.display = 'block';}, interval_ms*i);
			}else if(i == blink_times){
				timeout_id[i] = setTimeout(function(){document.getElementById("training_afterImage_" + (i-1) ).style.display = 'none';document.getElementById("training_blank").style.display = 'block'; checkDoneButtonFire = true; activeDoneButton(true);}, interval_ms*i);
			}else{
				timeout_id[i] = setTimeout(function(){document.getElementById("training_afterImage_" + (i-1)).style.display = 'none';document.getElementById("training_afterImage_" + i).style.display = 'block';}, interval_ms*i);
			}
		}
	}
}

/* Let the canvas go back to the initial status,
 * For the stimuli: style="display: block;"
 * For the afterimage: style="display: none;"
 * For the blank: style="display: none" */
// stimuli = document.getElementById(""); 
// afterimage = document.getElementById(""); 
// blank = document.getElementById(""); 
function resetCanvasDisplay(stimuli, blank, afterimage1, afterimage2, afterimage3, afterimage4){
	stimuli.style.display = 'block';
	blank.style.display = 'none';
	afterimage1.style.display = 'none';
	afterimage2.style.display = 'none';
	afterimage3.style.display = 'none';
	afterimage4.style.display = 'none';
}

/* Let the Done button disabled, and the answer text area clean*/
// btn is the button that you want to reset as disabled
// disabled button background color is light grey, text color is dark grey
// btn = document.getElementById("")
// ata is the answer text area that you want to clean
// ata = document.getElementById("")
function resetDoneButtonAnswerArea(btn,ata){
	btn.style.background = "#EDEDED";
	btn.style.color = "#A3A3A3";
	btn.value = "Please give an answer";
	btn.disabled = true;
	ata.value = '';
}

/* Initialize the training stimuli, 
 * firstly let the explanation and the button to start training disappear,
 * secondly let the training stimuli displays, 
 * thirdly count the current training times, 
 * lastly prepare the afterimage block and the stimuli block according to the speed */
function startTraining(button){
	waitDuration = 1000;
	// Clean the checked checkbox
	cleanTrainingCheckbox();
	// Clean the text area
	document.getElementById("training_answer_given").value = '';
	// Let the bold words become normal
	for(let i=1; i<=4; i++){
		document.getElementById("training_bold_" + i).style.fontWeight = 'normal';
	}

	// Cancel the current animation
	cancelAnimationFrame(callback_id);
	// clean all the setTimeout()
	for(let i=0; i<=4; i++){
		clearTimeout(timeout_id[i]);
	}

	// Disable the training explanation part and the button ifself
	document.getElementById("training_explanation").style.display = 'none';
	// Enable the training stimuli part
	document.getElementById("training_stimuli").style.display = 'block';

	// Clear the previous draw
	context_training.clearRect(0,0,myCanvas_training.width,myCanvas_training.height);
	// Reset canvas
	resetCanvasDisplay(myCanvas_training, myCanvas_training_blank, myCanvas_training_afterimage, myCanvas_training_afterimage_1, myCanvas_training_afterimage_2, myCanvas_training_afterimage_3);
	// Reset the donut positiion
	donutGroup.x = circleOuterRadius;
	donutGroup.x_static = vis_travel_distance_pixel_needed/2 + circleOuterDiameter/4;
	donutGroup.x_slow_motion = vis_travel_distance_pixel_needed/4;
	donutGroup.direction = 1;

	// Initialize the training parameters
	if(current_training_turn == undefined){
		current_training_turn = 0;
	}else{
		current_training_turn++;
	}

	// Rewrite the training/trial/confidence survey title, speed discription, and button text according to the turn
	if(current_training_turn == 0){
		// Part 1
		document.getElementById("training_title_2").innerHTML = "Training: Part 1";
		document.getElementById("start_experiment_button").innerHTML = "Start Experiment Part 1";
		document.getElementById("experiment_title").innerHTML = "Experiment: Part 1";
		document.getElementById("confidence_survey_title").innerHTML = "Confidence Survey: Part 1";
		document.getElementById("confidence_survey_count").innerHTML = "first";
		document.getElementById("submit_confidence_survey_button").innerHTML = "Submit Confidence Survey Part 1";

		document.getElementById("training_title_1").innerHTML = "Training: Part 2";
		document.getElementById("training_first_sentence").innerHTML = "Now it’s time for you to do the second training, confirm you read each line by clicking the checkbox:";
		if(random_speed_array[1] == original_speed_array[0]){
	      document.getElementById("training_speed_discription").innerHTML = "staying static";
	    }else if(random_speed_array[1] == original_speed_array[1]){
	      document.getElementById("training_speed_discription").innerHTML = "moving at a low speed";
	    }else if(random_speed_array[1] == original_speed_array[2]){
	      document.getElementById("training_speed_discription").innerHTML = "moving at a high speed";
	    }
    	document.getElementById("start_training_button").innerHTML = "Start Training Part 2";
	}else if(current_training_turn == 1){
		// Part 2
		document.getElementById("training_title_2").innerHTML = "Training: Part 2";
		document.getElementById("start_experiment_button").innerHTML = "Start Experiment Part 2";
		document.getElementById("experiment_title").innerHTML = "Experiment: Part 2";
		document.getElementById("confidence_survey_title").innerHTML = "Confidence Survey: Part 2";
		document.getElementById("confidence_survey_count").innerHTML = "second";
		document.getElementById("submit_confidence_survey_button").innerHTML = "Submit Confidence Survey Part 2";

		document.getElementById("training_title_1").innerHTML = "Training: Part 3";
		document.getElementById("training_first_sentence").innerHTML = "Now it’s time for you to do the last training, confirm you read each line by clicking the checkbox:";
		if(random_speed_array[2] == original_speed_array[0]){
	      document.getElementById("training_speed_discription").innerHTML = "staying static";
	    }else if(random_speed_array[2] == original_speed_array[1]){
	      document.getElementById("training_speed_discription").innerHTML = "moving at a low speed";
	    }else if(random_speed_array[2] == original_speed_array[2]){
	      document.getElementById("training_speed_discription").innerHTML = "moving at a high speed";
	    }
    	document.getElementById("start_training_button").innerHTML = "Start Training Part 3";
	}else if(current_training_turn == 2){
		// Part 3
		document.getElementById("training_title_2").innerHTML = "Training: Part 3";
		document.getElementById("start_experiment_button").innerHTML = "Start Experiment Part 3";
		document.getElementById("experiment_title").innerHTML = "Experiment: Part 3";
		document.getElementById("confidence_survey_title").innerHTML = "Confidence Survey: Part 3";
		document.getElementById("confidence_survey_count").innerHTML = "third";
		document.getElementById("submit_confidence_survey_button").innerHTML = "Submit Confidence Survey Part 3";
	}

	// /* Prepare the training stimuli and call for animation */
	// training_percentage = randomNumber(0, 100, original_percentage_array);
	// console.log("training_percentage  = " + training_percentage);

	// /* Call for animation */
	// lastFinishedTime = initializationTime = Date.now();
	// callback_id = requestAnimationFrame(function (){timeBasedAnimation(myCanvas_training, context_training, random_speed_array[current_training_turn], training_percentage)});
	
	btn_vis = true;
	training_trial_count++;
	document.getElementById("training_part").style.display = 'none';
	document.getElementById("vision_focus").style.display = 'block';
}

/* Check if the text area has a reasonable answer at the end of stimuli displaying, if the answer is positif, let the Done button disabled. 
 * This function is in case that the subject input an answer during the stimuli displaying process
 * During the displaying process, the Done button is always disabled, no metter there is or there isn't a given answer
 * It changes the button color from light grey to dark green.
 * If the case is training, 0 and 100 are considered, 
 * if the case is trial, 0 or 100 cannot be accepted as reasonable answer */
// val is the value from the answer text area
// flag is to identify the case: true -> training, false -> trial
function activeDoneButton(flag){
	// console.log("activeDoneButton fires");
	// For training case, 100 can be accepted ==> According to the feedback, let the subject enter whatever they want, so the 0 should be accepted.
	if(flag == true){
		let btn = document.getElementById("training_done_button");
		// The input answer is a string, parses it into int, decimal
		let val = parseInt(document.getElementById("training_answer_given").value, 10);
	  	if(val<0 || val>100){
		  	btn.style.background = "#EDEDED";
			btn.style.color = "#A3A3A3";
			btn.value = "Please give an appropriate answer";
		  	btn.disabled = true;
	  	}else if(val>=0 && val<=100){
		  	btn.style.background = "#006400";
		  	btn.style.color = "#FFFFFF";
		  	btn.value = "Done and Check Answer";
		    btn.disabled = false;
		    // console.log("the answer is 1: " + val);
	  	}else{
	  		btn.style.background = "#EDEDED";
			btn.style.color = "#A3A3A3";		
		  	btn.disabled = true;
	  	}
	// For trial case, 0 can be accepted
	}else if(flag == false){
		let btn = document.getElementById("trial_done_button");
		let val = parseInt(document.getElementById("trial_answer_given").value, 10);
	  	if(val<0 || val>=100){
		  	btn.style.background = "#EDEDED";
			btn.style.color = "#A3A3A3";
			btn.value = "Please give an appropriate answer";
		  	btn.disabled = true;
	  	}else if(val>=0 && val<100){
		  	btn.style.background = "#006400";
		  	btn.style.color = "#FFFFFF";
		  	if(current_trial == 20 || current_trial == 41 || current_trial == 62){
			  	btn.value = "Done and Go to Confidence Survey";
			}else{
			  	btn.value = "Done and Go to Next Trial";
			}
		    btn.disabled = false;
		    // console.log("the answer is 1: " + val);
	  	}else{
	  		btn.style.background = "#EDEDED";
			btn.style.color = "#A3A3A3";
		  	btn.disabled = true;
	  	}
	}
}

function checkDoneButton(value, flag){
	// console.log("checkDoneButton fires");

	// For training case
	if(flag){
		let btn = document.getElementById("training_done_button");
		// The input answer is a string, parses it into int, decimal
		let val = parseInt(document.getElementById("training_answer_given").value, 10);
		// console.log("val = " + val);

		// The subject cannot click on the Done button untill the end of stimuli displaying	
		if(checkDoneButtonFire){
			if(val>=0 && val<=100){
			  	btn.style.background = "#006400";
			  	btn.style.color = "#FFFFFF";
			  	btn.value = "Done and Check Answer";
			    btn.disabled = false;
			    // console.log("the answer is 2: " + val);
	  		}else{
	  			btn.style.background = "#EDEDED";
				btn.style.color = "#A3A3A3";
				btn.value = "Please give an appropriate answer";
			  	btn.disabled = true;
	  		}
		}else{
			btn.style.background = "#EDEDED";
			btn.style.color = "#A3A3A3";
			btn.value = "Please wait for activation";
			btn.disabled = true;
		}
	// For trial case
	}else if(!flag){
		// Start the timer to record the answer time taken, if the first time to enter a letter in the textarea
		// Initial trial_answered = false; !trial_answered = true;
		if(!trial_answered){
			start_type_time = Date.now();
			type_milliseconds = start_type_time - start_answer_time;
			// console.log("start_type_time = " + start_type_time);
			// console.log("type_milliseconds = " + type_milliseconds);
			// trial_answered = true means the subject starts to give answer
			trial_answered = true;
		}

		let btn = document.getElementById("trial_done_button");
		let val = parseInt(document.getElementById("trial_answer_given").value, 10);
		// console.log("val = " + val);

		// The subject cannot click on the Done button untill the end of stimuli displaying		
		if(checkDoneButtonFire){
			if(val>=0 && val<100){
			  	btn.style.background = "#006400";
			  	btn.style.color = "#FFFFFF";
			  	if(current_trial == 20 || current_trial == 41 || current_trial == 62){
			  		btn.value = "Done and Go to Confidence Survey";
			  	}else{
			  		btn.value = "Done and Go to Next Trial";
			  	}
			    btn.disabled = false;
			    // console.log("the answer is 2: " + val);
	  		}else{
	  			btn.style.background = "#EDEDED";
				btn.style.color = "#A3A3A3";
				btn.value = "Please give an appropriate answer";
			  	btn.disabled = true;
	  		}
		}else{
			btn.style.background = "#EDEDED";
			btn.style.color = "#A3A3A3";
			btn.value = "Please wait for activation";
			btn.disabled = true;
		}
	}
}

function checkAnswer(button){
	// Cancel the current animation
	cancelAnimationFrame(callback_id);
	// clean all the setTimeout()
	for(let i=0; i<=4; i++){
		clearTimeout(timeout_id[i]);
	}

	// Record the subject answer
	let ans = parseInt(document.getElementById("training_answer_given").value, 10);
	// console.log("training_answer_given = " + ans);
	// The correct answer
	let cans = parseInt(training_percentage*100, 10).toFixed();
	// console.log("exact answer = " + cans);
	// The acceptable minimum and maximum
	let min = cans - 10;
	let max = min + 20;
	// If the minimum is <=0 (there is no 0 in training case), min = 1 
	if(min < 0){ min = 0;}
	// If the maximum is >0 (there is 100 in training case), max = 100 
	if(max > 100){max = 100;}

	/* Compare with the acceptble answer range */
	// Answer can be accepted
	if(ans >= min && ans <= max){
		// Initial the count for training times
		if(current_training_times_per_turn == undefined){
			current_training_times_per_turn = 1;
		}else{
			current_training_times_per_turn++;
		}
		// console.log("current_training_times_per_turn = " + current_training_times_per_turn);
		// Insert the feedback into modal
		document.getElementById("answer_feedback").innerHTML = "Your answer " + ans + "%" + " is accepted." + "<br>" +
															   "The exact answer is: " + cans + "%." + "<br>" + 
															   "Current number of passes: " + current_training_times_per_turn +".";

		if(current_training_times_per_turn >= training_times_per_turn){
			document.getElementById("training_answer_check_button").innerHTML = "Close";
		}else{
			document.getElementById("training_answer_check_button").innerHTML = "Close and Continue Training";
		}
		
	// Answer cannot be accepted
	}else{
		// console.log("current_training_times_per_turn = " + current_training_times_per_turn);
		// Insert the feedback into modal
		document.getElementById("answer_feedback").innerHTML = "Your answer is: " + ans + "%." + "<br>" + 																								   "The exact answer is: " + cans + "%." + "<br>" +
															   "The reasonable range is between " + min + "-" + max + "%." + "<br>" +
															   "Please try again.";
	}

	// Open the modal (pop-up dialogue)
	$('#training_answer_check').modal();
	
	// Reset the Done button and clear the answer text area
	resetDoneButtonAnswerArea(document.getElementById("training_done_button"), document.getElementById("training_answer_given"));
	// Reset flag
	checkDoneButtonFire = false;
	// clean all the setTimeout()
	for(let i=0; i<=4; i++){
		clearTimeout(timeout_id[i]);
	}
	
	// Reset the donut positiion
	donutGroup.x = circleOuterRadius;
	donutGroup.x_static = vis_travel_distance_pixel_needed/2 + circleOuterDiameter/4;
	donutGroup.x_slow_motion = vis_travel_distance_pixel_needed/4;
}

/* Reset the Done button and clean the text area, 
 * prepare the stimuli for the next training*/
function nextTraining(button){
	// // Reset the Done button and clear the answer text area
	// resetDoneButtonAnswerArea(document.getElementById("training_done_button"), document.getElementById("training_answer_given"));
	// // Reset flag
	// checkDoneButtonFire = false;
	// // In case that the subject clicks on the Done button before the end of Blink() -> after images block
	// // clean all the setTimeout()
	// for(let i=0; i<=4; i++){
	// 	clearTimeout(timeout_id[i]);
	// }
	
	// Clear the previous draw
	context_training.clearRect(0,0,myCanvas_training.width,myCanvas_training.height);
	// Reset canvas 
	resetCanvasDisplay(myCanvas_training, myCanvas_training_blank, myCanvas_training_afterimage, myCanvas_training_afterimage_1, myCanvas_training_afterimage_2, myCanvas_training_afterimage_3);
	// // Reset the donut positiion
	// donutGroup.x = circleOuterRadius;
	// donutGroup.x_static = vis_travel_distance_pixel_needed/2 + circleOuterDiameter/4;
	// donutGroup.x_slow_motion = vis_travel_distance_pixel_needed/4;

	
	// // Initial the count for training times
	// if(current_training_times_per_turn == undefined){
	// 	current_training_times_per_turn = 1;
	// }else{
	// 	current_training_times_per_turn++;
	// }

	// If the training times ahieves the threshold
	if(current_training_times_per_turn >= training_times_per_turn){
		// Enable the start experiment button
		document.getElementById("training_button_2").style.display = 'block';
	}

	/* Prepare the training stimuli */
	training_percentage = randomNumber(0, 100, original_percentage_array);
	// console.log("training_percentage  = " + training_percentage);

	/* Call for animation */
	lastFinishedTime = initializationTime = Date.now();
	callback_id = requestAnimationFrame(function (){timeBasedAnimation(myCanvas_training, context_training, random_speed_array[current_training_turn], training_percentage)});
	
}

function startExperiment(button){
	console.log("start experiment");

	// Cancel the current animation
	cancelAnimationFrame(callback_id);
	// clean all the setTimeout()
	for(let i=0; i<=4; i++){
		clearTimeout(timeout_id[i]);
	}
	
	// Reset the training_part as initial
	// Disable the training stimuli part and the start experiment button
	document.getElementById("training_stimuli").style.display = 'none';
	document.getElementById("training_button_2").style.display = 'none';
		
	// clean the training times count
	current_training_times_per_turn = undefined;
	// Enable the training explanation part and the start training button
	document.getElementById("training_explanation").style.display = 'block';
	
	
	// Disable the whole training part
	document.getElementById("training_part").style.display = 'none';
	// Enable the whole trial part
	document.getElementById("trial_stimuli").style.display = 'block';

	// Reset flag
	checkDoneButtonFire = false;

	// Initialize the part
	if(current_part == undefined){
		current_part = 0;
	}else{
		current_part++;
	}

	if(current_part == (threshold_part-1) ){
		endTrialPart = true;
	}

	// Initialize the trial
	if(current_trial == undefined){
		current_trial = 0;
	}

	// Initialize the repetition
	if(current_repetition == undefined && current_trial_per_repetition == undefined){
		current_repetition = 0;
		current_trial_per_repetition =0;
	}

	// Cancel the current animation
	cancelAnimationFrame(callback_id);
	// clean all the setTimeout()
	for(let i=0; i<=4; i++){
		clearTimeout(timeout_id[i]);
	}

	// Reset the donut positiion
	donutGroup.x = circleOuterRadius;
	donutGroup.x_static = vis_travel_distance_pixel_needed/2 + circleOuterDiameter/4;
	donutGroup.x_slow_motion = vis_travel_distance_pixel_needed/4;
	// Reset waitDuration
	waitDuration = 200;
	// Clear the previous draw
	context_trial.clearRect(0,0,myCanvas_trial.width,myCanvas_trial.height);
	// Reset canvas
	resetCanvasDisplay(myCanvas_trial, myCanvas_blank, myCanvas_afterimage, myCanvas_afterimage_1, myCanvas_afterimage_2, myCanvas_afterimage_3);

	// /* Prepare the trial stimuli and call for animation */
	// lastFinishedTime = initializationTime = Date.now();
	// callback_id = requestAnimationFrame(function (){timeBasedAnimation(myCanvas_trial, context_trial, random_speed_array[current_part], random_percentage_array[current_repetition][current_trial_per_repetition])});
	
	btn_vis = true;
	training_trial_count++;
	document.getElementById("trial_stimuli").style.display = 'none';
	document.getElementById("vision_focus").style.display = 'block';
}

function nextTrial(button){

	// console.log("current_failed_times = " + current_failed_times);
	/* Record time taken and answer given */
	measurements['time_taken_type_ms'] = type_milliseconds;
    // console.log("time_taken_type_ms: " + type_milliseconds);
	// Record the time taken
	end_answer_time = Date.now();
	// console.log("end_answer_time: " + end_answer_time);
	submit_milliseconds = end_answer_time - start_type_time;
	// Push answer time taken into dataflow
    measurements['time_taken_submit_ms'] = submit_milliseconds;
    // console.log("time_taken_submit_ms: " + submit_milliseconds);

    // Reset the answered flag
    trial_answered = false;
    
    //Get given answer
  	trial_given_answer = $('#trial_answer_given').val();
  	/* ---------------------------------------------------------------------------------------------------------------------------------------*/

  	/* Reset done button and text area, clean afterimage blocks and reset canvas */
  	// Reset the Done button and clear the answer text area
	resetDoneButtonAnswerArea(document.getElementById("trial_done_button"), document.getElementById("trial_answer_given"));
	// In case that the subject clicks on the Done button before the end of Blink() -> after images block
	// Cancel the current animation
	cancelAnimationFrame(callback_id);
	// clean all the setTimeout()
	for(let i=0; i<=4; i++){
		clearTimeout(timeout_id[i]);
	}
	// Clear the previous draw
	context_trial.clearRect(0,0,myCanvas_trial.width,myCanvas_trial.height);
	// Reset canvas
	resetCanvasDisplay(myCanvas_trial, myCanvas_blank, myCanvas_afterimage, myCanvas_afterimage_1, myCanvas_afterimage_2, myCanvas_afterimage_3);
	// Reset flag
	checkDoneButtonFire = false;

	// Reset the donut positiion
	donutGroup.x = circleOuterRadius;
	donutGroup.x_static = vis_travel_distance_pixel_needed/2 + circleOuterDiameter/4;
	donutGroup.x_slow_motion = vis_travel_distance_pixel_needed/4;
	/* ---------------------------------------------------------------------------------------------------------------------------------------*/

	/* Verify attention check */
	// When the current trial is attention check (exact answer = 0)
	if(random_percentage_array[current_repetition][current_trial_per_repetition] == 0){
		// The acceptable range of given answer is from 0% to 10%
		if(trial_given_answer >= 0 && trial_given_answer <= 10){
			console.log("Attention check passed");
		}else{
			current_failed_times++;
			measurements['attention_check_errors_number'] = current_failed_times;
			console.log("current_failed_times = " + current_failed_times);
		}
	}

	// When the failed times up to the threshold, failed times >=6
	if(current_failed_times > attentioncheck_threshold){
		// Kick out the participant
		excluded = true;
		// Block the participant at the page "Attention check failed page"
		$('#new_visualization').hide();
		$('#attentioncheck').show();
		$(":button").hide();
		// Add info in Ajax
  		measurements['excluded'] = excluded;

  		$.ajax({
	        url: 'ajax/excluded.php',
	        type: 'POST',
	        data: JSON.stringify(measurements),
	        contentType: 'application/json',
      	});
	}
	/* ---------------------------------------------------------------------------------------------------------------------------------------*/

	/* Push parameters into ajax and submit answers */
	measurements['color_condiiton'] = condition_color;
	measurements['speed_condiiton'] = random_speed_array[0] + "- " + random_speed_array[1] + "- " + random_speed_array[2];

	measurements['trial_number'] = current_trial + 1;
	measurements['current_experiment_part'] = current_part + 1;
	measurements['current_repetition_per_part'] = current_repetition + 1;
	measurements['current_trial_per_repetition'] = current_trial_per_repetition + 1;

	measurements['oneCM'] = oneCM;
	measurements['soccerball_diameter_cm'] = soccerballDiameter_cm;
	measurements['soccerball_diameter_pixel'] = soccerballDiameter;
	measurements['donutchart_innerradius_cm'] = circleInnerRadius_cm;
	measurements['donutchart_innerradius_pixel'] = circleInnerRadius;
	measurements['donutchart_outerradius_cm'] = circleOuterRadius_cm;
	measurements['donutchart_outerradius_pixel'] = circleOuterRadius;

	if(movingFlag){
		if(random_speed_array[current_part] == 30){
			measurements['vis_travel_distance_cm'] = vis_travel_distance_cm;
			measurements['vis_travel_distance_pixel'] = vis_travel_distance_pixel_needed - circleOuterDiameter;
		}else if(random_speed_array[current_part] == 15){
			measurements['vis_travel_distance_cm'] = vis_travel_distance_cm/2;
			measurements['vis_travel_distance_pixel'] = (vis_travel_distance_pixel_needed - circleOuterDiameter)/2;
		}
	}else if(!movingFlag){
		measurements['vis_travel_distance_cm'] = 0;
		measurements['vis_travel_distance_pixel'] = 0;
	}
	measurements['vis_display_duration'] = display_millisecond;
  	
	measurements['speed_duration'] = random_speed_array_string[current_part];
	measurements['current_speed_cm_s'] = random_speed_array[current_part];

	measurements['correct_answer'] = random_percentage_array[current_repetition][current_trial_per_repetition] * 100;
	measurements['given_answer'] = trial_given_answer;

	$.ajax({
      url: 'ajax/results_format.php',
      type: 'POST',
      data: JSON.stringify(measurements),
      contentType: 'application/json',
      success: function (data) {
      // console.log(measurements);
      }
  	});	

	if(excluded){
  		$('#new_visualization').hide().promise().done(() => $('#attentioncheck').show());
  		$(":button").hide();
  	}
	/* ---------------------------------------------------------------------------------------------------------------------------------------*/

	/* Count balance */
	if(current_trial == (threshold_trial-1) ){
		endExperiment = true;
	}
	if( (current_repetition == (threshold_repetitions-1)) && (current_trial_per_repetition == (threshold_trial_per_repetition-1)) ){
		endTrialRepetition = true;
	}
	if(!endTrialRepetition){
		if( current_trial_per_repetition == (threshold_trial_per_repetition-1) ){
			endTrialPerRepetition = true;
		}
		if(endTrialPerRepetition){
			current_repetition++;
			current_trial_per_repetition = 0;
			endTrialPerRepetition = false;
		}else{
			current_trial_per_repetition++;
		}
	}
	// Trial count self plus one
	if(!endExperiment){
		current_trial++;
	}
	/* ---------------------------------------------------------------------------------------------------------------------------------------*/

	/* Prepare the next trial afterimage blocks and stimuli */
	if(!endTrialRepetition){
		// Prepare the trial stimuli and call for animation
		start_answer_time = lastFinishedTime = initializationTime = Date.now();
		// console.log("start_answer_time: " + start_answer_time);
		callback_id = requestAnimationFrame(function (){timeBasedAnimation(myCanvas_trial, context_trial, random_speed_array[current_part], random_percentage_array[current_repetition][current_trial_per_repetition])});
	}else if(endTrialRepetition && !endExperiment){
		// Disable the experiment div, enable the confidence survey
		document.getElementById("trial_stimuli").style.display = 'none';
		document.getElementById("confidence_survey").style.display = 'block';
		
		// Reset the endTrialPart flag
		endTrialRepetition = false;
		// Reset current_repetition and the current_trial_per_repetition
		current_repetition = undefined;
		current_trial_per_repetition = undefined;
		
	}else if(endTrialRepetition && endExperiment){
		console.log("end experiment");
		// Disable the experiment div, enable the confidence survey
		document.getElementById("trial_stimuli").style.display = 'none';
		document.getElementById("confidence_survey").style.display = 'block';
	}
	/* ---------------------------------------------------------------------------------------------------------------------------------------*/	
}

function checkConfidenceSurveyRadio(radio){
	confidence_survey_answered = true;
	confidence_survey_answer = radio.value;
	if(confidence_survey_answered){
		document.getElementById("confidence_survey_button").style.display = 'block';
	}else{
		document.getElementById("confidence_survey_button").style.display = 'none';
	}
}

$('.condifence_survey_radio').on('input', function() {
    confidence_survey_answered = true;
    confidence_survey_answer = $(this).val();
    if (confidence_survey_answered) {
    	document.getElementById("confidence_survey_button").style.display = 'block';
	}else{
		document.getElementById("confidence_survey_button").style.display = 'none';
	}
});

function cleanConfidenceSurveyRadio(){
	let x = document.getElementsByClassName("condifence_survey_radio");
	for(let i=0; i<x.length; i++){
		if(x[i].checked){
			x[i].checked = false;
		}
	}
	confidence_survey_answer = -1;
	confidence_survey_answered = false;
	document.getElementById("confidence_survey_button").style.display = 'none';
}

function submitConfidenceSurvey(button){
	// Submit the confidence survery answer
	if(current_part == 0){
		measurements['confidence_survey_answer_1'] = confidence_survey_answer;
	}else if(current_part == 1){
		measurements['confidence_survey_answer_2'] = confidence_survey_answer;
	}else if(current_part == 2){
		measurements['confidence_survey_answer_3'] = confidence_survey_answer;
	}
	$.ajax({
      url: 'ajax/questionnaire_confidence.php',
      type: 'POST',
      data: JSON.stringify(measurements),
      contentType: 'application/json',
      success: function (data) {
      	// console.log(measurements);
      }
  	});

	// Reset the radio button
	cleanConfidenceSurveyRadio();

	// If the experiment is still going on
  	if(!endTrialPart){
  		document.getElementById("confidence_survey").style.display = 'none';
  		document.getElementById("training_part").style.display = 'block';
  	// If the whole process finished, go into post_questionnaire.php
  	}else if(endTrialPart){
  		document.getElementById("confidence_survey").style.display = 'none';
  		$('#new_visualization').hide();
  		$('#postquestionnaire').show();
  	}
}

</script>