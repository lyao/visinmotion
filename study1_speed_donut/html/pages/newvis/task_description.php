<style type="text/css">
	#td_drawn{
		position: relative;
		text-align: center;
		margin: auto;
	}

	#td_canvas2{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#td_blank{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#td_afterimage1{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#td_afterimage2{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#td_afterimage3{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#td_afterimage4{
		position: relative;
		text-align: center;
		margin: auto;
	}
</style>

<div id="row">
	<div id="col">
		<h2>Task Description</h2>

		<div class = "section">
			<p>Here is an explanation of the types of questions you will have to answer in the study.</p>
		</div>

		<div class = "section" id = "td_div_see" style="display: block;">
			<b>What you will see:</b><br>

			<div id = "td_div_1" style="display: block;">
				<ul id = "td_div_see_li_1" style="display: block;">
					<li>You will always see <span style = "font-weight: bold;">a donut chart composed of two slices</span>, one slice is in <span style = "font-weight: bold; color: #E90738;">red</span>, and the other slice in <span style = "font-weight: bold; color: #C3C1C1;">grey</span>. For simplicity, there will be no soccer ball in the middle and no labels around the donut.</li>
				</ul>
				<ul id = "td_div_see_li_2" style="display: none;">
					<li>You will see <span style = "font-weight: bold;">different percentages (%)</span> for <span style = "font-weight: bold; color: #E90738;">your team</span> (in <span style = "font-weight: bold; color: #E90738;">red</span>) in the experiment. For example, here are some of these:</li>
				</ul>

				<div id = "td_div" style="width: 1100px; text-align: center; display: none;">
					<canvas id = "td_canvas1"></canvas>
					<div style="width: 1100px; position: relative; margin-bottom: 20px; display: inline-flex;">
						<div id = "td_lab_1" style = "width: 220px; text-align: center;">10% for <span style = "font-weight: bold; color: #E90738;">red</span></div>
						<div id = "td_lab_2" style = "width: 220px; text-align: center;">30% for <span style = "font-weight: bold; color: #E90738;">red</span></div>
						<div id = "td_lab_3" style = "width: 220px; text-align: center;">50% for <span style = "font-weight: bold; color: #E90738;">red</span></div>
						<div id = "td_lab_4" style = "width: 220px; text-align: center;">70% for <span style = "font-weight: bold; color: #E90738;">red</span></div>
						<div id = "td_lab_5" style = "width: 220px; text-align: center;">90% for <span style = "font-weight: bold; color: #E90738;">red</span></div>
					</div>
				</div>
			</div>								

			<div id = "td_div_2" style="display: none;">
				<div id = "td_li_1" style="display: none;">
					<ul id = "td_div_see_li_3" style="display: none;">
						<li>In the experiment we will only show you the donut for <span style = "font-weight: bold;">a brief amount of time</span>.</li>
					</ul>
					<ul id = "td_div_see_li_4" style="display: none;">
						<li>Sometimes the donut chart will be <span id = "td_div_2_speed" style = "font-weight: bold;"></span> on the screen, like the one below (in this explanation, the donut will be displayed in a loop, but in the experiment, it will be displayed only 1 time):</li>
					</ul>
				</div>

				<div id = "td_li_2" style="display: none;">
					<ul>
						<li>After the donut chart disappears you will see a very brief set of 4 images flashing on the screen. These 4 images we call them <span style = "font-weight: bold;">"masking image"</span>. The purpose of the masking images is to make sure that you see the donut only for a specific amount of time. You <span style = "font-weight: bold;">do not need to pay attention to them or try to see them clearly</span>. They are very fast on purpose. The masking images look like this (in this explanation, the masking images will be displayed in a loop, but in the experiment, they will be displayed only 1 time):</li>
					</ul>
				</div>

				<div id = "td_li_3" style="display: none;">
					<ul>
						<li>When the donut chart will be <span id = "td_div_2_speed_2" style = "font-weight: bold;"></span> on the screen, together it will look like this (in this explanation, the combination will be displayed in a loop, but in the experiment, it will be displayed only 1 time):</li>
					</ul>
				</div>

			</div>
		</div>

		<div class = "section" id = "td_div_do" style="display: none;">
			<b>What we want you to do:</b><br>

			<ul id = "td_div_do_li_1" style="display: none">
				<li>We want you to tell us <span style = "font-weight: bold;">what percentage (%)</span> <span style = "font-weight: bold; color: #E90738;">your team</span> (in <span style = "font-weight: bold; color: #E90738;">red</span>) had the ball. For example, you might estimate that <span style = "font-weight: bold; color: #E90738;">your team</span> (in <span style = "font-weight: bold; color: #E90738;">red</span>) had the ball ⅓ of the time. Since ⅓ is almost 33%, you can say <span style = "font-weight: bold; color: #E90738;">your team</span> (in <span style = "font-weight: bold; color: #E90738;">red</span>) had the ball for 33% of the time. If you notice that the red slice looks a bit bigger than 33% you might give an answer of 40%.</li>
			</ul>
			<ul id = "td_div_do_li_2" style="display: none">
				<li>Although the exact answer here is 33% we just want you to make a <span style = "font-weight: bold;">quick estimate</span>. Reasonable answers here might range between 23-43%.</li>
			</ul>
			<ul id = "td_div_do_li_3" style="display: none;">
				<li>Enter your answer as <span style = "font-weight: bold;">a whole number</span> (for example: answer 40 but not 40.5)  in the text field and then click the “Done” button.</li>
			</ul>
		</div>
		<br>

		<div class = "section" id = "td_div_interface" style="display: none;">
			<div id = "td_drawn" style="display: none;">
				<canvas id = "td_canvas2" style="display: none;"></canvas>
				<canvas id = "td_blank" style="display: none;"></canvas>
				
				<div id = "td_afterimage1" style="display: none;">
					<img src="../html/img/gif/afterimage.gif" id = "td_img_a">
				</div>
				
				<div id = "td_afterimage2" style="display: none;">
					<img src="../html/img/gif/static_afterimage.gif" id = "td_img_sa">
				</div>
				
				<div id = "td_afterimage3" style="display: none;">
					<img src="../html/img/gif/low_speed_afterimage.gif" id = "td_img_la">
				</div>
				
				<div id = "td_afterimage4" style="display: none;">
					<img src="../html/img/gif/high_speed_afterimage.gif" id = "td_img_ha">
				</div>
			</div>
		</div>

		<div class = "section" id = "td_div_interface_img" style = "display: none;">
			<p style="font-weight: bold;">What the interface screenshot looks like:</p>
			<div style="text-align: center; width: 1100px;">
				<img id = "interface_screenshot" src="../html/img/png/interface_screenshot.png" width = "400" height="167">

				<!-- when subject trys to click the interface -->
				<div class="modal fade" id="interface_screenshot_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
			
							<div class="modal-body" style="text-align: left;">
								<p>Hey, this is just a screenshot of our interface.</p>
								<p>This screenshot is to let you know what our interface looks like.</p>
								<p>You do not need to enter something or click the button on this screenshot.</p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-success" data-dismiss="modal">Close</button>			
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal -->
				</div>

			</div>		
		</div>

		<div class = "section" id = "td_btn" style="display: block;">
			<button onclick="nextDiv(this)">I understand</button>
		</div>

		<div class = "section" id = "td_endarea" style = "display: none; margin-top: 50px;">
			<p style="font-weight: bold;">Please click the button below to proceed to the training: </p>
		</div>

	</div>
</div>

<script type="text/javascript">
// If the subject click on the interface screenshot
document.getElementById("interface_screenshot").addEventListener("click", function(){$('#interface_screenshot_modal').modal();})
// Record the "I understand" button pressed times
var i_understand = -1;

// Record the callback_id
var callback_id_td = new Array();

// Parameters to record the canvas and context
var canvas2_td, context2_td;
// After image part, composed by 5 canvas, 4 free-drawn, 1 blank
var canvas2_td_blank, context2_td_blank;
var canvas2_td_afterimage_1;
var canvas2_td_afterimage_2;
var canvas2_td_afterimage_3;
var canvas2_td_afterimage_4;

// Disable the next button for the moment, set the attributes
document.addEventListener("DOMContentLoaded", function(){
	let nextButton = document.getElementById("btn_<?php echo $id;?>");
	nextButton.style.border = "none";
	nextButton.style.background = "#006400";
	nextButton.style.color = "#FFFFFF";
	nextButton.style.display = 'none';
});

// Parameter to record the time
var initializationTime_td, lastFinishedTime_td, currentTime_td, deltaTime_td, oneLoopTime_1_td, oneLoopTime_2_td, donutStopTime;
// Flag
var donut_stop = false;

function static(canvas, ctx, speed, percentage){
	// Get the current time
	currentTime_td = Date.now();
	// Calculate the delta time (ms) between last time call and present time call
	deltaTime_td = currentTime_td - lastFinishedTime_td;

	if(deltaTime_td > 0 && deltaTime_td < 1600){
		// Clear the previous draw
		ctx.clearRect(0,0,canvas.width,canvas.height);
		// Redraw with the new coordinates
		donutGroup.draw(ctx, breakpointValue(percentage, start_point), speed);
				
		// It's time to display the afterimage or the blank
	}else if(deltaTime_td >= 1600 && deltaTime_td < 2600){
		// Clear the previous draw
		ctx.clearRect(0,0,canvas.width,canvas.height);
	}else{
		lastFinishedTime_td = Date.now();
	}

	callback_id_td[0] = requestAnimationFrame(function (){static(canvas, ctx, speed, percentage)});
}

function lowSpeed(canvas, ctx, speed, percentage){
	// Get the current time
	currentTime_td = Date.now();
	// Calculate the delta time (ms) between last time call and present time call
	deltaTime_td = currentTime_td - lastFinishedTime_td;
	oneLoopTime_1_td = currentTime_td - initializationTime_td;
	
	// Clear the previous draw
	ctx.clearRect(0,0,canvas.width,canvas.height);
	// Redraw with the new coordinates
	donutGroup.draw(ctx, breakpointValue(percentage, start_point), speed);

	if ( donutGroup.direction == 1 ){
		donutGroup.x_slow_motion += speed*oneCM/1000*frequencyDelta*deltaTime_td;
	}else if ( donutGroup.direction == 0 ){
		donutGroup.x_slow_motion -= speed*oneCM/1000*frequencyDelta*deltaTime_td;
	} 

	// Set the right border to change the moving direction
	if ( donutGroup.x_slow_motion > (3*vis_travel_distance_pixel_needed/4 - circleOuterDiameter/4)){
		donutGroup.x_slow_motion = (3*vis_travel_distance_pixel_needed/4 - circleOuterDiameter/4);
		donutGroup.direction = 0;
	}else if(donutGroup.x_slow_motion <= (vis_travel_distance_pixel_needed/4 + circleOuterDiameter/4)){
		if(oneLoopTime_1_td>1600 && oneLoopTime_1_td<2600){
			// Clear the previous draw
			ctx.clearRect(0,0,canvas.width,canvas.height);
		}else if(oneLoopTime_1_td>2600){	
			donutGroup.x_slow_motion = vis_travel_distance_pixel_needed/4 + circleOuterDiameter/4;
			donutGroup.direction = 1;
			initializationTime_td = Date.now();
		}
	}
	lastFinishedTime_td = Date.now();
	callback_id_td[1] = requestAnimationFrame(function (){lowSpeed(canvas, ctx, speed, percentage)});
}

function highSpeed(canvas, ctx, speed, percentage){
	// Get the current time
	currentTime_td = Date.now();
	// Calculate the delta time (ms) between last time call and present time call
	deltaTime_td = currentTime_td - lastFinishedTime_td;
	oneLoopTime_1_td = currentTime_td - initializationTime_td;
	
	// Clear the previous draw
	ctx.clearRect(0,0,canvas.width,canvas.height);
	// Redraw with the new coordinates
	donutGroup.draw(ctx, breakpointValue(percentage, start_point), speed);

	if ( donutGroup.direction == 1 ){
		donutGroup.x += speed*oneCM/1000*frequencyDelta*deltaTime_td;
	}else if ( donutGroup.direction == 0 ){
		donutGroup.x -= speed*oneCM/1000*frequencyDelta*deltaTime_td;
	}   	

	// Set the right border to change the moving direction
	if ( donutGroup.x > (vis_travel_distance_pixel_needed - circleOuterRadius)){
		donutGroup.x = (vis_travel_distance_pixel_needed - circleOuterRadius);
		donutGroup.direction = 0;
	}else if(donutGroup.x <= circleOuterRadius){
		if(oneLoopTime_1_td>1600 && oneLoopTime_1_td<2600){
			// Clear the previous draw
			ctx.clearRect(0,0,canvas.width,canvas.height);
		}else if(oneLoopTime_1_td>2600){	
			donutGroup.x = circleOuterRadius;
			donutGroup.direction = 1;
			initializationTime_td = Date.now();
		}
	}

	lastFinishedTime_td = Date.now();
	callback_id_td[2] = requestAnimationFrame(function (){highSpeed(canvas, ctx, speed, percentage)});
}

function nextDiv(button){
	// For what you will see
	if(i_understand == -1){
		// Displaying the second sentence
		document.getElementById("td_div_see_li_2").style.display = 'block';
		// Displaying the percentage donut groups
		document.getElementById("td_div").style.display = 'block';

		// i_understang slef plus one
		i_understand++;
	}else if(i_understand == 0){
		// Percentage explanation hidden
		document.getElementById("td_div_1").style.display = 'none';
		document.getElementById("td_div_2").style.display = 'block';
		document.getElementById("td_li_1").style.display = 'block';
		document.getElementById("td_div_see_li_3").style.display = 'block';
		
		// i_understang slef plus one
		i_understand++;
	}else if(i_understand == 1){
		// Insert the discription
		document.getElementById("td_div_2_speed").innerHTML = "staying static";
		document.getElementById("td_div_see_li_4").style.display = 'block';
		document.getElementById("td_div_interface").style.display = 'block';
		document.getElementById("td_drawn").style.display = 'block';

		// Clear the previous draw
		context2_td.clearRect(0,0,canvas2_td.width,canvas2_td.height);
		// resetcanvas
		document.getElementById("td_afterimage4").style.display = 'none';
		document.getElementById("td_afterimage1").style.display = 'none';
		document.getElementById("td_afterimage2").style.display = 'none';
		document.getElementById("td_afterimage3").style.display = 'none';
		document.getElementById("td_canvas2").style.display = 'block';
		document.getElementById("td_blank").style.display = 'none';

		// Sketch the static donut
    	lastFinishedTime_td = Date.now();
		callback_id_td[0] = requestAnimationFrame(function (){static(canvas2_td_blank, context2_td, 0, 0.25)});

		// i_understang slef plus one
		i_understand++;
	}else if(i_understand == 2){
		// cancel animation
		cancelAnimationFrame(callback_id_td[0]);
		// Clear the previous draw
		context2_td.clearRect(0,0,canvas2_td.width,canvas2_td.height);
		// resetcanvas
		document.getElementById("td_afterimage4").style.display = 'none';
		document.getElementById("td_afterimage1").style.display = 'none';
		document.getElementById("td_afterimage2").style.display = 'none';
		document.getElementById("td_afterimage3").style.display = 'none';
		document.getElementById("td_canvas2").style.display = 'block';
		document.getElementById("td_blank").style.display = 'none';

		// Insert the discription
		document.getElementById("td_div_2_speed").innerHTML = "moving at a low speed";

		// Reset the donut positiion
		donutGroup.x = circleOuterRadius;
		donutGroup.x_static = vis_travel_distance_pixel_needed/2 + circleOuterDiameter/4;
		donutGroup.x_slow_motion = vis_travel_distance_pixel_needed/4;
		donutGroup.direction = 1;
		// Sketch the donut at low speed
		initializationTime_td = lastFinishedTime_td = Date.now();
		callback_id_td[1] = requestAnimationFrame(function (){lowSpeed(canvas2_td_blank, context2_td, 15, 0.25)});
		
		// i_understang slef plus one
		i_understand++;
	}else if(i_understand == 3){
		// cancel animation
		cancelAnimationFrame(callback_id_td[1]);
		// Clear the previous draw
		context2_td.clearRect(0,0,canvas2_td.width,canvas2_td.height);
		// resetcanvas
		document.getElementById("td_afterimage4").style.display = 'none';
		document.getElementById("td_afterimage1").style.display = 'none';
		document.getElementById("td_afterimage2").style.display = 'none';
		document.getElementById("td_afterimage3").style.display = 'none';
		document.getElementById("td_canvas2").style.display = 'block';
		document.getElementById("td_blank").style.display = 'none';

		// Insert the discription
		document.getElementById("td_div_2_speed").innerHTML = "moving at a high speed";

		// Reset the donut positiion
		donutGroup.x = circleOuterRadius;
		donutGroup.x_static = vis_travel_distance_pixel_needed/2 + circleOuterDiameter/4;
		donutGroup.x_slow_motion = vis_travel_distance_pixel_needed/4;
		donutGroup.direction = 1;

		// Sketch the donut at high speed
		initializationTime_td = lastFinishedTime_td = Date.now();
		callback_id_td[2] = requestAnimationFrame(function (){highSpeed(canvas2_td_blank, context2_td, 30, 0.25)});
		
		// i_understang slef plus one
		i_understand++;
	// The afterimage block display
	}else if(i_understand == 4){
		// cancel animation
		cancelAnimationFrame(callback_id_td[2]);
		// Clear the previous draw
		context2_td.clearRect(0,0,canvas2_td.width,canvas2_td.height);

		// Motion explanation list hidden
		document.getElementById("td_li_1").style.display = 'none';
		// Afterimage explanation list displays
		document.getElementById("td_li_2").style.display = 'block';

		// resetcanvas, afterimage
		document.getElementById("td_afterimage4").style.display = 'none';
		document.getElementById("td_afterimage1").style.display = 'block';
		document.getElementById("td_afterimage2").style.display = 'none';
		document.getElementById("td_afterimage3").style.display = 'none';
		document.getElementById("td_canvas2").style.display = 'none';
		document.getElementById("td_blank").style.display = 'none';

		// i_understang slef plus one
		i_understand++;
	// Together = donut chart + afterimage block
	}else if(i_understand == 5){

		// Afterimage explanation list hidden
		document.getElementById("td_li_2").style.display = 'none';
		// Insert the discription
		document.getElementById("td_div_2_speed_2").innerHTML = "staying static";
		// Together explanation list displays
		document.getElementById("td_li_3").style.display = 'block';

		// resetcanvas, static afterimage
		document.getElementById("td_afterimage4").style.display = 'none';
		document.getElementById("td_afterimage1").style.display = 'none';
		document.getElementById("td_afterimage2").style.display = 'block';
		document.getElementById("td_afterimage3").style.display = 'none';
		document.getElementById("td_canvas2").style.display = 'none';
		document.getElementById("td_blank").style.display = 'none';

		// i_understang slef plus one
		i_understand++;
	}else if(i_understand == 6){

		// Insert the discription
		document.getElementById("td_div_2_speed_2").innerHTML = "moving at a low speed";

		// resetcanvas, low speed afterimage
		document.getElementById("td_afterimage4").style.display = 'none';
		document.getElementById("td_afterimage1").style.display = 'none';
		document.getElementById("td_afterimage2").style.display = 'none';
		document.getElementById("td_afterimage3").style.display = 'block';
		document.getElementById("td_canvas2").style.display = 'none';
		document.getElementById("td_blank").style.display = 'none';

		// i_understang slef plus one
		i_understand++;
	}else if(i_understand == 7){
		// Insert the discription
		document.getElementById("td_div_2_speed_2").innerHTML = "moving at a high speed";
		
		// resetcanvas, high speed afterimage
		document.getElementById("td_afterimage4").style.display = 'block';
		document.getElementById("td_afterimage1").style.display = 'none';
		document.getElementById("td_afterimage2").style.display = 'none';
		document.getElementById("td_afterimage3").style.display = 'none';
		document.getElementById("td_canvas2").style.display = 'none';
		document.getElementById("td_blank").style.display = 'none';

		// i_understang slef plus one
		i_understand++;
	// For what we want you to do
	}else if(i_understand == 8){
		// cancel animation
		cancelAnimationFrame(callback_id_td[6]);
		// Clear the previous draw
		context2_td.clearRect(0,0,canvas2_td.width,canvas2_td.height);

		document.getElementById("td_drawn").style.display = 'none';
		document.getElementById("td_div_interface").style.display = 'none';
		document.getElementById("td_div_see").style.display = 'none';
		document.getElementById("td_div_do").style.display = 'block';
		document.getElementById("td_div_do_li_1").style.display = 'block';

		// i_understang slef plus one
		i_understand++;
	}else if(i_understand == 9){
		document.getElementById("td_div_do_li_2").style.display = 'block';

		// i_understang slef plus one
		i_understand++;
	}else if(i_understand == 10){
		document.getElementById("td_div_do_li_3").style.display = 'block';

		// i_understang slef plus one
		i_understand++;
	}else if(i_understand == 11){
		document.getElementById("td_div_interface_img").style.display = 'block';

		// i_understang slef plus one
		i_understand++;
	}else if(i_understand == 12){
		// Hidden the "I understand" button
		document.getElementById("td_btn").style.display = 'none';

		// endarea
		document.getElementById("td_endarea").style.display = 'block';
		let nextButton = document.getElementById("btn_<?php echo $id;?>");
		nextButton.style.display = 'block';
	}
}
</script>