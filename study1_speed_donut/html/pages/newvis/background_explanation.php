<style type="text/css">

</style>


<div class="row">
  <div class="col">
    <h2>Background Explanation</h2>

    <div class="section" style = "text-align: center;">
      <img src="../html/img/jpg/bge_sb_stadium.jpg" alt="Soccer stadium image from TV" width="600" height="300" style="margin-left: 250px; margin-right: 250px;">
    </div>

    <div class = "section">
      <p>
        <b>Imagine this scenario:</b> 
        <br/>
        You are sitting in front of your TV watching a soccer match. There are two teams on the court, your <span style = "font-weight: bold; color: #E90738;">home team</span> (in <span style = "font-weight: bold; color: #E90738;">red</span>) and an <span style = "font-weight: bold; color: #C3C1C1;">away team</span> (in <span style = "font-weight: bold; color: #C3C1C1;">grey</span>). The soccer ball is being modified by the TV channel to give additional information about the game: The ball will be surrounded by a small chart that shows for how much time each team had possession of the ball. We call this chart a donut chart. 
      </p>
    </div>

    <div class = "section">
      <div id="bge_div" style="width: 200px; height: 150px; text-align: center; float: left; padding-top: 20px;">
        <canvas id = "bge_canvas1"></canvas>
      </div>
      <div style="width: 900px; height: 150px; float: right;">
        <ul>
          <li>The donut has two slices, the <span style = "font-weight: bold; color: #E90738;">red</span> represents <span style = "font-weight: bold; color: #E90738;">your team</span> and the <span style = "font-weight: bold; color: #C3C1C1;">grey</span> one represents the <span style = "font-weight: bold; color: #C3C1C1;">opponent team</span>. The whole donut represents 100 percent(%) of the current playing time.</li>
          <li>In the image on the left, <span style = "font-weight: bold; color: #E90738;">your team</span> (in <span style = "font-weight: bold; color: #E90738;">red</span>) had the ball for <span style = "font-weight: bold; color: #E90738;">60%</span> of the time and the <span style = "font-weight: bold; color: #C3C1C1;">opponent team</span> (in <span style = "font-weight: bold; color: #C3C1C1;">grey</span>) had the ball for <span style = "font-weight: bold; color: #C3C1C1;">40%</span> of the time already played.</li>
        </ul>
      </div>
    </div>
    
  </div>
</div>

<script type="text/javascript">
// Force the subject stays on this page for 5s, to read the sentences
var waitTime_bge = 4;
var btn_bge = false;

// Disable the next button, set the attributes
document.addEventListener("DOMContentLoaded", function(){
  let nextButton = document.getElementById("btn_<?php echo $id;?>");
  nextButton.style.border = "none";
  nextButton.style.background = "#EDEDED";
  nextButton.style.color = "#A3A3A3";
  nextButton.disabled = true;
});

// Excute per second, insert the new button value, to let the subject know how much time left
var callback_id_bge = setInterval(function(){
  if(btn_bge){
                  if(waitTime_bge == 0){
                    let nextButton = document.getElementById("btn_<?php echo $id;?>");
                    btn_<?php echo $id;?>.innerHTML = "I understand. Go to Task Desciption.";
                    nextButton.style.background = "#006400";
                    nextButton.style.color = "#FFFFFF";
                    nextButton.disabled = false;
                  }else{
                    btn_<?php echo $id;?>.innerHTML = "You can click the button after "+ waitTime_bge +"s";
                    waitTime_bge--;
                  }
  }
}, 1000);
  
//Check the next button be pressed and initialized the static explanation donut group
$(document).on('click','#btn_<?php echo $id;?>',function(){
    clearInterval(callback_id_bge);
});
</script>

