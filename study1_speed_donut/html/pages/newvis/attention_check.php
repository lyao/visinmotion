<div class="row">
  <div class="col">
    <h2>We are sorry, but you cannot continue the experiment</h2>
    <p>According to the previous answers, you have not passed the attention check. As we wrote in our consent form, you won't get paid.</p>

    <p>The trials you failed were intentionally trivial to answer and your answers have not been in the acceptable and reasonable range of +/-10%. You failed 6 of these questions at this point.</p>

    <p>For our research study to be valid, it is critical for us to get reliable data. To get reliable data, we need to make sure that our participants read all instructions and do our experiment seriously. Your answers to the previous attention check are an indication that you may not have paid full attention to all our trials.</p>

    <p>Please feel free to contact the requester if you have any question or complaint.</p>
    <p>Thank you for your interest in our online study.</p>
  </div>
</div>