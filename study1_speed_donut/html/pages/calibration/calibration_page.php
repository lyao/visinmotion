<script>

	
    const creditCardPhysicalW = 85.6;  //now in inches
    const creditCardPhysicalH = 53.98; //now in inces

    const inchFactor = 0.03937007874;
    let cCPhysicalW_inch = creditCardPhysicalW * inchFactor;
    let cCPhysicalH_inch = creditCardPhysicalH * inchFactor;
    let aspectRatio = creditCardPhysicalH / creditCardPhysicalW; //
	  //drawing width/height when we start out
	  let cw = creditCardPhysicalW * 5; //
    let ch = cw * aspectRatio; //

    //slider settings
    let max_W_SliderValue = screen.width; //
    let min_W_SliderValue = 100;  //
    let startValue = cw; //

    

    let spikelength = 20; //the little spikes that stand out from the drawn credit card. Needed to see the frame of the card under the physical card
	
	var oneCM = 0; //the number of pixels whose width together equals 1cm on a person's screen based on their calibration
	
 </script>
  
  
<h1>Calibration</h1>

<div id = "<?php echo $id;?>_intro1">
	<div>
		<p>Before we begin with the experiment we have to learn a little more about your screen. 
		On the next couple of pages we will therefore have to go through a calibration step.</p>
   
    <p>
      <ul>
        <li>Please make your browser window <span style
       = "font-weight: bold;">full screen</span> and <span style
       = "font-weight: bold;">do not resize it anymore for the duration of the experiment</span>.</li>
       <li>Please <span style
       = "font-weight: bold;">avoid distractions during the experiment</span> for the next <?php echo $EXP_DURATION ?> minutes.</li>
      </ul>
    </p> 
	</div>
	<button onclick = "moveOn(this)" >I understand</button>
</div>

<div id = "<?php echo $id;?>_intro2" style="display:none">
	<div>
		For this calibration to work every participant of the study needs to have access to a same-sized object that we can compare their screen to. 
	</div>
	<button onclick = "moveOn(this)" >I understand</button>
 </div>
 
 <div id = "<?php echo $id;?>_intro3" style="display:none">
	<div>
		One common object with the same size around the world are plastic cards, the size of bank or credit cards. 
		<div style="margin:20px 10px 20px 10px">
			<img src="img/calibration/multiplecards2.jpg" height="350px"><img src="img/calibration/credit-card.png" height="150px" style="margin-left:20px;"> 
		</div>
		For the upcoming calibration step only the size of the card matters. You can choose any old or current card that is <span style
       = "font-weight: bold;">the same size</span> as your bank card, for example your library card, a loyalty card, or a membership card. As long as your card is the size of a credit or debit card it will work. 
	</div>
	<button onclick = "moveOn(this)">I understand</button>
</div>

<div id = "<?php echo $id;?>_intro4" style="display:none">
	<div>
		Please grab one bank-card sized card now. You need it for the next step. <br><br>
		Please rest assured that there is no way that we can read your card through your computer's screen. Your screen is not a card reading device and the purpose of this study has nothing to do with the card - that's why we do not care which card you use as long as it is <span style
       = "font-weight: bold;">the same size</span> as a bank card. 
	</div>
   <button onclick = "moveOn(this)">I have my card ready</button>
</div>

<div id = "<?php echo $id;?>_main" style="display:none">

	<div>
		Align your card with the left and top green lines of the card below and use the slider to match the size of your card as accurately as you can. <br><br>
		At the end of the calibration the white "1cm" and "1in" lines should be exactly 1cm / 1 inch long on your screen. You can measure this with a ruler if you want but you do not have to. 
	</div>
	
	<div style="margin:50px 10px 50px 10px; overflow:hidden;">
		<div style="width:50%;float:left;margin-right:10px;">
			<input type="range"  id="myRange" oninput="moveSlider(this)" style="-webkit-appearance: none;appearance: none;width: 100%;height: 25px;background: #d3d3d3;outline: none;"/>
		</div>
		<p id="range-label" style="overflow: hidden;vertical-align:text-bottom;">0 - 10</p>
	</div>
	
	<svg id="calibrationSVG">
	  <line id = "calib_rightline" style="stroke:#000000;stroke-width:4;" y1 = "0"/>
	  <line id = "calib_bottomline" style="stroke:#000000;stroke-width:4;" x1 = "0"/>
	  <line id = "calib_leftline" style="stroke:#4CAF50;stroke-width:4;" y1 = "0"/>
	  <line id = "calib_topline" style="stroke:#4CAF50;stroke-width:4;" x1 = "0" />
	  <rect id="calib_card" style="fill: #69b3a2" rx="20" ry="20" stroke="black" stroke-opacity="0.5" width=240 height = 120></rect>
	  <text id = "calib_instructions" x="5" y="5" class="small" fill=white>&larr; &uarr; Put your card here along the green lines (left and top) &uarr;</text>
	  <text id = "calib_onecmtext" class="small" fill=white>1cm</text>
	  <text id = "calib_oneintext" class="small" fill=white>1inch</text>
	  <line id =  "calib_onecm"  style="stroke:#FFFFFF;stroke-width:2;" />
	  <line id =  "calib_oneinch" style="stroke:#FFFFFF;stroke-width:2;"/>
	</svg>
	
	<div>
		<button onclick = "moveOn(this)" style="float:left">I finished the calibration</button>
  </div>

  <div id = "<?php echo $id;?>_endarea" style="display:none; margin-top: 100px;">
    <p style="font-weight: bold;">Please click the button below to proceed to the pre-questionnaire:</p>
  </div>
</div>

<script>

//this script is for controlling the intro pages for the calibration, hiding and unhiding the different parts
let currentElement = 0;
let introdivs = ["<?php echo $id;?>_intro1",
					   "<?php echo $id;?>_intro2",
					   "<?php echo $id;?>_intro3",
					   "<?php echo $id;?>_intro4",
					   "<?php echo $id;?>_main"];

//disable the next button for the moment
var currentdiv = document.getElementById(introdivs[currentElement]);

document.addEventListener("DOMContentLoaded", function(){
	let nextbutton = document.getElementById("btn_<?php echo $id;?>");
	nextbutton.style.display = 'none';
});


function moveOn(button){
    
	//console.log(currentElement);
    var currentdiv = document.getElementById(introdivs[currentElement]);
	//console.log(introdivs[currentElement]);
	
	if (introdivs[currentElement] == "<?php echo $id;?>_main"){
    // Enable the end area
    document.getElementById("<?php echo $id;?>_endarea").style.display = 'block';

		var nextbutton = document.getElementById("btn_<?php echo $id;?>");
    // Set button attributes
    nextbutton.style.border = "none";
    nextbutton.style.background = "#006400";
    nextbutton.style.color = "#FFFFFF";
		nextbutton.style.display = 'block';
	}
	else{
		currentElement++;
		var nextdiv = document.getElementById(introdivs[currentElement]);
		
		currentdiv.style.display = 'none';
		nextdiv.style.display = 'block';
		
		if(introdivs[currentElement]  == "<?php echo $id;?>_main"){
			//only once the svg is visible can we get the bounding box 
			//without the code the svg isn't visible when its surrounding div is made visible
			var svg = document.getElementById("calibrationSVG");
			var bbox = svg.getBBox();
			svg.setAttribute("width", bbox.width + "px");
			svg.setAttribute("height", bbox.height + "px");
			svg.setAttribute("viewBox", `${bbox.x} ${bbox.y} ${bbox.width} ${bbox.height}`);
		} 
	}
};

// //when the next button is clicked we log
// //code from Yvonne's example page
// $('body').on('next', function(e, type){
// 	// console.log(e);
//   if (type === '<?php echo $id;?>'){
//     measurements['onecm'] = oneCM;
//     console.log("loggin calibration. 1cm =  " + oneCM + "px");
//     // console.log("excluded " + excluded);
//   }
// });

</script>

<script>
//this script is for doing the actual calibration, controlling the svg etc.
  var card = document.getElementById("calib_card");
  card.setAttributeNS(null,"width",cw);
  card.setAttributeNS(null,"height",ch);
  card.setAttributeNS(null,"x",spikelength);
  card.setAttributeNS(null,"y",spikelength);

  var inst = document.getElementById("calib_instructions");
  inst.setAttributeNS(null,"x",2 * spikelength);
  inst.setAttributeNS(null,"y",2 * spikelength);


  var ll = document.getElementById("calib_leftline");
  ll.setAttributeNS(null,"x1",spikelength);
  ll.setAttributeNS(null,"x2",spikelength);
  var y2 = ch + 2 * spikelength;
  ll.setAttributeNS(null,"y2",y2);

  var rl = document.getElementById("calib_rightline");
  rl.setAttributeNS(null,"x1",cw + spikelength);
  rl.setAttributeNS(null,"x2",cw + spikelength);
  var y2 = ch + 2 * spikelength;
  rl.setAttributeNS(null,"y2",y2);

  var tl = document.getElementById("calib_topline");
  tl.setAttributeNS(null,"y1",spikelength);
  tl.setAttributeNS(null,"y2",spikelength);
  var x2 = parseFloat(cw) + 2 * spikelength;
  tl.setAttributeNS(null,"x2",x2);

  var bl = document.getElementById("calib_bottomline");
  bl.setAttributeNS(null,"y1",ch+spikelength);
  bl.setAttributeNS(null,"y2",ch+spikelength);
  var x2 = parseFloat(cw) + 2 * spikelength;
  bl.setAttributeNS(null,"x2",x2);

  var pixelWidth_mm = creditCardPhysicalW / cw;
  var pixelWidth_in = pixelWidth_mm * inchFactor;
  oneCM = 10 / pixelWidth_mm;
  oneIn =  1.0 / pixelWidth_in;

  var onecm = document.getElementById("calib_onecm");
  onecmx1 = spikelength+20;
  onecmy = spikelength + 80;
  onecm.setAttributeNS(null,"x1",onecmx1);
  onecm.setAttributeNS(null,"x2",onecmx1 + oneCM);
  onecm.setAttributeNS(null,"y1",onecmy);
  onecm.setAttributeNS(null,"y2",onecmy);

  var onecmtext = document.getElementById("calib_onecmtext");
  onecmtext.setAttributeNS(null,"x",onecmx1);
  onecmtext.setAttributeNS(null,"y",onecmy - 5);

  var oneinch = document.getElementById("calib_oneinch");
  oneiny = onecmy + 40;
  oneinch.setAttributeNS(null,"x1",onecmx1);
  oneinch.setAttributeNS(null,"x2",onecmx1 + oneIn);
  oneinch.setAttributeNS(null,"y1",oneiny);
  oneinch.setAttributeNS(null,"y2",oneiny);

  var oneintext = document.getElementById("calib_oneintext");
  oneintext.setAttributeNS(null,"x",onecmx1);
  oneintext.setAttributeNS(null,"y",oneiny - 5);

  var slider = document.getElementById("myRange");
  slider.setAttribute("min",min_W_SliderValue);
  slider.setAttribute("max",max_W_SliderValue);
  slider.value = startValue; //("value",startValue);

  var sliderlabel = document.getElementById("range-label");
  sliderlabel.innerHTML  = startValue;


var moveSlider = function(slider){
    var cw = slider.value;
    var ch = cw * aspectRatio;

    var value = slider.value;
    //console.log(slider.value);
    var card = document.getElementById("calib_card");
    card.setAttributeNS(null,"width",cw);
    card.setAttributeNS(null,"height",ch);

    var svg = document.getElementById("calibrationSVG"); // document.getElementsByTagName("calibrationSVG")[0];
    var bbox = svg.getBBox();
	//we need to resize the bounding box of the svg and push content underneath away to have everything visible all the time
    svg.setAttribute("width", bbox.width + "px");
    svg.setAttribute("height", bbox.height + "px");
    svg.setAttribute("viewBox", `${bbox.x} ${bbox.y} ${bbox.width} ${bbox.height}`);

    sliderlabel.innerHTML  = value;

    var ll = document.getElementById("calib_leftline");
    var y2 = ch + 2 * spikelength;
    ll.setAttributeNS(null,"y2",y2);

    var tl = document.getElementById("calib_topline");
    var x2 = parseFloat(cw)  + 2 * spikelength;
    tl.setAttributeNS(null,"x2",x2);

    var rl = document.getElementById("calib_rightline");
    rl.setAttributeNS(null,"x1",parseFloat(cw) + spikelength);
    rl.setAttributeNS(null,"x2",parseFloat(cw) + spikelength);
    var y2 = ch + 2 * spikelength;
    rl.setAttributeNS(null,"y2",y2);

    var bl = document.getElementById("calib_bottomline");
    bl.setAttributeNS(null,"y1",ch+spikelength);
    bl.setAttributeNS(null,"y2",ch+spikelength);
    var x2 = parseFloat(cw) + 2 * spikelength;
    bl.setAttributeNS(null,"x2",x2);

    var pixelWidth_mm = creditCardPhysicalW / cw;
    var pixelWidth_in = pixelWidth_mm * inchFactor;
    oneCM = 10 / pixelWidth_mm;
    oneIn =  1.0 / pixelWidth_in;

    var onecm = document.getElementById("calib_onecm");
    onecm.setAttributeNS(null,"x2",spikelength+20 + oneCM);

    var oneinch = document.getElementById("calib_oneinch");
    oneinch.setAttributeNS(null,"x2",spikelength+20 + oneIn);
  }

</script>

<script src = "js/visualizations/general_parameters.js"></script> 
<script>
  //Check the next button be pressed and initialized the soccer ball + donut group for the next page
  $(document).on('click','#btn_<?php echo $id;?>',function(){
    //Make sure the calibration tool's result be applied
    // console.log("Calibration tool 1cm =  " + oneCM + "px");

    //And then transfer all parameters into pixel, rewrite all parameters
    soccerballDiameter = oneCM * soccerballDiameter_cm; // The diameter of soccer ball
    // console.log("The diameter of soccer ball =  " + soccerballDiameter + "px");
    circleInnerDiameter = oneCM * circleInnerDiameter_cm;
    // console.log("The inner diameter of donut chart =  " + circleInnerDiameter + "px");
    circleOuterDiameter = oneCM * circleOuterDiameter_cm;
    // console.log("The outer diameter of donut chart =  " + circleOuterDiameter + "px");

    soccerballRadius = oneCM * soccerballRadius_cm;
    // console.log("The radius of soccer ball =  " + soccerballRadius + "px");
    circleInnerRadius = oneCM * circleInnerRadius_cm; // The inner radius of donut chart
    // console.log("The inner radius of donut chart =  " + circleInnerRadius + "px");
    circleOuterRadius = oneCM * circleOuterRadius_cm; // The outer radius of donut chart
    // console.log("The outer radius of donut chart =  " + circleOuterRadius + "px");

    vis_travel_distance_pixel_needed = oneCM * vis_travel_distance_cm + circleOuterRadius*2;
    // console.log("vis_travel_distance_pixel_needed =  " + vis_travel_distance_pixel_needed + "px");
    /* ---------------------------------------------------------------------------------------------------------------------------------------*/

    // If the subject's equipment does not meet our requirement, exclude it and stop the experiment
    if(vis_travel_distance_pixel_needed>=(window.innerWidth/2 + 450)){
      measurements['oneCM'] = oneCM;
      measurements['pixel_size_check'] = "The pixel size is too big to do the experiment: " + vis_travel_distance_pixel_needed + "px";

      // $.ajax({
      //   url: 'ajax/excluded.php',
      //   type: 'POST',
      //   data: JSON.stringify(measurements),
      //   contentType: 'application/json',
      // });

      excluded = true;
      $('body').trigger('excluded');
      console.log("Failed on calibration tool -> exclude");
      $('#<?php echo $next ?>').hide().promise().done(() => $('#excluded').show());
      $(":button").hide();

    }else{
      console.log("Passed on calibration tool");
      resolution_height = window.screen.height;
      resolution_width = window.screen.width;
      measurements['resolution_height'] = resolution_height;
      measurements['resolution_width'] = resolution_width;

      console.log("resolution_height = " + resolution_height);
      console.log("resolution_width = " + resolution_width);
      
      $('#<?php echo $id ?>').hide().promise().done(() => $('#<?php echo $next ?>').show());
    }
    /* ---------------------------------------------------------------------------------------------------------------------------------------*/

    /* Sketch the Canvas Time Based Animation for the next pages */

    // For training: set the canvas size accroding to the calibration result
    myCanvas_training = document.getElementById("canvas_0");
    context_training = myCanvas_training.getContext("2d");
    // After image part, composed by 5 canvas, 4 free-drawn, 1 blank
    myCanvas_training_blank = document.getElementById("training_blank");
    context_training_blank = myCanvas_training_blank.getContext("2d");
    myCanvas_training_afterimage = document.getElementById("training_afterImage");
    context_training_afterimage = myCanvas_training_afterimage.getContext("2d");
    myCanvas_training_afterimage_1 = document.getElementById("training_afterImage_1");
    context_training_afterimage_1 = myCanvas_training_afterimage_1.getContext("2d");
    myCanvas_training_afterimage_2 = document.getElementById("training_afterImage_2");
    context_training_afterimage_2 = myCanvas_training_afterimage_2.getContext("2d");
    myCanvas_training_afterimage_3 = document.getElementById("training_afterImage_3");
    context_training_afterimage_3 = myCanvas_training_afterimage_3.getContext("2d");

    // For trial: set the canvas size accroding to the calibration result
    myCanvas_trial = document.getElementById("canvas_1");
    context_trial = myCanvas_trial.getContext("2d");
    // After image part, composed by 5 canvas, 4 free-drawn, 1 blank
    myCanvas_blank = document.getElementById("trial_blank");
    context_blank = myCanvas_blank.getContext("2d");
    myCanvas_afterimage = document.getElementById("trial_afterImage");
    context_afterimage = myCanvas_afterimage.getContext("2d");
    myCanvas_afterimage_1 = document.getElementById("trial_afterImage_1");
    context_afterimage_1 = myCanvas_afterimage_1.getContext("2d");
    myCanvas_afterimage_2 = document.getElementById("trial_afterImage_2");
    context_afterimage_2 = myCanvas_afterimage_2.getContext("2d");
    myCanvas_afterimage_3 = document.getElementById("trial_afterImage_3");
    context_afterimage_3 = myCanvas_afterimage_3.getContext("2d");
    // // If there is a soccerball
    // myImage = new Image();
    // myImage.src = "../html/img/png/soccerball1.png";
    
    // Set the height and the width for each canvas
    // The 5 divs in After image part have the same size as the stimuli one
    myCanvas_training_afterimage_3.height = myCanvas_training_afterimage_2.height = myCanvas_training_afterimage_1.height = myCanvas_training_afterimage.height = myCanvas_training_blank.height = myCanvas_training.height = myCanvas_afterimage_3.height = myCanvas_afterimage_2.height = myCanvas_afterimage_1.height = myCanvas_afterimage.height = myCanvas_blank.height = myCanvas_trial.height = circleOuterDiameter + 1;
    myCanvas_training_afterimage_3.width = myCanvas_training_afterimage_2.width = myCanvas_training_afterimage_1.width = myCanvas_training_afterimage.width = myCanvas_training_blank.width = myCanvas_training.width = myCanvas_afterimage_3.width = myCanvas_afterimage_2.width = myCanvas_afterimage_1.width = myCanvas_afterimage.width = myCanvas_blank.width = myCanvas_trial.width = vis_travel_distance_pixel_needed;

    // Set the width of #trial_drawn equals to the width of #trial_stimuli
    // Set the width of #trial_answer equals to the width of #trial_answer, to make the elements in #trial_answer align-center
    document.getElementById("training_answer").style.width = document.getElementById("training_drawn").style.width = document.getElementById("trial_answer").style.width = document.getElementById("trial_drawn").style.width = document.getElementById("trial_stimuli").style.width + "px"; // Don't forget add "px", if not, size setting won't fire
    // Set the height of #trial_drawn 1.5 multiples higher than the height of .canvas, to avoid the tiny scale offset
    document.getElementById("training_drawn").style.height = document.getElementById("trial_drawn").style.height = myCanvas_trial.height * 1.5 + "px";

    // Sketch the donut chart(soccerball is an option)
    donutGroup = {
      x:circleOuterRadius, // In case of fast motion: Start point is the far left
      x_slow_motion: vis_travel_distance_pixel_needed/4 + circleOuterDiameter/4, // In case of slow motion: Start point is the middle far left ( == 1/4 vis_travel_distance_pixel_needed for far left)
      x_static: vis_travel_distance_pixel_needed/2, // In case of static: Start point is the middle of the canvas

      y:circleOuterRadius,
      direction: 1, // Start direction is from left to right
      // soccerballdiameter: soccerballDiameter, // If there is a soccerball in the middle
      sliceweight: circleOuterRadius - circleInnerRadius, 
      donutmiddleradius: (circleOuterRadius - circleInnerRadius)/2 + circleInnerRadius,
      
      draw: function (ctx, breakpoint, speed){
       
            // Sketch the target slice
            ctx.beginPath();
            // In case of fast motion
            if(speed == 30){
              ctx.arc(this.x, this.y, this.donutmiddleradius, start_point, breakpoint);
            // In case of slow motion
            }else if(speed == 15){
              ctx.arc(this.x_slow_motion, this.y, this.donutmiddleradius, start_point, breakpoint);
            // In case of static
            }else if(speed == 0){
              ctx.arc(this.x_static, this.y, this.donutmiddleradius, start_point, breakpoint);
            }
            ctx.strokeStyle = condition_color;
            ctx.lineWidth = this.sliceweight;
            ctx.stroke();

            // Sketch the other slice
            ctx.beginPath();
            // In case of fast motion
            if(speed == 30){
              ctx.arc(this.x, this.y, this.donutmiddleradius, breakpoint, end_point);
            // In case of slow motion
            }else if(speed == 15){
              ctx.arc(this.x_slow_motion, this.y, this.donutmiddleradius, breakpoint, end_point);
            // In case of slow motion
            }else if(speed == 0){
              ctx.arc(this.x_static, this.y, this.donutmiddleradius, breakpoint, end_point);
            }

            ctx.strokeStyle = other_color;
            ctx.lineWidth = this.sliceweight;
            ctx.stroke();

            // // Sketch the ball (In the formal one, no soccer ball)
            // if(soccerball == true){
            //   context.drawImage(myImage, this.x-this.soccerballdiameter/2, this.y-this.soccerballdiameter/2, this.soccerballdiameter, this.soccerballdiameter);
            //   console.log("canvas ball display 1");
            // }

            // console.log("Donut drawn");
          }
    }

    // Sketch the after image
    afterImage = {
      sliceweight: circleOuterRadius - circleInnerRadius, 
      donutmiddleradius: (circleOuterRadius - circleInnerRadius)/2 + circleInnerRadius,
      // Sketch on which canvas, which context, and how many patterns to draw
      draw: function(canvas, ctx, conut){
        let x = new Array(), y = new Array(), percentage = new Array();
        for(let i=0; i<conut;i++){
          x.push(randomNumberEasiest(0, canvas.width));
          y.push(randomNumberEasiest(0, canvas.height));
          percentage.push(breakpointValue(randomNumber(0, 100, original_percentage_array), start_point));
        }
        for(let i=0;i<conut; i++){
          ctx.beginPath();
          ctx.arc(x[i],y[i], this.donutmiddleradius, start_point, percentage[i]);
          ctx.strokeStyle = condition_color;
          ctx.lineWidth = this.sliceweight;
          ctx.stroke();
        }
      }
    }
    /* ---------------------------------------------------------------------------------------------------------------------------------------*/
    // Insert the training title and speed discription into HTML
    document.getElementById("training_title_1").innerHTML = "Training: Part 1";
    document.getElementById("training_first_sentence").innerHTML = "Now it’s time for you to start training, confirm you read each line by clicking the checkbox:";
    if(random_speed_array[0] == original_speed_array[0]){
      document.getElementById("training_speed_discription").innerHTML = "staying static";
    }else if(random_speed_array[0] == original_speed_array[1]){
      document.getElementById("training_speed_discription").innerHTML = "moving at a low speed";
    }else if(random_speed_array[0] == original_speed_array[2]){
      document.getElementById("training_speed_discription").innerHTML = "moving at a high speed";
    }

    // Insert start experiment button description
    document.getElementById("start_training_button").innerHTML = "Start Training Part 1";

    /* Prepare the afterimage block */ 
    // For training part, draw 4 afterimages
    afterImage.draw(myCanvas_training_afterimage, context_training_afterimage, 50);
    afterImage.draw(myCanvas_training_afterimage_1, context_training_afterimage_1, 50);
    afterImage.draw(myCanvas_training_afterimage_2, context_training_afterimage_2, 50);
    afterImage.draw(myCanvas_training_afterimage_3, context_training_afterimage_3, 50);

    // For trial part, draw 4 afterimages
    afterImage.draw(myCanvas_afterimage, context_afterimage, 50);
    afterImage.draw(myCanvas_afterimage_1, context_afterimage_1, 50);
    afterImage.draw(myCanvas_afterimage_2, context_afterimage_2, 50);
    afterImage.draw(myCanvas_afterimage_3, context_afterimage_3, 50);
    /* ---------------------------------------------------------------------------------------------------------------------------------------*/
      
    /* Sketch the donut group for pre-questionnaire */
    var canvas1_preqn = document.getElementById("preqn_canvas1");
    var context1_preqn = canvas1_preqn.getContext("2d");
    var sliceweight_preqn = circleOuterRadius - circleInnerRadius, donutradius_preqn = (circleOuterRadius - circleInnerRadius)/2 + circleInnerRadius, gap_preqn = 10;

    // Set the wwidth of preqn_canvas
    canvas1_preqn.width = 20*donutradius_preqn+11*sliceweight_preqn+9*gap_preqn;
    canvas1_preqn.height = 2*donutradius_preqn+2*sliceweight_preqn+0*gap_preqn;

    // Prepare the percnetage and the position
    var per_preqn = new Array(), x_preqn = new Array(), y_preqn = new Array();
    // per_preqn = [0.18, 0.32, 0.43, 0, 0.58, 0.72, 0.83, 0, 0.75, 0.85, 0.99];
    per_preqn = [0.01, 0.15, 0.25, 0.35, 0.45, 0.55, 0.65, 0.75, 0.85, 0.99];

    for(let i=0; i<10; i++){
      per_preqn[i] = breakpointValue(per_preqn[i], start_point);
      x_preqn[i] = (2*i+1)*donutradius_preqn + (i+1)*sliceweight_preqn + i*gap_preqn;
      y_preqn[i] = donutradius_preqn+sliceweight_preqn;
    }

    // Skectch
    for(let i=0; i<10; i++){
      Sketch(context1_preqn, per_preqn[i], x_preqn[i], y_preqn[i], sliceweight_preqn, donutradius_preqn);
    }
    /* ---------------------------------------------------------------------------------------------------------------------------------------*/

    /* Sketch the donut chart with the soccer ball inside for background explanation */
    var canvas1_bge = document.getElementById("bge_canvas1");
    var context1_bge = canvas1_bge.getContext("2d");
    var sliceweight_bge = circleOuterRadius - circleInnerRadius, donutradius_bge = (circleOuterRadius - circleInnerRadius)/2 + circleInnerRadius;

    // Set the wwidth of preqn_canvas
    canvas1_bge.width = 2*donutradius_bge+2*sliceweight_bge;
    canvas1_bge.height = 2*donutradius_bge+2*sliceweight_bge;

    Sketch(context1_bge, breakpointValue(0.6, start_point), canvas1_bge.width/2, canvas1_bge.height/2, sliceweight_bge, donutradius_bge, true, soccerballDiameter, soccerballDiameter);
    /* ---------------------------------------------------------------------------------------------------------------------------------------*/

    /* Sketch the donut groups for task description */
    // For percentage explanation
    var canvas1_td = document.getElementById("td_canvas1");
    var context1_td = canvas1_td.getContext("2d");
    var sliceweight_td = circleOuterRadius - circleInnerRadius, donutradius_td = (circleOuterRadius - circleInnerRadius)/2 + circleInnerRadius, gap_td = 20;
    var gif_1 = document.getElementById("td_img_a"), gif_2 = document.getElementById("td_img_sa"), gif_3 = document.getElementById("td_img_la"), gif_4 = document.getElementById("td_img_ha");

    // Set the wwidth of preqn_canvas
    canvas1_td.width = 1100;
    canvas1_td.height = 2*donutradius_td+2*sliceweight_td;

    // Prepare the percnetage and the position
    var per_td = [0.1, 0.3, 0.5, 0.7, 0.9], x_td = new Array(), y_td = new Array();

    for(let i=0; i<5; i++){
      per_td[i] = breakpointValue(per_td[i], start_point);
      x_td[i] = 110 + 220*i;
      y_td[i] = donutradius_td+sliceweight_td;
    }

     // Skectch
    for(let i=0; i<5; i++){
      Sketch(context1_td, per_td[i], x_td[i], y_td[i], sliceweight_td, donutradius_td);
    }

    // For displaying explanation
    // Get the canvas and ctx
    canvas2_td = document.getElementById("td_canvas2");
    context2_td = canvas2_td.getContext("2d");
    // After image part, composed by 5 canvas, 4 free-drawn, 1 blank
    canvas2_td_blank = document.getElementById("td_blank");
    context2_td_blank = canvas2_td_blank.getContext("2d");

    canvas2_td_afterimage_1 = document.getElementById("td_afterimage1");

    canvas2_td_afterimage_2 = document.getElementById("td_afterimage2");

    canvas2_td_afterimage_3 = document.getElementById("td_afterimage3");

    canvas2_td_afterimage_4 = document.getElementById("td_afterimage4");

    // Set the width and height for each canvas
    canvas2_td_afterimage_4.height = canvas2_td_afterimage_3.height = canvas2_td_afterimage_2.height = canvas2_td_afterimage_1.height = canvas2_td_blank.height = canvas2_td.height = circleOuterDiameter + 1;
    gif_4.width = gif_3.width = gif_2.width = gif_1.width = canvas2_td_afterimage_4.width = canvas2_td_afterimage_3.width = canvas2_td_afterimage_2.width = canvas2_td_afterimage_1.width = canvas2_td_blank.width = canvas2_td.width = vis_travel_distance_pixel_needed;
    gif_4.height = gif_3.height = gif_2.height = gif_1.height = gif_1.width / 1750*70;

    // Set the width of #td_drawn equals to the width of #trial_stimuli
    document.getElementById("td_drawn").style.width = document.getElementById("trial_stimuli").style.width + "px"; // Don't forget add "px", if not, size
    document.getElementById("td_drawn").style.height = canvas2_td.height * 1.5 + "px";


    /* ---------------------------------------------------------------------------------------------------------------------------------------*/

    /* Sketch the vision focus box */
    // Set the width of #td_drawn equals to the width of #trial_stimuli
    document.getElementById("vision_focus_box_son").style.width = document.getElementById("trial_stimuli").style.width + "px";
    document.getElementById("vision_focus_box_son").style.height = document.getElementById("vision_focus_box_son").style.lineHeight = myCanvas_trial.height + "px";
    document.getElementById("vision_focus_box_parent").style.height = myCanvas_trial.height * 1.5 + 40 + "px";
    /* ---------------------------------------------------------------------------------------------------------------------------------------*/
   
  });
   
</script>



