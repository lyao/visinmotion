<div class="row">
  <div class="col">
    <h2>We are sorry, but you cannot continue the experiment</h2>
    <p>According to the calibration tool's result, that your screen size is not big enough to do our experiment.</p>

    <p>
      In the introduction of our experiment, we have explained that the minimum screen size is 13", and if your screen size cannot reach the study request, as we wrote in our consent form, you won't get paid.
    </p>

    <p>Please feel free to contact the requester if you have any question or complaint.</p>
    <p>Thank you for your interest in our online study.</p>
  </div>
</div>