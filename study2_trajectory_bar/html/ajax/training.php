<?php

	$data = json_decode(file_get_contents('php://input'), true);

	$agree_filename = "../../results/format/training/training_" . $data["participant_id"] . '.csv';
  $exists = file_exists($agree_filename);
  $agree_file = fopen($agree_filename, "a+");

  if (!$exists){
    fwrite($agree_file, "participant_id, timestamp, study_id,session_id, color_condiiton,speed_condiiton, browser_name, browser_version, os, consent_form_agreed, one_CM_equal_to_how_many_pixels, resolution_height_pixel, resolution_width_pixel, barchart_length_cm, barchart_length_pixel, barchart_width_cm, barchart_width_pixel, vis_travel_distance_one_way_cm, vis_travel_distance_one_way_pixel, movement_kind, current_trajectory, vis_display_duration_ms, speed_duration, current_speed_cm_per_s, training_turn, correct_answer, given_answer, current_training_correct_times_per_turn, current_training_errors_times_per_turn, current_training_times_per_turn");
  }

  fwrite($agree_file,
    PHP_EOL .
    $data["participant_id"] . ',' .
    date(DateTime::ISO8601) . ',' .
    $data["study_id"] . ',' .
    $data["session_id"]. ',' .

    $data["color_condiiton"] . ',' .
    $data["speed_condiiton"] . ',' .

    $data['browser_name']   . ',' .
    $data["browser_version"]. ',' .
    $data["os"] . ',' .
    $data["consent_form_agreed"] . ',' .

    $data["oneCM"]. ',' .

    $data["resolution_height"]. ',' .
    $data["resolution_width"]. ',' .

    $data["barchart_length_cm"]. ',' .
    $data["barchart_length_pixel"]. ',' .
    $data["barchart_width_cm"]. ',' .
    $data["barchart_width_pixel"]. ',' .
    
    $data["vis_travel_distance_cm"]. ',' .
    $data["vis_travel_distance_pixel"]. ',' .  

    $data["movement_kind"]. ',' . 
    $data["current_trajectory"]. ',' . 

    $data["vis_display_duration"]. ',' .

    $data["speed_duration"]. ',' .
    $data["current_speed_cm_s"] . ',' .

    $data["training_trial_count"] . ',' .

    $data["correct_answer"]. ',' .
    $data["given_answer"]. ',' .

    $data["current_training_correct_times_per_turn"] . ',' .
    $data["current_training_errors_times_per_turn"] . ',' .
    $data["current_training_times_per_turn"]

  );

  fclose($agree_file);
	exit;

?>