<?php

	$data = json_decode(file_get_contents('php://input'), true);

	$results_format_filename = "../../results/format/answer/" . $data["participant_id"] . '.csv';
  $exists = file_exists($results_format_filename);
  $results_format_file = fopen($results_format_filename, "a+");

  if (!$exists){
    fwrite($results_format_file, "participant_id,timestamp, study_id,session_id, color_condiiton, speed_condiiton, browser_name, browser_version, os, consent_form_agreed, trial_number, current_experiment_part,current_repetition_per_part, current_trial_per_repetition, one_CM_equal_to_how_many_pixels, resolution_height_pixel, resolution_width_pixel, soccerball_diameter_cm, soccerball_diameter_pixel, barchart_length_cm, barchart_length_pixel, barchart_width_cm, barchart_width_pixel, vis_travel_distance_one_way_cm, vis_travel_distance_one_way_pixel, vis_display_duration_ms, movement_kind, current_trajectory, speed_duration, current_speed_cm_per_s, correct_answer, given_answer, time_taken_type_ms, time_taken_submit_ms, attention_check_errors_number, excluded");
  }

  fwrite($results_format_file,
    PHP_EOL .
    $data["participant_id"] . ',' .
    date(DateTime::ISO8601) . ',' .
    $data["study_id"] . ',' .
    $data["session_id"] . ',' .

    $data["color_condiiton"] . ',' .
    $data["speed_condiiton"] . ',' .

    $data['browser_name']   . ',' .
    $data["browser_version"]. ',' .
    $data["os"] . ',' .
    $data["consent_form_agreed"] . ',' .

    $data["trial_number"] . ',' .
    $data["current_experiment_part"] . ',' .
    $data["current_repetition_per_part"] . ',' .
    $data["current_trial_per_repetition"] . ',' .

    $data["oneCM"]. ',' .

    $data["resolution_height"]. ',' .
    $data["resolution_width"]. ',' .

    $data["soccerball_diameter_cm"]. ',' .
    $data["soccerball_diameter_pixel"]. ',' .

    $data["barchart_length_cm"]. ',' .
    $data["barchart_length_pixel"]. ',' .
    $data["barchart_width_cm"]. ',' .
    $data["barchart_width_pixel"]. ',' .
    
    $data["vis_travel_distance_cm"]. ',' .
    $data["vis_travel_distance_pixel"]. ',' .  
    $data["vis_display_duration"]. ',' .

    // New add
    $data["movement_kind"]. ',' .
    $data["current_trajectory"]. ',' .

    $data["speed_duration"]. ',' . 
    $data["current_speed_cm_s"]. ',' .

    $data["correct_answer"]. ',' .
    $data["given_answer"]. ',' .

    $data["time_taken_type_ms"]. ',' .
    $data["time_taken_submit_ms"]. ',' .

    $data["attention_check_errors_number"]. ',' .
    $data["excluded"]

  );

  fclose($results_format_file);
	exit;

?>