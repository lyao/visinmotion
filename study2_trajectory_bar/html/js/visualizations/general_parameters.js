// console.log("general_parameters.js loaded");

/* Visualized object parameters -- same for training part and formal trial */
// The minimum request for the experiment is 13", the width of 13" screen is 26cm
// We set the distanc travled of our soccer ball equal to 24 cm
var vis_travel_distance_cm = 24; //in CM
var vis_travel_distance_pixel_needed; // in PIXEL
// Set the size unit of donut group (in centimetere), to make sure that the seen size is the same for each parctipicant
// Diameters in CM
var soccerballDiameter_cm = 0.5; // The unvisible soccerball diameter is 0.5 CM
var circleInnerDiameter_cm = soccerballDiameter_cm+0.25; // THe inner diameter of the donut chart is 0.75 CM
var circleOuterDiameter_cm = soccerballDiameter_cm+0.5; // THe outer diameter of the donut chart is 1 CM
// Radius in CM
var soccerballRadius_cm = soccerballDiameter_cm/2;
var circleInnerRadius_cm = soccerballDiameter_cm/2+0.25/2;
var circleOuterRadius_cm = soccerballDiameter_cm/2+0.25;
// Diameters in PIXEL
var soccerballDiameter, circleInnerDiameter, circleOuterDiameter;
// Radius in PIXEL
var soccerbalRadius, circleInnerRadius, circleOuterRadius;
/* ---------------------------------------------------------------------------------------------------------------------------------------*/

/* Study 1 - Speed - bar */
// Visualized object variables in CM
// Let the bar chart width equals to the donut outer diameter
// The longer border
var barchart_length_cm = 2 * Math.PI * (circleOuterRadius_cm + circleInnerRadius_cm)/2; 
// Calcualte the barchart_height, ensure the area of bar chart is the same as the dount chart
// The shorter border
var barchart_width_cm = circleOuterRadius_cm - circleInnerRadius_cm;
// Visualized object variables in PIXELS
var barchart_length, barchart_width;
// Travel distance in cm
var barchart_travel_distance_cm = 24;
var barchart_travel_distance_pixel;
// Travel distance needed in pixel, 
// should equal to the 24 + width of bar (in case of vertical)
// should equal to the 24 + length of bar (in case of horizontal)
var barchart_travel_distance_pixel_needed_vertical, barchart_travel_distance_pixel_needed_horizontal;
// The whole bar chart = 100%, so need a breakpoint to separate the target slice and otherslice
var break_point_barchart, start_point_barchart, end_point_barchart;

// The bar should display only one time - flag
var displayed_one_time = false;
/* ---------------- CANVAS -------------- */
var cvs1, cvs2, cvs3, cvs4, cvs5, cvs6;
var ctx1, ctx2, ctx3, ctx4, ctx5, ctx6;
// Sketch variables
var barChart_vertical, barChart_horizontal;
// Breakpoint
var bar_target_height, bar_other_height, bar_target_width, bar_other_width;

/* For pre-questionnaire drawn */
var cvs_pre_bar, ctx_pre_bar;

/* For training errors and correct counter */
var current_training_times_errors_per_turn;
/* ---------------------------------------------------------------------------------------------------------------------------------------*/


/* Study 2 - Trajectory - bar */
// The max space in CM, used to do screen size check
var x_cm = 24, y_cm = 10;
// The max space in PIXEL, the max space needed in PIXEL
var x_pixel, y_pixel, x_pixel_needed, y_pixel_needed;

// The path count
var path_count_trial = 21, path_count_training = 14;
// The object to record the path for slow motion / fast motion for training/trial separately
var slow_path_trial = new Array(), fast_path_trial = new Array(), slow_path_training = new Array(), fast_path_training = new Array();
// The objec is unordered, in the experiment, we need random order, so upset the subscript
var trial_subscript = randomArray( generateSubscript(0, 21) ); var training_subscript =randomArray( generateSubscript(0, 14) );

// The array to record points number for each path
var path_point_count_slow_trial = new Array(), path_point_count_fast_trial = new Array(), path_point_count_slow_training = new Array(), path_point_count_fast_training = new Array();

// Array to record the timeslot of each path
// Only the time_slot_slow_trial and time_slot_fast_trial used in demo
// The value of one cell == 1600(duration allowed to display per stimulus) / the number of breakpoints of one path
var time_slot_slow_trial = new Array(), time_slot_fast_trial = new Array(), time_slot_slow_training = new Array(), time_slot_fast_training = new Array();

// Parameters to record pixel needed in X axis and Y axis, for donut and bar (horizontal) separately
var donut_pixel_needed_x, donut_pixel_needed_y;
var bar_horizontal_pixel_needed_x, bar_horizontal_pixel_needed_y;

// Parameters to do sketch
// Used in trial
var donut_trajectory, bar_horizontal_trajectory;
// Used In demo
var donut_trajectory_2, bar_horizontal_trajectory_2;
// Vision focus cross
var visionFocus;

// The duration allowed to display == 1600ms
var duration_allowed = 1600;

// Data density of each path - didn't use
var data_points_slow_motion = 125, data_points_fast_motion = 300;

/* Canvas added in both training and trial part for irregular movement */
// Trajectory - training
var cvs_tra_tra_2, cvs_tra_tra_ai_b, cvs_tra_tra_ai, cvs_tra_tra_ai_1, cvs_tra_tra_ai_2, cvs_tra_tra_ai_3;
var ctx_tra_tra_2, ctx_tra_tra_ai_b, ctx_tra_tra_ai, ctx_tra_tra_ai_1, ctx_tra_tra_ai_2, ctx_tra_tra_ai_3;
// Trajectory - trial
var cvs_tra_tri_3, cvs_tra_tri_ai_b, cvs_tra_tri_ai, cvs_tra_tri_ai_1, cvs_tra_tri_ai_2, cvs_tra_tri_ai_3;
var ctx_tra_tri_3, ctx_tra_tri_ai_b, ctx_tra_tri_ai, ctx_tra_tri_ai_1, ctx_tra_tri_ai_2, ctx_tra_tri_ai_3;

/* Variables about time */
// For the whole process
var lastFinishedTime_irregular, initializationTime_irregular, currrentTime_irregular;
// Only for vision focus 
var lastFinishedTime_visionfocus, initializationTime_visionfocus, deltaTime_visionfocus, endTime_visionfocus, visionfocus_duration_allowed = 1500, blankTime_after_visionfocus, blankTime_allowed = 500;
var endTime_wait;
// Only for stimuli
var lastFinishedTime_irregular_stimuli, initializationTime_irregular_stimuli, currentTime_irregular_stimuli, deltaTime_irregular_stimuli;
// Let the cross display for one second
var duration_allowed_visionfocus = 1000;
// The flag only used for first frame
var firstFrame_irregular = true;

// A group of variables to calculate the distance should travel according to the real time taken, in PIXEL
var x_previous_frame, y_previous_frame, x_current_set, y_current_set, x_target_frame, y_target_frame;
var distance_should_travel, distance_set, distance_travelled_totally = 0;
// The flag to switch the coordinate accumulation direction
var coordinatesPlus = false, coordinatesMinus = false;
// path_counter should be accumulated ourside the tBA function
var path_counter = 0, coordinate_counter; 
/* ---------------------------------------------------------------------------------------------------------------------------------------*/


/* Animation parameters */
// Frame independent
var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
var cancelAnimationFrame = window.cancelAnimationFrame || window.mozCancelAnimationFrame || window.webkitCancelAnimationFrame || window.msCancelAnimationFrame;
var callback_id, timeout_id = new Array(); // The animation request/setTimeout() id, used to cancel the previous one
// Time Based animation
// frequencyDelta is set to 1ms, means the minimum unit of displacement is based on 1ms
// deltaTime in moving case is used as micro time taken between two neighbour frames, in static case is used as a timer to count the duration that how long time the stimuli stays on the screen
// movingFlag == true if the speed != 0, movingFlag == false if the speed == 0
var currentTime, lastFinishedTime, deltaTime, frequencyDelta = 1, movingFlag;
/* ---------------------------------------------------------------------------------------------------------------------------------------*/

/* Canvas drawn parameters */
// Same for training part and trials
var donutGroup, afterImage;
// Trainig part
var myCanvas_training, context_training;
var myCanvas_training_blank, context_training_blank, myCanvas_training_afterimage, context_training_afterimage, myCanvas_training_afterimage_1, context_training_afterimage_1, myCanvas_training_afterimage_2, context_training_afterimage_2, myCanvas_training_afterimage_3, context_training_afterimage_3;
// Formal trial
var myCanvas_trial, context_trial;
var myCanvas_blank, context_blank, myCanvas_afterimage, context_afterimage, myCanvas_afterimage_1, context_afterimage_1, myCanvas_afterimage_2, context_afterimage_2, myCanvas_afterimage_3, context_afterimage_3;
/* ---------------------------------------------------------------------------------------------------------------------------------------*/

/* Color definition*/
var condition_color = "#E90738" // The light red with high chroma
var other_color = "#C3C1C1" // The light grey
/* ---------------------------------------------------------------------------------------------------------------------------------------*/

/* Trail's speed == Training's speed */
// Trial's speed order == Trianing's speed order
// negatif  => irregular movement, positif => linear movement (baseline)
// -30 => fast trajectories, -15 => slow trajectories, 15 => slow linear motion, 30 => fast linear motion
var original_speed_array = [-30, -15, 15, 30]; // in CM, if this changes, threshold_part needs to be changes = the count of speed
// Traverse all possible permutations (latin square)
var permutation_speed_array = new arrayPermutation(original_speed_array);
permutation_speed_array.start();
// Generate one element from permutations randomly
// var random_speed_array = randomArray(original_speed_array);
var random_speed_array = getArrayElementsRandomly(permutation_speed_array.result, 1); // Once the number of elements changed, the following for-loop to match the name-string with the speed-number should be modified, the array-dimension changes from 1 to 2
// Trial's speed order string
var random_speed_array_string = [];
for(let i=0;i<random_speed_array.length; i++){
	// -30 => fast trajectories
	if(random_speed_array[i] == original_speed_array[0]){
		random_speed_array_string.push("30-fast-irregular-movement");
	}else if(random_speed_array[i] == original_speed_array[1]){
		random_speed_array_string.push("15-slow-irregular-movement");
	}else if(random_speed_array[i] == original_speed_array[2]){
		random_speed_array_string.push("15-slow-linear-movement");
	}else if(random_speed_array[i] == original_speed_array[3]){
		random_speed_array_string.push("30-fast-linear-movement");
	}
}

// console.log("original_speed_array: " + original_speed_array);
// console.log("random_speed_array: " + random_speed_array);
// console.log("random_speed_array_string: " + random_speed_array_string);
/* ---------------------------------------------------------------------------------------------------------------------------------------*/

/* Trial's static stimuli displaying time == Training's static stimuli displaying time */
// For now, we only have 1 group of static stimuli, if we have more than 1 group, just add the static_displaying_time_x below,
// and don't forget to change the condition to enable the button in checkDoneButton(), 
// add the case in the classification of (speed == 0) in timeBasedAnimation()
var static_displaying_time_1 = 1600; // in MS, this one still need to be discussed
/* ---------------------------------------------------------------------------------------------------------------------------------------*/

/* Trial's stimuli percentage definition */
var original_percentage_array = [0.18, 0.32, 0.43, 0, 0.58, 0.72, 0.83];
// Generate 3 random groups of array according to the original one
var random_percentage_array_1, random_percentage_array_2, random_percentage_array_3;
// The first number and the last number should not equal to 0.50 = 50%, which is the attention-checking number
// The first number of the following group should not be the same as the last number of the previous group 
for(;;){
	random_percentage_array_1 = randomArray(original_percentage_array);
	if((random_percentage_array_1[0] != original_percentage_array[3]) && (random_percentage_array_1[random_percentage_array_1.length-1] != original_percentage_array[3])){
		break;
	}
}
for(;;){
	random_percentage_array_2 = randomArray(original_percentage_array);
	if((random_percentage_array_2 != random_percentage_array_1) && (random_percentage_array_2[0] != random_percentage_array_1[random_percentage_array_1.length-1]) && (random_percentage_array_2[0] != original_percentage_array[3]) && (random_percentage_array_2[random_percentage_array_2.length-1] != original_percentage_array[3])){		
		break;
	}
}
for(;;){
	random_percentage_array_3 = randomArray(original_percentage_array);
	if((random_percentage_array_3 != random_percentage_array_2) && (random_percentage_array_3 != random_percentage_array_1) && (random_percentage_array_3[0] != random_percentage_array_2[random_percentage_array_2.length-1]) && (random_percentage_array_3[0] != original_percentage_array[3]) && (random_percentage_array_3[random_percentage_array_3.length-1] != original_percentage_array[3])){
		break;
	}
}

var random_percentage_array = [random_percentage_array_1, random_percentage_array_2, random_percentage_array_3];
// console.log("random_percentage_array = " + random_percentage_array);
// console.log("random_percentage_array_1 = " + random_percentage_array_1);
// console.log("random_percentage_array_2 = " + random_percentage_array_2);
// console.log("random_percentage_array_3 = " + random_percentage_array_3);
/* ---------------------------------------------------------------------------------------------------------------------------------------*/

/* Trial's repetition and part */
var threshold_part = original_speed_array.length; // in TIMES
var current_part = undefined;
// flag to record if it is one part it is the end, endTrialPart == true if current_part == threshold_part-1, enTrialPart == false if not
var endTrialPart = false;

var threshold_repetitions = random_percentage_array.length; // in TIMES
var current_repetition = undefined;
// flag to record if one repetition is end or not, endTrialRepetition == true if current_repetition == threshold_repetitions-1, endTrialRepetition == false if not
var endTrialRepetition = false;

var threshold_trial_per_repetition = original_percentage_array.length; // threshold_trial = number of stimuli * number of repetition * number of speed
var current_trial_per_repetition = undefined;
// flag to record if a set of trial per repetition is end or not, endTrialPerRepetition == true if current_trial_per_repetition == threshold_trial-1, endTrialPerRepetition == false if not
var endTrialPerRepetition = false;

var threshold_trial = original_speed_array.length * random_percentage_array.length * original_percentage_array.length;
var current_trial = undefined;
// flag to record is one repetition is end or not, endExperiment == true if current_trial == 63, endExperiment == false if not
var endExperiment = false;

/* Trial answered time taken and given answer */
// Answer time taken == from the first time enter the answer in the text area to the click the Done button
var trial_answered = false;
var start_answer_time, start_type_time, end_answer_time;
// submit_millisecond is to record that the subject spends how much time to submit one answer
// submit_millisecond == end_answer_time - start_type_time;
// type_milliseconds is to record that the subject spends how much time to give one answer and start to type in the textare
// type_milliseconds == start_type_time - start_answer_time;
var type_milliseconds, submit_milliseconds;
// trial_given_answer is to record the answer given from subject
var trial_given_answer;

/* Stimuli display duration, afteriamge block display duration */
// display_millisecond is to record how long time the stimuli displays on the screen
// initializationTime is the time to start the animation
var display_millisecond = null, initializationTime;
// waitDuration is the threshold that how long time should wait before the donut displays
// waitDeltaTime is to calculate how long time has waited from the initialization
var waitDuration, waitDeltaTime;
/* ---------------------------------------------------------------------------------------------------------------------------------------*/

/* Donut chart drawn parameters -- same for training part and trials */
var start_point = -0.25*(2*Math.PI); // start from the 12 o'clock point
var break_point; // The point to change the color
var end_point = 0.75*(2*Math.PI);
/* ---------------------------------------------------------------------------------------------------------------------------------------*/

/* Training's times */
var threshold_training_turns = original_speed_array.length; // in TIMES
var current_training_turn = undefined;
var training_times_per_turn = 6; // in TIMES
var current_training_times_per_turn = undefined;
/* ---------------------------------------------------------------------------------------------------------------------------------------*/

/* Training's stimuli percentage -> [0, 100], formal stimuli not included*/
var training_percentage;
/* ---------------------------------------------------------------------------------------------------------------------------------------*/

/* Confidence survey */
// confidence_survey_answer is to record the answer, 
// confidence_survey_answered == true if subject answered, == false if not
var confidence_survey_answer = -1, confidence_survey_answered = false;
/* ---------------------------------------------------------------------------------------------------------------------------------------*/

/* Questionnaire */
// xxx_answer is to record the answer, 
// xxx_answered == true if subject answered, == false if not
// Pre-questionnaire
var experienced_answer = -1, frequency_answer = -1;
var experienced_answered = false, frequency_answered = false;
// Post-questionnaire
// var no_movement_answer = -1, slow_movement_answer = -1, fast_movement_answer = -1;
// var no_movement_answered = false, slow_movement_answered = false, fast_movement_answered = false;

// For study 2 - trajectory
var slow_linear_movement_answer = -1, slow_curve_movement_answer = -1, fast_linear_movement_answer = -1, fast_curve_movement_answer = -1;
var slow_linear_movement_answered = false, slow_curve_movement_answered = false, fast_linear_movement_answered = false, fast_curve_movement_answered = false;
/* ---------------------------------------------------------------------------------------------------------------------------------------*/

/* Done button can be pressed flag */
var checkDoneButtonFire = false, startExperimentButtonFire = false;
/* ---------------------------------------------------------------------------------------------------------------------------------------*/

/* Attention check counter */
// We have 3*4=12 times of attention check, the exact answer = 0, we can accept <=7 times errors, we will block the participant from the 8th error
var attentioncheck_threshold = 7, current_failed_times = 0; 
/* ---------------------------------------------------------------------------------------------------------------------------------------*/

/* Resolution */
var resolution_height, resolution_width;
/* ---------------------------------------------------------------------------------------------------------------------------------------*/


/* Functions  part */
// Generate the subscript array
// @param start_point is the array[0]
// @param count is the length of the array
function generateSubscript(start_point, count){
	let arr = new Array();
	for(i=0; i<count; i++){
		arr[i] = start_point + i;
	}

	return arr;
}
// Upset the array
function randomArray(arr){
	// Duplicate the arr, the aim is to not change the orignial arr, only change the duplicated array
	let array = arr.concat();
	let len = array.length;
    for (let i = 1; i < len; i++) {
	    const random = Math.floor(Math.random() * (i + 1));
	    [array[i], array[random]] = [array[random], array[i]];
	}
    return array;
}

// Get X elements randomly from the given array
// arr is the given array, count is the number of elements wanted
function getArrayElementsRandomly(arr, count){
	let array = arr.concat();
	let shuffled = array.slice(0), len = array.length, min = len-count, temp, index;
	while (len-- > min) {
        index = Math.floor((len + 1) * Math.random());
        temp = shuffled[index];
        shuffled[index] = shuffled[len];
        shuffled[len] = temp;
    }
    if(count == 1){
    	return shuffled.slice(min)[0]; // The output is a 1-dimension array
    }else{
    	return shuffled.slice(min); // The output is a 2-dimension array
	}
}

// Array permutation
function arrayPermutation(array){
	this.len = 0;    // record the times of permutation
    this.arr = array.concat();   // duplicate the array
    this.result = [];    // record the result of permutation

    // The initial methode to creat object
    if (typeof arrayPermutation.run == 'undefined') {
        arrayPermutation.prototype.start = function() {
            this.run(0);
        }

        // Recursion - Core methode, index is the subscript of array
        arrayPermutation.prototype.run = function(index) {
            // When traverse to the end of the array, save the result in the array[Result], the time of permutation++
            if (index == this.arr.length - 1) {
                this.result.push(this.arr.slice());
                this.len++;
                return;
            }

            for(let i = index; i < this.arr.length; i++) {
                this.swap(this.arr, index, i);      // exchange the position with the number whose subscript is index
                this.run(index + 1);                // the reset elements redo the permutation
                this.swap(this.arr, index, i);      // reset the array
            }
        }

        // exchange the position
        arrayPermutation.prototype.swap = function(array, i, j) {
            var t;
            t = array[i];
            array[i] = array[j]; 
            array[j] = t;
        }
    }
}

// Generate random percentage number in the a range of [minNum,maxNum], all percenatges for formal trials not included
// minNum is the minmum one(included) in the range, maxNum is the maximum one(included) in the range
// arr is an array whose elements should not be generated
// the input should like (0, 100, arrayNumber), the output is in format like 0.94, which means 94%
function randomNumber(minNum, maxNum, arr){
	let array = arr.concat();
	let len = array.length;
	let final_random_number;
	let random_number = (Math.round(Math.random() * (maxNum - minNum)) + minNum).toFixed(); // Generate a random number in the range, take only the entire part, discard the decimal part
	for(let i=0;i<len;i++){
		if(random_number != (array[i]*100).toFixed()){
			final_random_number = random_number;
		}else if(random_number === (array[i]*100).toFixed()){
			final_random_number  = undefined;
			random_number = (Math.round(Math.random() * (maxNum - minNum)) + minNum).toFixed();
			i = 0; // Restart from the beginning
		}
	}
	return (final_random_number/100).toFixed(2);
}

// Generate an entier random number in the range of [minNum, maxNum]
function randomNumberEasiest(minNum, maxNum){
	let random_number = (Math.round(Math.random() * (maxNum - minNum)) + minNum).toFixed(); 
	return random_number;
}


// Calculate the break_point value
function breakpointValue(percentage, start_point){
	let value = percentage*(2*Math.PI) + start_point;
	return value;
}

/* ---------------------------------------------------------------------------------------------------------------------------------------*/


/* Visualized object parameters */
// If there is a soccerball
// var myImageSB = new Image();
// myImageSB.src = "../html/img/png/soccerball1.png";
// ctx => skecth on which context
// per => how much percentage should be displayed
// x => x coordinate, the donut chart center
// y => y coordinate, the donut chart center
// sw => slice weight, how thickness the circle is
// dr => donut chart radius, how big the donut chart is
// img => boolean, if there is a picture in the middle of donut (== true), if not (== false)
// img_w => image width
// img_h => image height
function Sketch(ctx, per, x, y, sw, dr, img, img_w, img_h){
    ctx.beginPath();
    ctx.arc(x, y, dr, start_point, per);
    ctx.strokeStyle = condition_color;
    ctx.lineWidth = sw;
    ctx.stroke();

    ctx.beginPath();
    ctx.arc(x, y, dr, per, end_point);
    ctx.strokeStyle = other_color;
    ctx.lineWidth = sw;
    ctx.stroke();

    if(img){
      ctx.drawImage(myImageSB, x-img_w/2, y-img_h/2, img_w, img_h);
    }
}
/* ---------------------------------------------------------------------------------------------------------------------------------------*/

/* Sketch a single horizontal bar chart, with or without thw soccer */
function horizontalBarSkecth(ctx, x_target, y, x_target_length, whole_length, y_height, img, img_w, img_h, gap){
	// Sketch the target slice
    ctx.fillStyle=condition_color;
    ctx.fillRect(x_target, y, x_target_length, y_height);
    // Sketch the other slice
    ctx.fillStyle=other_color;
    ctx.fillRect(x_target + x_target_length, y, whole_length - x_target_length, y_height);

    if(img){
    	ctx.drawImage(myImageSB, (whole_length-img_w)/2, y_height + gap, img_w, img_h);
    }
} 