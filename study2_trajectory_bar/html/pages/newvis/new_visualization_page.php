<style type="text/css">
	/* Training part */
	#training_drawn{
		position: relative;
		text-align: center;
		margin: auto;
	}

	#canvas_0{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#training_afterImage{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#training_afterImage_1{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#training_afterImage_2{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#training_afterImage_3{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#training_blank{
		position: relative;
		text-align: center;
		margin: auto;	
	}

	#canvas_2{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#irregular_training_blank{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#irregular_training_afterImage{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#irregular_training_afterImage_1{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#irregular_training_afterImage_2{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#irregular_training_afterImage_3{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#irregular_training_blank{
		position: relative;
		text-align: center;
		margin: auto;	
	}
	#training_answer{
		position: relative;
		text-align: center;
		margin: auto;
	}
	/* ------------------------------ */

	/* Trial part */
	#trial_drawn{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#canvas_1{
		/*position: fixed;  /* Fixed: position relative to the broswer, won't move with the scrolling of mouse; Relative: position relative to the previous element;*/
		/*top: 15%; */ /* top: 15%; position at the 15% far away from the top border of broswer */
		position: relative;
		text-align: center; /* let the elements inside this div align-center */
		/*left: 0px; */ /*set left = right = X px; margin: auto; is to make the div at the center of vertical line */
		/*right: 0px; */
		margin: auto;
	}
	#trial_afterImage{
		/*position: fixed;
		top: 15%;
		left: 0px;
		right: 0px;
		margin: auto;*/
		position: relative;
		text-align: center;
		margin: auto;
	}
	#trial_afterImage_1{
		/*position: fixed;
		top: 15%;
		left: 0px;
		right: 0px;
		margin: auto;*/
		position: relative;
		text-align: center;
		margin: auto;
	}
	#trial_afterImage_2{
		/*position: fixed;
		top: 15%;
		left: 0px;
		right: 0px;
		margin: auto;*/
		position: relative;
		text-align: center;
		margin: auto;
	}
	#trial_afterImage_3{
		/*position: fixed;
		top: 15%;
		left: 0px;
		right: 0px;
		margin: auto;*/
		position: relative;
		text-align: center;
		margin: auto;
	}
	#trial_blank{
		/*position: fixed;
		top: 15%;
		left: 0px;
		right: 0px;
		margin: auto;*/
		position: relative;
		text-align: center;
		margin: auto;	
	}

	#canvas_3{
		/*position: fixed;  /* Fixed: position relative to the broswer, won't move with the scrolling of mouse; Relative: position relative to the previous element;*/
		/*top: 15%; */ /* top: 15%; position at the 15% far away from the top border of broswer */
		position: relative;
		text-align: center; /* let the elements inside this div align-center */
		/*left: 0px; */ /*set left = right = X px; margin: auto; is to make the div at the center of vertical line */
		/*right: 0px; */
		margin: auto;
	}
	#irregular_trial_afterImage{
		/*position: fixed;
		top: 15%;
		left: 0px;
		right: 0px;
		margin: auto;*/
		position: relative;
		text-align: center;
		margin: auto;
	}
	#irregular_trial_afterImage_1{
		/*position: fixed;
		top: 15%;
		left: 0px;
		right: 0px;
		margin: auto;*/
		position: relative;
		text-align: center;
		margin: auto;
	}
	#irregular_trial_afterImage_2{
		/*position: fixed;
		top: 15%;
		left: 0px;
		right: 0px;
		margin: auto;*/
		position: relative;
		text-align: center;
		margin: auto;
	}
	#irregular_trial_afterImage_3{
		/*position: fixed;
		top: 15%;
		left: 0px;
		right: 0px;
		margin: auto;*/
		position: relative;
		text-align: center;
		margin: auto;
	}
	#irregular_trial_blank{
		/*position: fixed;
		top: 15%;
		left: 0px;
		right: 0px;
		margin: auto;*/
		position: relative;
		text-align: center;
		margin: auto;	
	}

	#trial_answer{
		/*position: fixed;
		width: 1100px;
		top: 30%;*/
		position: relative;
		text-align: center;
		margin: auto;
	}
	/* ------------------------------ */

	/* input = number, scrolling the mouse or click up & down are not allowed*/
	input::-webkit-outer-spin-button,input::-webkit-inner-spin-button{
        -webkit-appearance:textfield;
  	}

  	input[type="number"]{
        -moz-appearance:textfield;
	}
	/* ------------------------------ */

</style>

<div id="row">
	<div id="col">
		<!-- Training part -->
		<div class = "page" id = "training_part" style="display: block;">

			<div class = "section" id ="training_explanation" style="display: block;">
				<h2 id = "training_title_1"></h2>
				<p id = "training_first_sentence" style = "font-weight: bold;"></p>
				<ul>
					<li> <input type="checkbox" class="training_checkbox"> &nbsp;&nbsp; You will be shown the same bar chart as before but <span id="training_bold_1" style="font-weight: bold;">the percentage (%) in <span style="color: #E90738; font-weight: bold;">red</span> will vary each time</span>.</li>
					<li>  <input type="checkbox" class="training_checkbox"> &nbsp;&nbsp; In this training, you will see the bar chart <span id = "training_speed_discription" style="font-weight: bold;"></span> on the screen, it will <span id="training_bold_2" style="font-weight: bold;">display only 1 time</span>.</li>
					<li>  <input type="checkbox" class="training_checkbox"> &nbsp;&nbsp; Only during training - each time you submit an answer we will tell you what the exact answer would have been. It is ok if you did not get the answer exactly correct, but you should be somewhere close. </li>
					<li>  <input type="checkbox" class="training_checkbox"> &nbsp;&nbsp; To pass this training, you need to get <span id="training_bold_3" style = "font-weight: bold;">at least 6 of the questions correct</span>.</li>
					<li> <input type="checkbox" class="training_checkbox"> &nbsp;&nbsp; Again, <span id="training_bold_4" style = "font-weight: bold;">make a quick but good guess of the right answer</span>, but do not try to measure or calculate correct answers. We want to learn what numbers you intuitively see.</li>
				</ul>
			</div>

			<div class = "section" id = "training_button_1" style="display: none;">
				<p style = "font-weight: bold;"> If you are ready, please click the button below: </p>
				<button id = "start_training_button" style="background-color: #006400; color: #FFFFFF;" onclick = "startTraining(this)"></button>	
			</div>

			<div class = "section" id = "training_stimuli" style="display: none;">
				<h2 id = "training_title_2"></h2>

				<!-- Training page ending module -->
				<div class = "pages" id = "training_button_2" style="display: none; border: solid; border-color: red; border-width: 2px; height: 115px;margin-top: 20px; margin-bottom: 20px; padding-top: 5px;">
					<ul>
						<li> If you want to do <span style="font-weight: bold;">more training</span>, you can <span style="font-weight: bold;">stay on this page</span> and keep training until you are ready to go into the experiment. </li>
						<li style = "font-weight: bold;"> If you are ready to start the experiment, please click the button below: </li>
					</ul>
					<button id = "start_experiment_button" style="background-color: #006400; color: #FFFFFF; margin-right: 20px;" onclick = "startExperiment(this)"></button>
				</div>
				
				<div clas = "section" id = "training_drawn">
					<!-- The canvas set for linear motion -->
					<canvas id = "canvas_0" style="display: none; background-color: #FFFFFF"></canvas>
					<canvas id = "training_blank" style="display: none; background-color: #FFFFFF"></canvas>
					<canvas id = "training_afterImage" style="display: none; background-color: #FFFFFF"></canvas>
					<canvas id = "training_afterImage_1" style="display: none; background-color: #FFFFFF"></canvas>
					<canvas id = "training_afterImage_2" style="display: none; background-color: #FFFFFF"></canvas>
					<canvas id = "training_afterImage_3" style="display: none; background-color: #FFFFFF"></canvas>
					<!-- The canvas set for irregular movement -->
					<canvas id = "canvas_2" style="display: none; background-color: #FFFFFF"></canvas>
					<canvas id = "irregular_training_blank" style="display: none; background-color: #FFFFFF"></canvas>
					<canvas id = "irregular_training_afterImage" style="display: none; background-color: #FFFFFF"></canvas>
					<canvas id = "irregular_training_afterImage_1" style="display: none; background-color: #FFFFFF"></canvas>
					<canvas id = "irregular_training_afterImage_2" style="display: none; background-color: #FFFFFF"></canvas>
					<canvas id = "irregular_training_afterImage_3" style="display: none; background-color: #FFFFFF"></canvas>
				</div>
				<div class = "section" id = "training_answer">
					<p>Tell us at what percentage (%) the <span style="color: #E90738; font-weight: bold;">red</span> section had:</p>
					<input type="number" id="training_answer_given" onmousewheel="return false;" onkeyup="value=value.replace(/[^\d]+/g,'')" placeholder="Please enter your answer here" step="1" min="0" max="100" style="width:250px; text-align: center" required autofocus oninput="checkDoneButton(this.value, true)">
					
					<!-- <input type="button" id="training_done_button" value="Please give an answer" disabled="disabled" onclick="nextTraining(this)"> -->
					<input type="button" id="training_done_button" value="Please give an answer" disabled="disabled" data-toggle="modal" data-target="#training_answer_check" onclick="checkAnswer(this)">
				</div>
		
				<!-- ------------------------------------ -->

				<!-- Check answer module -->
				<div class="modal fade" id="training_answer_check" tabindex="-1" role="dialog" aria-labelledby="training_answer_check_label" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h4 class="modal-title" id="training_answer_check_label">Answer Feedback</h4>
							</div>

							<div class="modal-body">
								<p id = "answer_feedback"></p>
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-success" data-dismiss="modal" id="training_answer_check_button" onclick="nextTraining(this)">Close and Continue Training</button>
							</div>
								
						</div>
					</div>
				</div>
				<!-- ------------------------------------ -->

				<!-- Training page ending module -->
				<!-- <div class = "pages" id = "training_button_2" style="display: none; border: solid; border-color: red; border-width: 2px; height: 115px;margin-top: 20px; margin-bottom: 20px; padding-top: 5px;">
					<ul>
						<li> If you want to do <span style="font-weight: bold;">more training</span>, you can <span style="font-weight: bold;">stay on this page</span> and keep training until you are ready to go into the experiment. </li>
						<li style = "font-weight: bold;"> If you are ready to start the experiment, please click the button below: </li>
					</ul>
					<button id = "start_experiment_button" style="background-color: #006400; color: #FFFFFF; margin-right: 20px;" onclick = "startExperiment(this)"></button>
				</div> -->
			</div>
				
		</div>
		
		<!-- ------------------------------------------------------------------------------------------------------------------------------------------------- -->

		<!-- Stimuli part -->
        <!-- The width of #trial_stimuli = #trial_drawn = #trial_answer -->
        <!-- #trial_stimuli is align-left, #trial_drawn and #trial_answer are align-center -->
		<div class = "page" id = "trial_stimuli" style="display: none;">
			<h2 id = "experiment_title"></h2>
			<!-- The height of #trial_drawn is 1.5 mutiples higher than the height of .canvas, to avoid the tiny scale offset -->
			<div class = "section" id = "trial_drawn">
				<!-- The canvas for linear motion -->
				<canvas id = "canvas_1" style="display: none; background-color: #FFFFFF"></canvas>
				<canvas id = "trial_blank" style="display: none; background-color: #FFFFFF"></canvas>
				<canvas id = "trial_afterImage" style="display: none; background-color: #FFFFFF"></canvas>
				<canvas id = "trial_afterImage_1" style="display: none; background-color: #FFFFFF"></canvas>
				<canvas id = "trial_afterImage_2" style="display: none; background-color: #FFFFFF"></canvas>
				<canvas id = "trial_afterImage_3" style="display: none; background-color: #FFFFFF"></canvas>
				<!-- The canavs for irregular movement -->
				<canvas id = "canvas_3" style="display: none; background-color: #FFFFFF"></canvas>
				<canvas id = "irregular_trial_blank" style="display: none; background-color: #FFFFFF"></canvas>
				<canvas id = "irregular_trial_afterImage" style="display: none; background-color: #FFFFFF"></canvas>
				<canvas id = "irregular_trial_afterImage_1" style="display: none; background-color: #FFFFFF"></canvas>
				<canvas id = "irregular_trial_afterImage_2" style="display: none; background-color: #FFFFFF"></canvas>
				<canvas id = "irregular_trial_afterImage_3" style="display: none; background-color: #FFFFFF"></canvas>
			</div>
			<div class = "section" id = "trial_answer">
				<p>Tell us at what percentage (%) the <span style="color: #E90738; font-weight: bold;">red</span> section had:</p>
					<input type="number" id="trial_answer_given" onmousewheel="return false;" onkeyup="value=value.replace(/[^\d]+/g,'')" placeholder="Please enter your answer here" step="1" min="0" max="100" style="width:250px; text-align: center" required autofocus oninput="checkDoneButton(this.value, false)">
					
					<input type="button" id="trial_done_button" value="Please give an answer" disabled="disabled" onclick="nextTrial(this)">
			</div>					
		</div>
		<!-- ------------------------------------------------------------------------------------------------------------------------------------------------- -->

		<!-- Confidence survey part -->
		<div class = "page" id = "confidence_survey" style="display: none;">
			<h2 id = "confidence_survey_title"></h2>
			<div class = "section" id = "confidence_survey_discription">
				<p>You have completed the <span id = "confidence_survey_count"></span> set of trials. Please rate your confidence in the correctness of your responses for the last block of answers.</p>
				<p>How confidence are you in the correctness of your responses?</p>

				<table>
					<thead>
						<tr>
							<th></th>
							<th class="likert">1</th>
							<th class="likert">2</th>
							<th class="likert">3</th>
							<th class="likert">4</th>
							<th class="likert">5</th>
							<th></th>
						</tr>			
					</thead>
					<tbody>
						<tr>
							<td>Not at all confident&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<td class="likert">
								<input type="radio" name="condifence_survey_radio" value="1" class="condifence_survey_radio validates-required validates">
							</td>
							<td class="likert">
								<input type="radio" name="condifence_survey_radio" value="2" class="condifence_survey_radio validates-required validates">
							</td>
							<td class="likert">
								<input type="radio" name="condifence_survey_radio" value="3" class="condifence_survey_radio validates-required validates">
							</td>
							<td class="likert">
								<input type="radio" name="condifence_survey_radio" value="4" class="condifence_survey_radio validates-required validates">
							</td>
							<td class="likert">
								<input type="radio" name="condifence_survey_radio" value="5" class="condifence_survey_radio validates-required validates">
							</td>
							<td>&nbsp;&nbsp;Very confident</td>
						</tr>					
					</tbody>				
				</table>
			</div>

			<div class = "section" id = "confidence_survey_button" style="display: none;">
				<button id = "submit_confidence_survey_button" style="background-color: #006400; color: #FFFFFF" onclick = "submitConfidenceSurvey(this)"></button>
			</div>
			
		</div>
		<!-- ------------------------------------------------------------------------------------------------------------------------------------------------- -->

		<!-- Vision focus part -->
		<!-- <div class = "page" id = "vision_focus" style="display: none; position: relative; top: 78px; width: 1100px; text-align: center;">
			<div id = "vision_focus_box_parent" style="text-align: center; width: 1100px;">
				<div id = "vision_focus_box_son" style="border: solid; background: #FFFFFF; color: #000000; text-align: center;">
					<p style="margin: 0px;">The bar will display here only one time within a brief amount of time. Please pay attention and let your vision focus on this area.</p>
				</div>
			</div>

			<input type="button" id="vision_focus_button" disabled="disabled" value = "You can click the button after 5s" style="background-color: #EDEDED; color: #A3A3A3;">
		</div> -->
		<!-- ------------------------------------------------------------------------------------------------------------------------------------------------- -->

	</div>
</div>

<script type="text/javascript">

//disable the next button for the moment
document.addEventListener("DOMContentLoaded", function(){
                                                  			let nextbutton = document.getElementById("btn_<?php echo $id;?>");
                                                  			nextbutton.style.display = 'none';
                                                		});

//Vision part variables 
var waitTime_vis = 4;
var btn_vis = false;
var training_trial_count = 0;

// vision_focus_button can be clicked after X second(s)
// var callback_id_vis = setInterval(function(){
//   if(btn_vis){
//                   if(waitTime_vis == 0){
//                     let nextButton = document.getElementById("vision_focus_button");
//                     nextButton.style.background = "#006400";
//                     nextButton.style.color = "#FFFFFF";
//                     nextButton.value = "I'm ready."
//                     nextButton.disabled = false;
//                   }else{
//                   	let nextButton = document.getElementById("vision_focus_button");
//                     nextButton.value = "You can click the button after "+ waitTime_vis +"s";
//                     waitTime_vis--;
//                   }
//   }
// }, 1000);

/* No more vision focus page, after Task description, go to training part 1, click start trianing button, go to training canvas set directly */
// When click the vision_focus_button
// $(document).on('click','#vision_focus_button',function(){
// 	// console.log("click one vision_focus_button");
// 	// Clean the setInterval
//     clearInterval(callback_id_vis);
//     // btn_vis = false
//     btn_vis = false;

//     // Start Training or trial
//     if(training_trial_count == 1 || training_trial_count == 3 || training_trial_count == 5){
//     // For Training 
//     	document.getElementById("vision_focus").style.display = 'none';
//     	document.getElementById("training_part").style.display = 'block';

//     	/* Prepare the training stimuli and call for animation */
// 		training_percentage = randomNumber(0, 100, original_percentage_array);
// 		// console.log("training_percentage  = " + training_percentage);
		
// 		/* Call for animation */
// 		lastFinishedTime = initializationTime = Date.now();
// 		displayed_one_time = false;
// 		callback_id = requestAnimationFrame(function (){timeBasedAnimation(myCanvas_training, context_training, random_speed_array[current_training_turn], training_percentage)});
// 	// For trial
//     }else if(training_trial_count == 2 || training_trial_count == 4 || training_trial_count == 6){
// 		// Reset waitDuration
// 		waitDuration = 200;
// 		// Clear the previous draw
// 		context_trial.clearRect(0,0,myCanvas_trial.width,myCanvas_trial.height);
// 		// Reset canvas
// 		resetCanvasDisplay(myCanvas_trial, myCanvas_blank, myCanvas_afterimage, myCanvas_afterimage_1, myCanvas_afterimage_2, myCanvas_afterimage_3);

//     	// For trial
//     	document.getElementById("vision_focus").style.display = 'none';
//     	document.getElementById("trial_stimuli").style.display = 'block';

//     	/* Prepare the trial stimuli and call for animation */
// 		start_answer_time = lastFinishedTime = initializationTime = Date.now();
// 		displayed_one_time = false;
// 		// console.log("start_answer_time: " + start_answer_time);
// 		callback_id = requestAnimationFrame(function (){timeBasedAnimation(myCanvas_trial, context_trial, random_speed_array[current_part], random_percentage_array[current_repetition][current_trial_per_repetition])}); 	
//     }    
// });

// Check if all the training reminders have been well checked, if so, go to training canvas set
$(".training_checkbox").click(function(){
	// console.log("click one training_checkbox");
	let len = $(".training_checkbox:checked").length;
	// console.log("len = " + len);
	if(len==5){
		document.getElementById("training_button_1").style.display = "block";
		// console.log("training check box checked and button display");
	}else{
		document.getElementById("training_button_1").style.display = "none";
	}
})

function cleanTrainingCheckbox(){
	let x = document.getElementsByClassName("training_checkbox");
	for(let i=0; i<x.length; i++){
		if(x[1].checked){
			x[1].checked = false;
		}
	}
	// Disable the start training button
	document.getElementById("training_button_1").style.display = "none";
}

/* Canvas, Frame independent, and Time based Animation */
// Linear motion - This funciton based on requestAnimationFrame() API, which gives hand to the broswer to update the content when frame refreshes, inside of setting the framerate in the code(in this case, sometimes the real framerate cannot achieve to the setting value because of the equipement). And then it cut the time interval by 1ms, which means the minmum step considered in this function is 1ms.
// @param canvas, ctx are parameters indicate sketching on which canvas
// @param speed is how fast the visualization will move at, in CM/S
// @param percentage is how much the percentage is of the target slice

function timeBasedAnimation(canvas, ctx, speed, percentage){

	currentTime = Date.now();
	waitDeltaTime =  currentTime - initializationTime;

	// deltaTime = currentTime - lastFinishedTime;
	// // Clear the previous draw
	// ctx.clearRect(0,0,canvas.width,canvas.height);

	if(waitDeltaTime < waitDuration){
		endTime_wait = Date.now();
		callback_id = requestAnimationFrame(function (){timeBasedAnimation(canvas, ctx, speed, percentage)});
	}else{

		// // Prepare the length of two sections (horizontal bar chart)
		// var target_lengt_vis = percentage * barchart_length;
		// var other_length_vis =  barchart_length - target_lengt_vis;
		
		// // Redraw with the new coordinates
		// barChart_horizontal.draw(speed, ctx, other_length_vis, target_lengt_vis);
		// donutGroup.draw(ctx, breakpointValue(percentage, start_point), speed);
		
		// Get the current time
		// currentTime = Date.now();
		// Calculate the delta time (ms) between last time call and present time call
		// deltaTime is used to move the dount
		// deltaTime = currentTime - lastFinishedTime;

		// For moving case, that the x coordianate will be changed according to the time, at the end of a round trip, Blink() fires, and then the moving direction and x coordinate will be reset
		// The fast motion case, when the donut chart touches the far left border, go back 
		if(speed == 30){

			visionFocus.x = visionFocus.y = 0.25*oneCM;
			visionFocus.draw(canvas, ctx);

			deltaTime_visionfocus = currentTime - endTime_wait;

			if(deltaTime_visionfocus < visionfocus_duration_allowed){
				endTime_visionfocus = Date.now();
				callback_id = requestAnimationFrame(function (){timeBasedAnimation(canvas, ctx, speed, percentage)});
			// Vision focus should disappear
			}else{
				// Clear the previous draw
				ctx.clearRect(0,0,canvas.width,canvas.height);
				
				blankTime_after_visionfocus = currentTime - endTime_visionfocus;
				
				if(blankTime_after_visionfocus < blankTime_allowed){
					start_answer_time = lastFinishedTime = Date.now();
					callback_id = requestAnimationFrame(function (){timeBasedAnimation(canvas, ctx, speed, percentage)});
				}else{

					deltaTime = currentTime - lastFinishedTime;
					// Clear the previous draw
					ctx.clearRect(0,0,canvas.width,canvas.height);

					// Prepare the length of two sections (horizontal bar chart)
					var target_lengt_vis = percentage * barchart_length;
					var other_length_vis =  barchart_length - target_lengt_vis;

					// Redraw with the new coordinates
					barChart_horizontal.draw(speed, ctx, other_length_vis, target_lengt_vis);

					// Set the moving flag, this parameter is used to identify the donut chart is moving or is static
					movingFlag = true;
					// Set the x coordinate and the moving direction according to the different speeds
					if ( barChart_horizontal.direction_high_speed == 1 ){
					    // (speed) is X cm/s, (speed*oneCM) is X pixel/s, (speed*oneCM/1000) is X pixel/ms, 
					    // (speed*oneCM/1000*frequencyDelta) is the displacement ( = X pixel) during 1 ms 
					    // (speed*oneCM/1000*frequencyDelta*deltaTime) is the displacement ( = X*deltaTime pixel) during one frame
					    // The duration of one frame = deltaTime = currentTime - lastFinishedTime
					    barChart_horizontal.x_high_speed += speed*oneCM/1000*frequencyDelta*deltaTime;
					}else if ( barChart_horizontal.direction_high_speed == 0 ){
					    barChart_horizontal.x_high_speed -= speed*oneCM/1000*frequencyDelta*deltaTime;
					}   	

					// Set the right border to change the moving direction
					if ( barChart_horizontal.x_high_speed > barchart_travel_distance_pixel ){
					    barChart_horizontal.x_high_speed = barchart_travel_distance_pixel;
					    barChart_horizontal.direction_high_speed = 0;
					    // Change the direction, which means the bar goes one way for 1 time
					    displayed_one_time = true;
					}

					// When the stimuli is in process of moving, callback this function itself according to the broswer's frame refresh
					if(barChart_horizontal.x_high_speed > 0 && barChart_horizontal.x_high_speed <= barchart_travel_distance_pixel){
						// Refresh the lastFinishedTime, because for the moving case, the time is cut into micro step based on 1ms, the donut chart moves on for a certain distance according to this deltaTime.
						
						    lastFinishedTime = Date.now();
						    callback_id = requestAnimationFrame(function (){timeBasedAnimation(canvas, ctx, speed, percentage)});
						
					//When the stimuli returns back to the left border, disabled the canvas_0/canavs_1, enable the afterimage block
					}else if(barChart_horizontal.direction_high_speed <= 0){
						// console.log("waitDuration = " + waitDuration);
						// Record the display duration
						display_millisecond = Date.now() - start_answer_time;
						// console.log("display_millisecond = " + display_millisecond);

						// Sometimes, the display_millisecond < 1600, and will restart from the beginning, let the bar go 2 round ways, which is not allowed.
					    if(display_millisecond >= 1600 || displayed_one_time){
					    	// Clear the previous draw
							ctx.clearRect(0,0,canvas.width,canvas.height);
						    // For trial case
						    if(canvas == document.getElementById("canvas_1")){
						    	document.getElementById("canvas_1").style.display = 'none';
								document.getElementById("trial_afterImage").style.display = 'block';			
							// For training case
							}else if(canvas == document.getElementById("canvas_0")){
								document.getElementById("canvas_0").style.display = 'none';
							    document.getElementById("training_afterImage").style.display = 'block';
							}
							// Afterimage blocks
							Blink(canvas, 4, 20);
						}
						// Reset the x coordinate and direction
						barChart_horizontal.x_high_speed = 0;
						barChart_horizontal.direction_high_speed = 1;

						if(display_millisecond < 1600 && !displayed_one_time){
							// Clear the previous draw
							ctx.clearRect(0,0,canvas.width,canvas.height);
							lastFinishedTime = initializationTime = Date.now();
							// console.log("start_answer_time: " + start_answer_time);
						    callback_id = requestAnimationFrame(function (){timeBasedAnimation(canvas, ctx, speed, percentage)});
						}		
					}
				}
			}
		// The slow motion case, when the donut chart touches the middle left border, go back 
		}else if(speed == 15){

			visionFocus.x = 1/4 * (barchart_travel_distance_pixel_needed_horizontal+barchart_length);
			visionFocus.y = 0.25*oneCM;
			visionFocus.draw(canvas, ctx);

			deltaTime_visionfocus = currentTime - endTime_wait;

			if(deltaTime_visionfocus < visionfocus_duration_allowed){
				endTime_visionfocus = Date.now();
				callback_id = requestAnimationFrame(function (){timeBasedAnimation(canvas, ctx, speed, percentage)});
			// Vision focus should disappear
			}else{

				// Clear the previous draw
				ctx.clearRect(0,0,canvas.width,canvas.height);
				
				blankTime_after_visionfocus = currentTime - endTime_visionfocus;
				
				if(blankTime_after_visionfocus < blankTime_allowed){
					start_answer_time = lastFinishedTime = Date.now();
					callback_id = requestAnimationFrame(function (){timeBasedAnimation(canvas, ctx, speed, percentage)});
				}else{			

					deltaTime = currentTime - lastFinishedTime;
					// Clear the previous draw
					ctx.clearRect(0,0,canvas.width,canvas.height);

					// Prepare the length of two sections (horizontal bar chart)
					var target_lengt_vis = percentage * barchart_length;
					var other_length_vis =  barchart_length - target_lengt_vis;

					// Redraw with the new coordinates
					barChart_horizontal.draw(speed, ctx, other_length_vis, target_lengt_vis);

					// Set the moving flag, this parameter is used to identify the donut chart is moving or is static
					movingFlag = true;
					// Start point, end point
					let start_x = 1/4 * (barchart_travel_distance_pixel_needed_horizontal+barchart_length), stop_x = 3/4 * barchart_travel_distance_pixel_needed_horizontal - 1/4 * barchart_length;
					// Set the x coordinate and the moving direction according to the different speeds
					if ( barChart_horizontal.direction_low_speed == 1 ){
					    barChart_horizontal.x_low_speed += speed*oneCM/1000*frequencyDelta*deltaTime;
					}else if ( barChart_horizontal.direction_low_speed == 0 ){
					    barChart_horizontal.x_low_speed -= speed*oneCM/1000*frequencyDelta*deltaTime;
					}

					// Set the border to change the moving direction
					if ( barChart_horizontal.x_low_speed > stop_x){
					    barChart_horizontal.x_low_speed = stop_x;
					    barChart_horizontal.direction_low_speed = 0;
					    // Change the direction, which means the bar goes one way for 1 time
					    displayed_one_time = true;
					}

					// When the stimuli is in process of moving, callback this function itself according to the broswer's frame refresh
					if( barChart_horizontal.x_low_speed > start_x && barChart_horizontal.x_low_speed <= stop_x ){
						// Refresh the lastFinishedTime, because for the moving case, the time is cut into micro step based on 1ms, the donut chart moves on for a certain distance according to this deltaTime.
						
						lastFinishedTime = Date.now();
						callback_id = requestAnimationFrame(function (){timeBasedAnimation(canvas, ctx, speed, percentage)});
						
					//When the stimuli returns back to the left border, disabled the canvas_0/canavs_1, enable the afterimage block
					}else if(barChart_horizontal.x_low_speed <= start_x ){
						// console.log("waitDuration = " + waitDuration);
						// Record the display duration
						display_millisecond = Date.now() - start_answer_time;
						// display_millisecond = Date.now() - initializationTime - waitDeltaTime - deltaTime_visionfocus - blankTime_after_visionfocus;
						// console.log("display_millisecond = " + display_millisecond);

						// Sometimes, the display_millisecond < 1600, and will restart from the beginning, let the bar go 2 round ways, which is not allowed.
					    if(display_millisecond >= 1600 || displayed_one_time){
					    	// Clear the previous draw
							ctx.clearRect(0,0,canvas.width,canvas.height);
						    // For trial case
						    if(canvas == document.getElementById("canvas_1")){
							    document.getElementById("canvas_1").style.display = 'none';
							    document.getElementById("trial_afterImage").style.display = 'block';
							// For training case
							}else if(canvas == document.getElementById("canvas_0")){
								document.getElementById("canvas_0").style.display = 'none';
							    document.getElementById("training_afterImage").style.display = 'block';
							}
							// Afterimage blocks
							Blink(canvas, 4, 20);
						}
					
						// Reset the x coordinate and direction
						barChart_horizontal.x_low_speed = start_x;
						barChart_horizontal.direction_low_speed = 1;

						if(display_millisecond < 1600 && !displayed_one_time){
							// Clear the previous draw
							ctx.clearRect(0,0,canvas.width,canvas.height);
							lastFinishedTime = initializationTime = Date.now();
							// console.log("start_answer_time: " + start_answer_time);
						    callback_id = requestAnimationFrame(function (){timeBasedAnimation(canvas, ctx, speed, percentage)});
						}
						
					}
				}
			}


		// For static case, the donut chart stays in the center of canvas for X ms (X == static_displaying_time_1), and then afterimage block fires
		}else if(speed == 0){

			// Prepare the length of two sections (horizontal bar chart)
			var target_lengt_vis = percentage * barchart_length;
			var other_length_vis =  barchart_length - target_lengt_vis;

			// Redraw with the new coordinates
			barChart_horizontal.draw(speed, ctx, other_length_vis, target_lengt_vis);

			movingFlag = false;
			let dlt = deltaTime - waitDeltaTime;
			if(dlt < static_displaying_time_1){
				// Do not refresh the lastFinishedTime, the role of deltaTime here is a timer, to count for static_displaying_time_1 ms.
				
					callback_id = requestAnimationFrame(function (){timeBasedAnimation(canvas, ctx, speed, percentage)});
				
			}else if(dlt >= static_displaying_time_1){
				// console.log("waitDuration = " + waitDuration);
				// Record the display duration
				display_millisecond = dlt;
				// console.log("display_millisecond = " + display_millisecond);

				if(display_millisecond >= 1600){
					// Clear the previous draw
					ctx.clearRect(0,0,canvas.width,canvas.height);

					// For trial case
				    if(canvas == document.getElementById("canvas_1")){
					    document.getElementById("canvas_1").style.display = 'none';
					    document.getElementById("trial_afterImage").style.display = 'block';
					// For training case
					}else if(canvas == document.getElementById("canvas_0")){
						document.getElementById("canvas_0").style.display = 'none';
					    document.getElementById("training_afterImage").style.display = 'block';
					}
					// Afterimage blocks
					Blink(canvas, 4, 20);
				}

				if(display_millisecond < 1600){
					// Clear the previous draw
					ctx.clearRect(0,0,canvas.width,canvas.height);
					
					start_answer_time = lastFinishedTime = initializationTime = Date.now();
					// console.log("start_answer_time: " + start_answer_time);
				    callback_id = requestAnimationFrame(function (){timeBasedAnimation(canvas, ctx, speed, percentage)});
				}			
			}
		}
	}
}		


// Irregular motion
function irregularTBA(cvs, ctx, speed, percentage){

	currrentTime_irregular = Date.now();
	waitDeltaTime = currrentTime_irregular - initializationTime_irregular;

	if(waitDeltaTime < waitDuration){
		callback_id = requestAnimationFrame(function (){irregularTBA(cvs, ctx, speed, percentage)});
	}else{

	// Initialized the counter and draw the vision focus part
	if(initializationTime_irregular == lastFinishedTime_irregular){
			//  Clear the canvas 
			ctx.clearRect(0,0,cvs.width,cvs.height);

			// Start from the first coordinate
			coordinate_counter = 0;
			// console.log("path_counter = " + path_counter);
			// console.log("coordinate_counter = " + coordinate_counter);
			// console.log("training_subscript = " + training_subscript[path_counter]);
			// Set the flag, the pointer of coordinate goes from the first point to the last point
			coordinatesPlus = true; coordinatesMinus = false;
			// Initialized the start point. ! Pay attention ! The orginal coordinates are in CM
			// Fast motion
			if( speed == -30 ){
				// Training
				if( cvs == document.getElementById("canvas_2") ){
					bar_horizontal_trajectory.x = x_previous_frame = fast_path_training["path_" + training_subscript[path_counter] ]["coordinate_" + coordinate_counter]["x"] * oneCM;
					bar_horizontal_trajectory.y = y_previous_frame = fast_path_training["path_" + training_subscript[path_counter] ]["coordinate_" + coordinate_counter]["y"] * oneCM;
				// Trial
				}else if( cvs == document.getElementById("canvas_3") ){
					bar_horizontal_trajectory.x = x_previous_frame = fast_path_trial["path_" + trial_subscript[path_counter] ]["coordinate_" + coordinate_counter]["x"] * oneCM;
					bar_horizontal_trajectory.y = y_previous_frame = fast_path_trial["path_" + trial_subscript[path_counter] ]["coordinate_" + coordinate_counter]["y"] * oneCM;
				}
			// Slow motion
			}else if(speed == -15){
				// Training
				if( cvs == document.getElementById("canvas_2") ){
					bar_horizontal_trajectory.x = x_previous_frame = slow_path_training["path_" + training_subscript[path_counter] ]["coordinate_" + coordinate_counter]["x"] * oneCM;
					bar_horizontal_trajectory.y = y_previous_frame = slow_path_training["path_" + training_subscript[path_counter] ]["coordinate_" + coordinate_counter]["y"] * oneCM;
				}else if( cvs == document.getElementById("canvas_3") ){
					bar_horizontal_trajectory.x = x_previous_frame = slow_path_trial["path_" + trial_subscript[path_counter] ]["coordinate_" + coordinate_counter]["x"] * oneCM;
					bar_horizontal_trajectory.y = y_previous_frame = slow_path_trial["path_" + trial_subscript[path_counter] ]["coordinate_" + coordinate_counter]["y"] * oneCM;
				}
			}
			// console.log("bar.x = x_previous_frame = " + bar_horizontal_trajectory.x);
			// console.log("bar.y = x_previous_frame = " + bar_horizontal_trajectory.y);
			
			/* Call for vision focus part*/
			// Define where to draw
			visionFocus.x = x_previous_frame;
			visionFocus.y = y_previous_frame;
			// Draw the cross
			visionFocus.draw(cvs, ctx);

			// Restart the time count, record the last finished time
			initializationTime_visionfocus = lastFinishedTime_visionfocus = lastFinishedTime_irregular  = Date.now();	
			callback_id = requestAnimationFrame(function (){irregularTBA(cvs, ctx, speed, percentage)});

	// Wait the vision focus cross displaying duration
	}else{

			// When the vision focus display duration >= 1000 ms, the first frame should be drawn
			if( (lastFinishedTime_visionfocus - initializationTime_visionfocus) >= visionfocus_duration_allowed ){
				// Record the vision focus cross display duration
				deltaTime_visionfocus = lastFinishedTime_visionfocus - initializationTime_visionfocus;
				// console.log("vision focus display duration = " + deltaTime_visionfocus);

				// Clear the canavs
				ctx.clearRect(0,0,cvs.width,cvs.height);

				blankTime_after_visionfocus = currrentTime_irregular - lastFinishedTime_visionfocus;
				

				if(blankTime_after_visionfocus < blankTime_allowed){
					callback_id = requestAnimationFrame(function (){irregularTBA(cvs, ctx, speed, percentage)});
				}else{

				// Draw the first frame
				if(firstFrame_irregular){

					bar_target_width = barchart_length * percentage;
    				bar_other_width = barchart_length - bar_target_width;
					bar_horizontal_trajectory.draw(ctx, bar_target_width, bar_other_width);

					// Go to the next coordinate
					coordinate_counter++;
					// console.log("coordinate_counter = " + coordinate_counter);
					
					// Turn off the flag
					firstFrame_irregular = false;
					
					// Restart the time count, record the last finished time
					start_answer_time = initializationTime_irregular_stimuli = lastFinishedTime_irregular_stimuli = Date.now();	
					callback_id = requestAnimationFrame(function (){irregularTBA(cvs, ctx, speed, percentage)});
				
				// After initialization, draw according to the real time taken
				// Each time of refreshing, go to the next coordinate with the same velocity vector
				}else{
					// Record the current time
					currentTime_irregular_stimuli = Date.now();

					// When the display duration >= 1600 ms, should be initialized
					if( (currentTime_irregular_stimuli - initializationTime_irregular_stimuli) >= duration_allowed ){
						// clean the canvas
						ctx.clearRect(0,0,cvs.width,cvs.height);

						// Record the stimuli display duration 
						display_millisecond = currentTime_irregular_stimuli - initializationTime_irregular_stimuli;

						// console.log("coordinate_counter = " + coordinate_counter);
						// console.log("distance_travelled_totally in CM = " + (distance_travelled_totally/oneCM) );
						// console.log("displayed duration = " + (currentTime_irregular_stimuli - initializationTime_irregular_stimuli) );
						// console.log("display_millisecond = " + display_millisecond);

						// Blink the canvas
						if( cvs == document.getElementById("canvas_2") ){
							document.getElementById("canvas_2").style.display = 'none';
							document.getElementById("irregular_training_afterImage").style.display = 'block';
						}else if( cvs == document.getElementById("canvas_3") ){
							document.getElementById("canvas_3").style.display = 'none';
							document.getElementById("irregular_trial_afterImage").style.display = 'block';
						}

						Blink(cvs, 4, 20);

						// // Let the initial time = last finished time
						// // initializationTime_1 = lastFinishedTime_1 = Date.now();
						// // The coordiante go back to the first one
						// coordinate_counter = undefined;
						// // The distance travelled in total should be reset
						// distance_travelled_totally = 0;
						// // Reset the flag
						// coordinatesPlus  = false; coordinatesMinus = false;
						// // // Clear the canvas 
						// // ctx1.clearRect(0,0,cvs1.width,cvs1.height);
						// // Call the animation --> loop, in the formal page, should be replaced by Masking images
						// // callback_id_array[1] = requestAnimationFrame(function (){tBA1()});

					// When the display duration doesn't archive the threshold
					}else{
						// Calculate the real time taken
						deltaTime_irregular_stimuli = currentTime_irregular_stimuli - lastFinishedTime_irregular_stimuli; // in MS

						/* The following things should be calculate beased on the speed in formal page */

						// Slow motion, speed = 15cm/s, the distance should travel is
						if(speed == -15){
							distance_should_travel = 15 * oneCM / 1000 * deltaTime_irregular_stimuli; //  in PIXEL
							// console.log("distance_should_travel = " + distance_should_travel);
						
						// Fast motion, speed = 30cm/s, the distance should travel is
						}else if(speed == -30){
							distance_should_travel = 30 * oneCM / 1000 * deltaTime_irregular_stimuli; //  in PIXEL
						}

						distance_travelled_totally = distance_travelled_totally +  distance_should_travel;

						do{
							// Slow motion
							if( speed == -15 ){
								// Training
								if( cvs == document.getElementById("canvas_2") ){
									x_current_set = slow_path_training["path_" + training_subscript[path_counter] ]["coordinate_" + coordinate_counter]["x"] * oneCM;
									y_current_set = slow_path_training["path_" + training_subscript[path_counter]]["coordinate_" + coordinate_counter]["y"] * oneCM;
								// Trial
								}else if( cvs == document.getElementById("canvas_3") ){
									x_current_set = slow_path_trial["path_" + trial_subscript[path_counter] ]["coordinate_" + coordinate_counter]["x"] * oneCM;
									y_current_set = slow_path_trial["path_" + trial_subscript[path_counter] ]["coordinate_" + coordinate_counter]["y"] * oneCM;
								}

							// Fast motion
							}else if( speed == -30 ){
								// Training
								if( cvs == document.getElementById("canvas_2") ){
									x_current_set = fast_path_training["path_" + training_subscript[path_counter] ]["coordinate_" + coordinate_counter]["x"] * oneCM;
									y_current_set = fast_path_training["path_" + training_subscript[path_counter] ]["coordinate_" + coordinate_counter]["y"] * oneCM;
								// Trial
								}else if( cvs == document.getElementById("canvas_3") ){
									x_current_set = fast_path_trial["path_" + trial_subscript[path_counter] ]["coordinate_" + coordinate_counter]["x"] * oneCM;
									y_current_set = fast_path_trial["path_" + trial_subscript[path_counter] ]["coordinate_" + coordinate_counter]["y"] * oneCM;
								}
							}

							distance_set = Math.sqrt( Math.pow( (x_current_set-x_previous_frame), 2) + Math.pow( (y_current_set-y_previous_frame), 2) );

							// Slow motion - 125 points in total, arrive the end of the path, should go back
							if(speed == -15){
								if( coordinate_counter == 124 ){
									// Already arriving at the last point, should switch the direction of accumulation 
									coordinatesPlus = false;
									coordinatesMinus = true;
									// console.log("coordinate_counter = " + coordinate_counter);
								}

							// Fast motion - 300 points in total, arrive the end of the path, should go back
							}else if(speed == -30){
								if( coordinate_counter == 299 ){
									// Already arriving at the last point, should switch the direction of accumulation 
									coordinatesPlus = false;
									coordinatesMinus = true;
									// console.log("coordinate_counter = " + coordinate_counter);
								}
							}

							if( coordinate_counter == 0 ){
								// Already arriving at the first point, should switch the direction of accumulation 
								coordinatesPlus = true;
								coordinatesMinus = false;
								// console.log("coordinate_counter = " + coordinate_counter);
							}

							if(coordinatesPlus){
								// Already go to the next coordinate (N+1), need find a mmiddle point between (N-1) and N. N is the point who meet the condition.
								coordinate_counter++;
								// console.log(coordinate_counter);
							}
							if(coordinatesMinus){
								coordinate_counter--;
								// console.log(coordinate_counter);
							}

						// Skip the points until the linear distance is longer than the target value (distance_should_travel)
						}while( distance_set <= distance_should_travel );

						// The x and y coordiante of the previous frame are x_previous_frame, y_previous_frame;
						// The x and y coordiante of the current set are
						// Slow motion
						if(speed == -15){
							// Training
							if( cvs == document.getElementById("canvas_2") ){
								x_current_set = slow_path_training["path_" + training_subscript[path_counter] ]["coordinate_" + (coordinate_counter-1)]["x"] * oneCM;
								y_current_set = slow_path_training["path_" + training_subscript[path_counter] ]["coordinate_" + (coordinate_counter-1)]["y"] * oneCM;
							// Trial
							}else if( cvs == document.getElementById("canvas_3") ){
								x_current_set = slow_path_trial["path_" + trial_subscript[path_counter] ]["coordinate_" + (coordinate_counter-1)]["x"] * oneCM;
								y_current_set = slow_path_trial["path_" + trial_subscript[path_counter] ]["coordinate_" + (coordinate_counter-1)]["y"] * oneCM;
							}
						}else if( speed == -30){
							// Training
							if( cvs == document.getElementById("canvas_2") ){
								x_current_set = fast_path_training["path_" + training_subscript[path_counter] ]["coordinate_" + (coordinate_counter-1)]["x"] * oneCM;
								y_current_set = fast_path_training["path_" + training_subscript[path_counter] ]["coordinate_" + (coordinate_counter-1)]["y"] * oneCM;
							// Trial
							}else if( cvs == document.getElementById("canvas_3") ){
								x_current_set = fast_path_trial["path_" + trial_subscript[path_counter] ]["coordinate_" + (coordinate_counter-1)]["x"] * oneCM;
								y_current_set = fast_path_trial["path_" + trial_subscript[path_counter] ]["coordinate_" + (coordinate_counter-1)]["y"] * oneCM;
							}
						}
						// console.log("x_current_set = " + x_current_set);
						// console.log("y_current_set = " + y_current_set);
						
						// The x and y coordiante of the target frame will be x_target_frame, y_target_frame;
						// The distance set in the coordinate array is c^2 = a^2 + b^2
						distance_set = Math.sqrt( Math.pow( (x_current_set-x_previous_frame), 2) + Math.pow( (y_current_set-y_previous_frame), 2) ); // in PIXEL
						// console.log("distance_set = " + distance_set);

						// The x_target_frame and y_target_frame should be
						x_target_frame = distance_should_travel/distance_set * ( x_current_set - x_previous_frame ) + x_previous_frame;
						y_target_frame = distance_should_travel/distance_set * ( y_current_set - y_previous_frame ) + y_previous_frame;
						// console.log("x_target_frame = " + x_target_frame);
						// console.log("y_target_frame = " + y_target_frame);

						// Set the bar chart x and y
						bar_horizontal_trajectory.x = x_target_frame;
						bar_horizontal_trajectory.y = y_target_frame;
				
						// Draw the target frame
						bar_horizontal_trajectory.draw(ctx, bar_target_width, bar_other_width);

						// Let the current frame coorddinates to be the previous
						x_previous_frame = x_target_frame;
						y_previous_frame = y_target_frame;
						// console.log("x_previous_frame = " + x_previous_frame);
						// console.log("y_previous_frame = " + y_previous_frame);
			
						// Restart the time count, record the last finished time
						lastFinishedTime_irregular_stimuli = Date.now();
						callback_id = requestAnimationFrame(function (){irregularTBA(cvs, ctx, speed, percentage)});
					
					}
				}
				}
				
			// still display the vision focus
			}else{
				// Restart the time count, record the last finished time
				lastFinishedTime_visionfocus = Date.now();
				callback_id = requestAnimationFrame(function (){irregularTBA(cvs, ctx, speed, percentage)});
			
			}
			
	}
	}
}


/* After image blink function */
// Once the stimuli go from left to right, and then return back from right to left, reach the far left border, the stimuli's canvas will disappear, the afterimage will appear during a very short time, perhaps only 0.5s, and repeat for several times.
// canvas is you want to apply this function on which canvas
// blink_times is how many times you want to repeat the afterimage
// interval_ms is how long time you want to let the afterimage appear for one time, in MS
function Blink(canvas, blink_times, interval_ms){
	// console.log("Blink fires");
	// Linear motion - For trial
	if(canvas == document.getElementById("canvas_1")){
		// After the stimuli display finishing, let blank canvas display for 1s
		// timeout_id[0] = setTimeout(function(){document.getElementById("trial_blank").style.display = 'none';document.getElementById("trial_afterImage").style.display = 'block';}, 1000);
		
		// Dispalay the 4 afterimages one by one, each one stays for interval_ms
		for(let i=1;i<=blink_times; i++){
			if(i == 1){
				timeout_id[i] = setTimeout(function(){document.getElementById("trial_afterImage").style.display = 'none';document.getElementById("trial_afterImage_" + i).style.display = 'block';}, interval_ms*i);
			}else if(i == blink_times){
				timeout_id[i] = setTimeout(function(){document.getElementById("trial_afterImage_" + (i-1) ).style.display = 'none';document.getElementById("trial_blank").style.display = 'block'; checkDoneButtonFire = true; activeDoneButton(false);}, interval_ms*i);
			}else{
				timeout_id[i] = setTimeout(function(){document.getElementById("trial_afterImage_" + (i-1) ).style.display = 'none';document.getElementById("trial_afterImage_" + i).style.display = 'block';}, interval_ms*i);
			}
		}
	// Linear motion - For training
	}else if(canvas == document.getElementById("canvas_0")){
		// After the stimuli display finishing, let blank canvas display for 1s
		// timeout_id[0] = setTimeout(function(){document.getElementById("training_blank").style.display = 'none';document.getElementById("training_afterImage").style.display = 'block';}, 1000);
		// Dispalay the 4 afterimages one by one, each one stays for interval_ms
		for(let i=1;i<=blink_times; i++){
			if(i == 1){
				timeout_id[i] = setTimeout(function(){document.getElementById("training_afterImage").style.display = 'none';document.getElementById("training_afterImage_" + i).style.display = 'block';}, interval_ms*i);
			}else if(i == blink_times){
				timeout_id[i] = setTimeout(function(){document.getElementById("training_afterImage_" + (i-1) ).style.display = 'none';document.getElementById("training_blank").style.display = 'block'; checkDoneButtonFire = true; activeDoneButton(true);}, interval_ms*i);
			}else{
				timeout_id[i] = setTimeout(function(){document.getElementById("training_afterImage_" + (i-1)).style.display = 'none';document.getElementById("training_afterImage_" + i).style.display = 'block';}, interval_ms*i);
			}
		}
	// Irregular motion - For training
	}else if(canvas == document.getElementById("canvas_2")){
		// Dispalay the 4 afterimages one by one, each one stays for interval_ms
		for(let i=1;i<=blink_times; i++){
			if(i == 1){
				timeout_id[i] = setTimeout(function(){document.getElementById("irregular_training_afterImage").style.display = 'none';document.getElementById("irregular_training_afterImage_" + i).style.display = 'block';}, interval_ms*i);
			}else if(i == blink_times){
				timeout_id[i] = setTimeout(function(){document.getElementById("irregular_training_afterImage_" + (i-1) ).style.display = 'none';document.getElementById("irregular_training_blank").style.display = 'block'; checkDoneButtonFire = true; activeDoneButton(true);}, interval_ms*i);
			}else{
				timeout_id[i] = setTimeout(function(){document.getElementById("irregular_training_afterImage_" + (i-1)).style.display = 'none';document.getElementById("irregular_training_afterImage_" + i).style.display = 'block';}, interval_ms*i);
			}
		}
	// Irregular motion - For trial
	}else if(canvas == document.getElementById("canvas_3")){
		// Dispalay the 4 afterimages one by one, each one stays for interval_ms
		for(let i=1;i<=blink_times; i++){
			if(i == 1){
				timeout_id[i] = setTimeout(function(){document.getElementById("irregular_trial_afterImage").style.display = 'none';document.getElementById("irregular_trial_afterImage_" + i).style.display = 'block';}, interval_ms*i);
			}else if(i == blink_times){
				timeout_id[i] = setTimeout(function(){document.getElementById("irregular_trial_afterImage_" + (i-1) ).style.display = 'none';document.getElementById("irregular_trial_blank").style.display = 'block'; checkDoneButtonFire = true; activeDoneButton(false);}, interval_ms*i);
			}else{
				timeout_id[i] = setTimeout(function(){document.getElementById("irregular_trial_afterImage_" + (i-1)).style.display = 'none';document.getElementById("irregular_trial_afterImage_" + i).style.display = 'block';}, interval_ms*i);
			}
		}
	}
}

/* Let the canvas go back to the initial status,
 * For the stimuli: style="display: block;"
 * For the afterimage: style="display: none;"
 * For the blank: style="display: none" */
// stimuli = document.getElementById(""); 
// afterimage = document.getElementById(""); 
// blank = document.getElementById(""); 
function resetCanvasDisplay(stimuli, blank, afterimage1, afterimage2, afterimage3, afterimage4){
	stimuli.style.display = 'block';
	blank.style.display = 'none';
	afterimage1.style.display = 'none';
	afterimage2.style.display = 'none';
	afterimage3.style.display = 'none';
	afterimage4.style.display = 'none';
}

/* Hide the un-used canvas set */
function hideCanvasSet(stimuli, blank, afterimage1, afterimage2, afterimage3, afterimage4){
	stimuli.style.display = 'none';
	blank.style.display = 'none';
	afterimage1.style.display = 'none';
	afterimage2.style.display = 'none';
	afterimage3.style.display = 'none';
	afterimage4.style.display = 'none';
}

/* Let the Done button disabled, and the answer text area clean*/
// btn is the button that you want to reset as disabled
// disabled button background color is light grey, text color is dark grey
// btn = document.getElementById("")
// ata is the answer text area that you want to clean
// ata = document.getElementById("")
function resetDoneButtonAnswerArea(btn,ata){
	btn.style.background = "#EDEDED";
	btn.style.color = "#A3A3A3";
	btn.value = "Please give an answer";
	btn.disabled = true;
	ata.value = '';
}

/* Initialize the training stimuli, 
 * firstly let the explanation and the button to start training disappear,
 * secondly let the training stimuli displays, 
 * thirdly count the current training times, 
 * lastly prepare the afterimage block and the stimuli block according to the speed */
function startTraining(button){
	// console.log("original_speed_array: " + original_speed_array);
	// console.log("random_speed_array: " + random_speed_array);

	waitDuration = 800;
	// Clean the checked checkbox
	cleanTrainingCheckbox();
	// Clean the text area
	document.getElementById("training_answer_given").value = '';
	// Let the bold words become normal
	for(let i=1; i<=4; i++){
		document.getElementById("training_bold_" + i).style.fontWeight = 'normal';
	}

	// Cancel the current animation
	cancelAnimationFrame(callback_id);
	// clean all the setTimeout()
	for(let i=0; i<=4; i++){
		clearTimeout(timeout_id[i]);
	}

	// Initialize the training parameters
	if(current_training_turn == undefined){
		current_training_turn = 0;
	}else{
		current_training_turn++;
	}

	/* Set the stimuli drawn canvas size according to the movement kinds, to let the answer area at the good position */
    // Irregular movement
    if(random_speed_array[current_training_turn] < 0){
      document.getElementById("training_drawn").style.height = document.getElementById("trial_drawn").style.height = cvs_tra_tra_2.height + "px";
      // document.getElementById("training_title_2").style.marginBottom = 1;
    // Linear motion
    }else if(random_speed_array[current_training_turn] > 0){
      // Set the height of #trial_drawn 1.5 multiples higher than the height of .canvas, to avoid the tiny scale offset
      document.getElementById("training_drawn").style.height = document.getElementById("trial_drawn").style.height = circleOuterDiameter * 1.5 + "px";
    }

	// Disable the training explanation part and the button ifself
	document.getElementById("training_explanation").style.display = 'none';
	// Enable the training stimuli part
	document.getElementById("training_stimuli").style.display = 'block';

	/* Clear the previous draw */
	// Clear linear motion canvas
	context_training.clearRect(0,0,myCanvas_training.width,myCanvas_training.height);
	// Clear irregular movement canvas
	ctx_tra_tra_2.clearRect(0,0,cvs_tra_tra_2.width, cvs_tra_tra_2.height);

	/* Reset canvas, only let the group matching the current kind of movement displays, leave the rest group hide */
	// Linear motion (represents by positif speed)
	if(random_speed_array[current_training_turn] > 0){
		resetCanvasDisplay(myCanvas_training, myCanvas_training_blank, myCanvas_training_afterimage, myCanvas_training_afterimage_1, myCanvas_training_afterimage_2, myCanvas_training_afterimage_3);
		hideCanvasSet(cvs_tra_tra_2, cvs_tra_tra_ai_b, cvs_tra_tra_ai, cvs_tra_tra_ai_1, cvs_tra_tra_ai_2, cvs_tra_tra_ai_3);

	// Irregular movement (represents by negatif speed)
	}else if(random_speed_array[current_training_turn] < 0){
		// console.log("canavs should display");
		resetCanvasDisplay(cvs_tra_tra_2, cvs_tra_tra_ai_b, cvs_tra_tra_ai, cvs_tra_tra_ai_1, cvs_tra_tra_ai_2, cvs_tra_tra_ai_3);
		hideCanvasSet(myCanvas_training, myCanvas_training_blank, myCanvas_training_afterimage, myCanvas_training_afterimage_1, myCanvas_training_afterimage_2, myCanvas_training_afterimage_3);
		// console.log(cvs_tra_tra_2.style.display);
	}

	// Reset the bar positiion
	barChart_horizontal.x_static = barchart_travel_distance_pixel/2;
	barChart_horizontal.x_low_speed = 1/4 * (barchart_travel_distance_pixel_needed_horizontal+barchart_length);
	barChart_horizontal.x_high_speed = 0;
	barChart_horizontal.direction_low_speed = 1;
	barChart_horizontal.direction_high_speed = 1;

	// Rewrite the training/trial/confidence survey title, speed discription, and button text according to the turn
	if(current_training_turn == 0){
		// Part 1
		document.getElementById("training_title_2").innerHTML = "Training: Part 1";
		document.getElementById("start_experiment_button").innerHTML = "Start Experiment Part 1";
		document.getElementById("experiment_title").innerHTML = "Experiment: Part 1";
		document.getElementById("confidence_survey_title").innerHTML = "Confidence Survey: Part 1";
		document.getElementById("confidence_survey_count").innerHTML = "first";
		document.getElementById("submit_confidence_survey_button").innerHTML = "Submit Confidence Survey Part 1";

		document.getElementById("training_title_1").innerHTML = "Training: Part 2";
		document.getElementById("training_first_sentence").innerHTML = "Now it’s time for you to do the second training, confirm you read each line by clicking the checkbox:";
		// Insert the speed & trace discription, original_speed_array = [-30, -15, 15, 30] => negatif - irregular movement, positif - linear motion
	    if(random_speed_array[1] == original_speed_array[0]){
	      document.getElementById("training_speed_discription").innerHTML = "move at a high speed and in an irregular movement";
	    }else if(random_speed_array[1] == original_speed_array[1]){
	      document.getElementById("training_speed_discription").innerHTML = "move at a low speed and in an irregular movement";
	    }else if(random_speed_array[1] == original_speed_array[2]){
	      document.getElementById("training_speed_discription").innerHTML = "move at a low speed and in a linear motion";
	    }else if(random_speed_array[1] == original_speed_array[3]){
	      document.getElementById("training_speed_discription").innerHTML = "move at a high speed and in a linear motion";
	    }
    	document.getElementById("start_training_button").innerHTML = "Start Training Part 2";

	}else if(current_training_turn == 1){
		// Part 2
		document.getElementById("training_title_2").innerHTML = "Training: Part 2";
		document.getElementById("start_experiment_button").innerHTML = "Start Experiment Part 2";
		document.getElementById("experiment_title").innerHTML = "Experiment: Part 2";
		document.getElementById("confidence_survey_title").innerHTML = "Confidence Survey: Part 2";
		document.getElementById("confidence_survey_count").innerHTML = "second";
		document.getElementById("submit_confidence_survey_button").innerHTML = "Submit Confidence Survey Part 2";

		document.getElementById("training_title_1").innerHTML = "Training: Part 3";
		document.getElementById("training_first_sentence").innerHTML = "Now it’s time for you to do the last training, confirm you read each line by clicking the checkbox:";
		// Insert the speed & trace discription, original_speed_array = [-30, -15, 15, 30] => negatif - irregular movement, positif - linear motion
	    if(random_speed_array[2] == original_speed_array[0]){
	      document.getElementById("training_speed_discription").innerHTML = "move at a high speed and in an irregular movement";
	    }else if(random_speed_array[2] == original_speed_array[1]){
	      document.getElementById("training_speed_discription").innerHTML = "move at a low speed and in an irregular movement";
	    }else if(random_speed_array[2] == original_speed_array[2]){
	      document.getElementById("training_speed_discription").innerHTML = "move at a low speed and in a linear motion";
	    }else if(random_speed_array[2] == original_speed_array[3]){
	      document.getElementById("training_speed_discription").innerHTML = "move at a high speed and in a linear motion";
	    }
    	document.getElementById("start_training_button").innerHTML = "Start Training Part 3";
	}else if(current_training_turn == 2){
		// Part 3
		document.getElementById("training_title_2").innerHTML = "Training: Part 3";
		document.getElementById("start_experiment_button").innerHTML = "Start Experiment Part 3";
		document.getElementById("experiment_title").innerHTML = "Experiment: Part 3";
		document.getElementById("confidence_survey_title").innerHTML = "Confidence Survey: Part 3";
		document.getElementById("confidence_survey_count").innerHTML = "third";
		document.getElementById("submit_confidence_survey_button").innerHTML = "Submit Confidence Survey Part 3";

		document.getElementById("training_title_1").innerHTML = "Training: Part 4";
		document.getElementById("training_first_sentence").innerHTML = "Now it’s time for you to do the last training, confirm you read each line by clicking the checkbox:";
		// Insert the speed & trace discription, original_speed_array = [-30, -15, 15, 30] => negatif - irregular movement, positif - linear motion
	    if(random_speed_array[3] == original_speed_array[0]){
	      document.getElementById("training_speed_discription").innerHTML = "move at a high speed and in an irregular movement";
	    }else if(random_speed_array[3] == original_speed_array[1]){
	      document.getElementById("training_speed_discription").innerHTML = "move at a low speed and in an irregular movement";
	    }else if(random_speed_array[3] == original_speed_array[2]){
	      document.getElementById("training_speed_discription").innerHTML = "move at a low speed and in a linear motion";
	    }else if(random_speed_array[3] == original_speed_array[3]){
	      document.getElementById("training_speed_discription").innerHTML = "move at a high speed and in a linear motion";
	    }
    	document.getElementById("start_training_button").innerHTML = "Start Training Part 4";
	}else if(current_training_turn == 3){
		// Part 4
		document.getElementById("training_title_2").innerHTML = "Training: Part 4";
		document.getElementById("start_experiment_button").innerHTML = "Start Experiment Part 4";
		document.getElementById("experiment_title").innerHTML = "Experiment: Part 4";
		document.getElementById("confidence_survey_title").innerHTML = "Confidence Survey: Part 4";
		document.getElementById("confidence_survey_count").innerHTML = "fourth";
		document.getElementById("submit_confidence_survey_button").innerHTML = "Submit Confidence Survey Part 4";
	}

	/* Prepare the training stimuli and call for animation */
	training_percentage = randomNumber(0, 100, original_percentage_array);
	// console.log("training_percentage  = " + training_percentage);
	// console.log("speed = " + random_speed_array[current_training_turn]);

	/* Call for animation according to the kind of movement and speed */
	// Linear motion
	if(random_speed_array[current_training_turn] > 0){

		console.log("linear motion");

		displayed_one_time = false;

		lastFinishedTime = initializationTime = Date.now();
		callback_id = requestAnimationFrame(function (){timeBasedAnimation(myCanvas_training, context_training, random_speed_array[current_training_turn], training_percentage)});

	// Irregular movement
	}else if(random_speed_array[current_training_turn] < 0){

		firstFrame_irregular = true;
		// Start from the first path in th random array
		path_counter = 0; coordinate_counter = undefined;
		distance_travelled_totally = 0;
		coordinatesPlus  = false; coordinatesMinus = false;

		console.log("irregular movement");
		// console.log("speed = " + random_speed_array[current_training_turn]);
		initializationTime_irregular = lastFinishedTime_irregular = Date.now();
		callback_id = requestAnimationFrame(function (){irregularTBA(cvs_tra_tra_2, ctx_tra_tra_2, random_speed_array[current_training_turn], training_percentage)});
	}
	
	// btn_vis = true;
	training_trial_count++;

	// document.getElementById("training_part").style.display = 'none';
	// document.getElementById("training_part").style.display = 'block';
	// document.getElementById("vision_focus").style.display = 'block';
}

/* Check if the text area has a reasonable answer at the end of stimuli displaying, if the answer is positif, let the Done button disabled. 
 * This function is in case that the subject input an answer during the stimuli displaying process
 * During the displaying process, the Done button is always disabled, no metter there is or there isn't a given answer
 * It changes the button color from light grey to dark green.
 * If the case is training, 0 and 100 are considered, 
 * if the case is trial, 0 or 100 cannot be accepted as reasonable answer */
// val is the value from the answer text area
// flag is to identify the case: true -> training, false -> trial
function activeDoneButton(flag){
	// console.log("activeDoneButton fires");
	// For training case, 100 can be accepted ==> According to the feedback, let the subject enter whatever they want, so the 0 should be accepted.
	if(flag == true){
		let btn = document.getElementById("training_done_button");
		// The input answer is a string, parses it into int, decimal
		let val = parseInt(document.getElementById("training_answer_given").value, 10);
	  	if(val<0 || val>100){
		  	btn.style.background = "#EDEDED";
			btn.style.color = "#A3A3A3";
			btn.value = "Please give an appropriate answer";
		  	btn.disabled = true;
	  	}else if(val>=0 && val<=100){
		  	btn.style.background = "#006400";
		  	btn.style.color = "#FFFFFF";
		  	btn.value = "Done and Check Answer";
		    btn.disabled = false;
		    // console.log("the answer is 1: " + val);
	  	}else{
	  		btn.style.background = "#EDEDED";
			btn.style.color = "#A3A3A3";		
		  	btn.disabled = true;
	  	}
	// For trial case, 0 can be accepted
	}else if(flag == false){
		let btn = document.getElementById("trial_done_button");
		let val = parseInt(document.getElementById("trial_answer_given").value, 10);
	  	if(val<0 || val>=100){
		  	btn.style.background = "#EDEDED";
			btn.style.color = "#A3A3A3";
			btn.value = "Please give an appropriate answer";
		  	btn.disabled = true;
	  	}else if(val>=0 && val<100){
		  	btn.style.background = "#006400";
		  	btn.style.color = "#FFFFFF";
		  	if(current_trial == 20 || current_trial == 41 || current_trial == 62 || current_trial == 83){
			  	btn.value = "Done and Go to Confidence Survey";
			}else{
			  	btn.value = "Done and Go to Next Trial";
			}
		    btn.disabled = false;
		    // console.log("the answer is 1: " + val);
	  	}else{
	  		btn.style.background = "#EDEDED";
			btn.style.color = "#A3A3A3";
		  	btn.disabled = true;
	  	}
	}
}

function checkDoneButton(value, flag){
	// console.log("checkDoneButton fires");

	// For training case
	if(flag){
		let btn = document.getElementById("training_done_button");
		// The input answer is a string, parses it into int, decimal
		let val = parseInt(document.getElementById("training_answer_given").value, 10);
		// console.log("val = " + val);

		// The subject cannot click on the Done button untill the end of stimuli displaying	
		if(checkDoneButtonFire){
			if(val>=0 && val<=100){
			  	btn.style.background = "#006400";
			  	btn.style.color = "#FFFFFF";
			  	btn.value = "Done and Check Answer";
			    btn.disabled = false;
			    // console.log("the answer is 2: " + val);
	  		}else{
	  			btn.style.background = "#EDEDED";
				btn.style.color = "#A3A3A3";
				btn.value = "Please give an appropriate answer";
			  	btn.disabled = true;
	  		}
		}else{
			btn.style.background = "#EDEDED";
			btn.style.color = "#A3A3A3";
			btn.value = "Please wait for activation";
			btn.disabled = true;
		}
	// For trial case
	}else if(!flag){
		// Start the timer to record the answer time taken, if the first time to enter a letter in the textarea
		// Initial trial_answered = false; !trial_answered = true;
		if(!trial_answered){
			start_type_time = Date.now();
			type_milliseconds = start_type_time - start_answer_time;
			// console.log("start_type_time = " + start_type_time);
			// console.log("type_milliseconds = " + type_milliseconds);
			// trial_answered = true means the subject starts to give answer
			trial_answered = true;
		}

		let btn = document.getElementById("trial_done_button");
		let val = parseInt(document.getElementById("trial_answer_given").value, 10);
		// console.log("val = " + val);

		// The subject cannot click on the Done button untill the end of stimuli displaying		
		if(checkDoneButtonFire){
			if(val>=0 && val<100){
			  	btn.style.background = "#006400";
			  	btn.style.color = "#FFFFFF";
			  	if(current_trial == 20 || current_trial == 41 || current_trial == 62 || current_trial == 83){
			  		btn.value = "Done and Go to Confidence Survey";
			  	}else{
			  		btn.value = "Done and Go to Next Trial";
			  	}
			    btn.disabled = false;
			    // console.log("the answer is 2: " + val);
	  		}else{
	  			btn.style.background = "#EDEDED";
				btn.style.color = "#A3A3A3";
				btn.value = "Please give an appropriate answer";
			  	btn.disabled = true;
	  		}
		}else{
			btn.style.background = "#EDEDED";
			btn.style.color = "#A3A3A3";
			btn.value = "Please wait for activation";
			btn.disabled = true;
		}
	}
}

function checkAnswer(button){
	// Cancel the current animation
	cancelAnimationFrame(callback_id);
	// clean all the setTimeout()
	for(let i=0; i<=4; i++){
		clearTimeout(timeout_id[i]);
	}

	// Record the subject answer
	let ans = parseInt(document.getElementById("training_answer_given").value, 10);
	// console.log("training_answer_given = " + ans);
	// The correct answer
	let cans = parseInt(training_percentage*100, 10).toFixed();
	// console.log("exact answer = " + cans);
	// The acceptable minimum and maximum
	let min = cans - 10;
	let max = min + 20;
	// If the minimum is <=0 (there is no 0 in training case), min = 1 
	if(min < 0){ min = 0;}
	// If the maximum is >0 (there is 100 in training case), max = 100 
	if(max > 100){max = 100;}

	/* Compare with the acceptble answer range */
	// Answer can be accepted
	if(ans >= min && ans <= max){
		// Initial the count for training times
		if(current_training_times_per_turn == undefined){
			current_training_times_per_turn = 1;
		}else{
			current_training_times_per_turn++;
		}
		// console.log("current_training_times_per_turn = " + current_training_times_per_turn);
		// Insert the feedback into modal
		document.getElementById("answer_feedback").innerHTML = "Your answer " + ans + "%" + " is accepted." + "<br>" +
															   "The exact answer is: " + cans + "%." + "<br>" + 
															   "Current number of passes: " + current_training_times_per_turn +".";

		if(current_training_times_per_turn >= training_times_per_turn){
			document.getElementById("training_answer_check_button").innerHTML = "Close";
		}else{
			document.getElementById("training_answer_check_button").innerHTML = "Close and Continue Training";
		}
		
	// Answer cannot be accepted
	}else{

		if(current_training_times_errors_per_turn == undefined){
			current_training_times_errors_per_turn = 1;
		}else{
			current_training_times_errors_per_turn++;
		}

		// console.log("current_training_times_per_turn = " + current_training_times_per_turn);
		// Insert the feedback into modal
		document.getElementById("answer_feedback").innerHTML = "Your answer is: " + ans + "%." + "<br>" + 																								   "The exact answer is: " + cans + "%." + "<br>" +
															   "The reasonable range is between " + min + "-" + max + "%." + "<br>" +
															   "Please try again.";
	}

	// Sent the info to ajax
	measurements['color_condiiton'] = condition_color;
	measurements['speed_condiiton'] = random_speed_array[0] + " | " + random_speed_array[1] + " | " + random_speed_array[2] + " | " + random_speed_array[3];

	measurements['oneCM'] = oneCM;

	measurements['barchart_length_cm'] = barchart_length_cm;
	measurements['barchart_length_pixel'] = barchart_length;
	measurements['barchart_width_cm'] = barchart_width_cm;
	measurements['barchart_width_pixel'] = barchart_width;

	// if(movingFlag){
	// 	if(random_speed_array[current_training_turn] == 30){
	// 		measurements['vis_travel_distance_cm'] = barchart_travel_distance_cm;
	// 		measurements['vis_travel_distance_pixel'] = barchart_travel_distance_pixel;
	// 	}else if(random_speed_array[current_training_turn] == 15){
	// 		measurements['vis_travel_distance_cm'] = barchart_travel_distance_cm/2;
	// 		measurements['vis_travel_distance_pixel'] = barchart_travel_distance_pixel/2;
	// 	}
	// }else if(!movingFlag){
	// 	measurements['vis_travel_distance_cm'] = 0;
	// 	measurements['vis_travel_distance_pixel'] = 0;
	// }
	
	// linear motion, speed = 30
	if(random_speed_array[current_training_turn] == original_speed_array[3]){
		measurements['vis_travel_distance_cm'] = barchart_travel_distance_cm;
		measurements['vis_travel_distance_pixel'] = barchart_travel_distance_pixel;
	// linear motion, speed = 15
	}else if(random_speed_array[current_training_turn] == original_speed_array[2]){
		measurements['vis_travel_distance_cm'] = barchart_travel_distance_cm/2;
		measurements['vis_travel_distance_pixel'] = barchart_travel_distance_pixel/2;
	// irregular movement
	}else if(random_speed_array[current_training_turn] < 0){
		measurements['vis_travel_distance_cm'] = distance_travelled_totally/oneCM;
		measurements['vis_travel_distance_pixel'] = distance_travelled_totally;
	}

	// Add a colone to record movment kinds and current trajectory
	if(random_speed_array[current_training_turn] > 0){
		measurements['movement_kind'] = "Linear motion";
		if(random_speed_array[current_training_turn] == original_speed_array[3]){
			measurements['current_trajectory'] = "Linear - 48cm round trip";
		}else if(random_speed_array[current_training_turn] == original_speed_array[2]){
			measurements['current_trajectory'] = "Linear - 24cm round trip";
		}
	}else if(random_speed_array[current_training_turn] < 0){
		measurements['movement_kind'] = "Irregular motion";
		if(random_speed_array[current_training_turn] == original_speed_array[1]){
			measurements['current_trajectory'] = "Slow training movement - Path_" + training_subscript[path_counter];
		}else if(random_speed_array[current_training_turn] == original_speed_array[0]){
			measurements['current_trajectory'] = "Fast training movement - Path_" + training_subscript[path_counter];
		}
	}

	measurements['vis_display_duration'] = display_millisecond;
  	
	measurements['speed_duration'] = random_speed_array_string[current_training_turn];
	measurements['current_speed_cm_s'] = Math.abs(random_speed_array[current_training_turn]);

	measurements['training_trial_count'] = training_trial_count;

	measurements['correct_answer'] = cans;
	measurements['given_answer'] = ans;

	measurements['current_training_correct_times_per_turn'] = current_training_times_per_turn;
	measurements['current_training_errors_times_per_turn'] = current_training_times_errors_per_turn;
	
	if(current_training_times_errors_per_turn == undefined){
		measurements['current_training_times_per_turn'] = current_training_times_per_turn;
	}else if(current_training_times_per_turn == undefined){
		measurements['current_training_times_per_turn'] = current_training_times_errors_per_turn;
	}else{
		measurements['current_training_times_per_turn'] = current_training_times_per_turn+current_training_times_errors_per_turn;
	}

	$.ajax({
	        url: 'ajax/training.php',
	        type: 'POST',
	        data: JSON.stringify(measurements),
	        contentType: 'application/json',
    });



	// Open the modal (pop-up dialogue)
	$('#training_answer_check').modal();
	
	// Reset the Done button and clear the answer text area
	resetDoneButtonAnswerArea(document.getElementById("training_done_button"), document.getElementById("training_answer_given"));
	// Reset flag
	checkDoneButtonFire = false;
	// clean all the setTimeout()
	for(let i=0; i<=4; i++){
		clearTimeout(timeout_id[i]);
	}
	
	// Reset the bar positiion
	barChart_horizontal.x_static = barchart_travel_distance_pixel/2;
	barChart_horizontal.x_low_speed = 1/4 * (barchart_travel_distance_pixel_needed_horizontal+barchart_length);
	barChart_horizontal.x_high_speed = 0;
	barChart_horizontal.direction_low_speed = 1;
	barChart_horizontal.direction_high_speed = 1;
}

/* Reset the Done button and clean the text area, 
 * prepare the stimuli for the next training*/
function nextTraining(button){
	// console.log("original_speed_array: " + original_speed_array);
	// console.log("random_speed_array: " + random_speed_array);
	// // Reset the Done button and clear the answer text area
	// resetDoneButtonAnswerArea(document.getElementById("training_done_button"), document.getElementById("training_answer_given"));
	// Reset flag
	checkDoneButtonFire = false;
	// // In case that the subject clicks on the Done button before the end of Blink() -> after images block
	// // clean all the setTimeout()
	// for(let i=0; i<=4; i++){
	// 	clearTimeout(timeout_id[i]);
	// }
	
	/* Clear and Reset canvas, only let the group matching the current kind of movement displays, leave the rest group hide */
	// Linear motion (represents by positif speed)
	if(random_speed_array[current_training_turn] > 0){
		// Clear the previous draw
		context_training.clearRect(0,0,myCanvas_training.width,myCanvas_training.height);
		resetCanvasDisplay(myCanvas_training, myCanvas_training_blank, myCanvas_training_afterimage, myCanvas_training_afterimage_1, myCanvas_training_afterimage_2, myCanvas_training_afterimage_3);
		hideCanvasSet(cvs_tra_tra_2, cvs_tra_tra_ai_b, cvs_tra_tra_ai, cvs_tra_tra_ai_1, cvs_tra_tra_ai_2, cvs_tra_tra_ai_3);

	// Irregular movement (represents by negatif speed)
	}else if(random_speed_array[current_training_turn] < 0){
		// Clear the previous draw
		ctx_tra_tra_2.clearRect(0,0,cvs_tra_tra_2.width,cvs_tra_tra_2.height);
		resetCanvasDisplay(cvs_tra_tra_2, cvs_tra_tra_ai_b, cvs_tra_tra_ai, cvs_tra_tra_ai_1, cvs_tra_tra_ai_2, cvs_tra_tra_ai_3);
		hideCanvasSet(myCanvas_training, myCanvas_training_blank, myCanvas_training_afterimage, myCanvas_training_afterimage_1, myCanvas_training_afterimage_2, myCanvas_training_afterimage_3);
	}


	// // Clear the previous draw
	// context_training.clearRect(0,0,myCanvas_training.width,myCanvas_training.height);
	// // Reset canvas 
	// resetCanvasDisplay(myCanvas_training, myCanvas_training_blank, myCanvas_training_afterimage, myCanvas_training_afterimage_1, myCanvas_training_afterimage_2, myCanvas_training_afterimage_3);
	

	// // Reset the donut positiion
	// donutGroup.x = circleOuterRadius;
	// donutGroup.x_static = vis_travel_distance_pixel_needed/2 + circleOuterDiameter/4;
	// donutGroup.x_slow_motion = vis_travel_distance_pixel_needed/4;

	
	// // Initial the count for training times
	// if(current_training_times_per_turn == undefined){
	// 	current_training_times_per_turn = 1;
	// }else{
	// 	current_training_times_per_turn++;
	// }

	// If the training times ahieves the threshold
	if(current_training_times_per_turn >= training_times_per_turn){
		// Enable the start experiment button
		document.getElementById("training_button_2").style.display = 'block';
	}

	/* Prepare the training stimuli */
	training_percentage = randomNumber(0, 100, original_percentage_array);
	// console.log("training_percentage  = " + training_percentage);
	// console.log("speed  = " + random_speed_array[current_training_turn]);

	/* Call for animation */
	// Linear motion
	if(random_speed_array[current_training_turn] > 0){
		
		/* Reset the flag */
		displayed_one_time = false;

		lastFinishedTime = initializationTime = Date.now();
		callback_id = requestAnimationFrame(function (){timeBasedAnimation(myCanvas_training, context_training, random_speed_array[current_training_turn], training_percentage)});

	// Irregular movement
	}else if(random_speed_array[current_training_turn] < 0){
		
		/* Reset the flags and counters */
		firstFrame_irregular = true;
		// Go to the next path in the random array
		path_counter++;
		// Only 14 traces for training trajectoris per speed, restart from the first one, reuse tham in a loop
		if(path_counter == 14){
			path_counter = 0;
		}
		coordinate_counter = undefined;
		distance_travelled_totally = 0;
		coordinatesPlus  = false; coordinatesMinus = false;	

		initializationTime_irregular = lastFinishedTime_irregular = Date.now();
		callback_id = requestAnimationFrame(function (){irregularTBA(cvs_tra_tra_2, ctx_tra_tra_2, random_speed_array[current_training_turn], training_percentage)});
	}
	
}

function startExperiment(button){
	console.log("start experiment");
	// console.log("original_speed_array: " + original_speed_array);
	// console.log("random_speed_array: " + random_speed_array);

	// Cancel the current animation
	cancelAnimationFrame(callback_id);
	// clean all the setTimeout()
	for(let i=0; i<=4; i++){
		clearTimeout(timeout_id[i]);
	}
	
	// Reset the training_part as initial
	// Disable the training stimuli part and the start experiment button
	document.getElementById("training_stimuli").style.display = 'none';
	document.getElementById("training_button_2").style.display = 'none';

	// clean the training times count
	current_training_times_per_turn = undefined;
	current_training_times_errors_per_turn = undefined;
	// Enable the training explanation part and the start training button
	document.getElementById("training_explanation").style.display = 'block';
	
	
	// Disable the whole training part
	document.getElementById("training_part").style.display = 'none';
	// Enable the whole trial part
	document.getElementById("trial_stimuli").style.display = 'block';

	// Reset flag
	checkDoneButtonFire = false;

	// Initialize the part
	if(current_part == undefined){
		current_part = 0;
	}else{
		current_part++;
	}

	if(current_part == (threshold_part-1) ){
		endTrialPart = true;
	}

	// Initialize the trial
	if(current_trial == undefined){
		current_trial = 0;
	}

	// Initialize the repetition
	if(current_repetition == undefined && current_trial_per_repetition == undefined){
		current_repetition = 0;
		current_trial_per_repetition =0;
	}

	// Cancel the current animation
	cancelAnimationFrame(callback_id);
	// clean all the setTimeout()
	for(let i=0; i<=4; i++){
		clearTimeout(timeout_id[i]);
	}

	// Reset the bar positiion
	barChart_horizontal.x_static = barchart_travel_distance_pixel/2;
	barChart_horizontal.x_low_speed = 1/4 * (barchart_travel_distance_pixel_needed_horizontal+barchart_length);
	barChart_horizontal.x_high_speed = 0;
	barChart_horizontal.direction_low_speed = 1;
	barChart_horizontal.direction_high_speed = 1;
	// Reset waitDuration
	waitDuration = 200;
	// Clear the previous draw
	context_trial.clearRect(0,0,myCanvas_trial.width,myCanvas_trial.height);
	ctx_tra_tri_3.clearRect(0,0,cvs_tra_tri_3.width, cvs_tra_tri_3.height);
	
	/* Reset canvas according to the movement kinds */
	// Linear motion
	if(random_speed_array[current_part] > 0){
		resetCanvasDisplay(myCanvas_trial, myCanvas_blank, myCanvas_afterimage, myCanvas_afterimage_1, myCanvas_afterimage_2, myCanvas_afterimage_3);
		hideCanvasSet(cvs_tra_tri_3, cvs_tra_tri_ai_b, cvs_tra_tri_ai, cvs_tra_tri_ai_1, cvs_tra_tri_ai_2, cvs_tra_tri_ai_3);

	// Irregular movement
	}else if(random_speed_array[current_part] < 0){
		resetCanvasDisplay(cvs_tra_tri_3, cvs_tra_tri_ai_b, cvs_tra_tri_ai, cvs_tra_tri_ai_1, cvs_tra_tri_ai_2, cvs_tra_tri_ai_3);
		hideCanvasSet(myCanvas_trial, myCanvas_blank, myCanvas_afterimage, myCanvas_afterimage_1, myCanvas_afterimage_2, myCanvas_afterimage_3);
	}

	// console.log("speed = " + random_speed_array[current_part]);
	/* Prepare the trial stimuli and call for animation according to the movement kinds */
	// Linear motion
	if(random_speed_array[current_part] > 0){

		/* Reset the flag */
		displayed_one_time = false;

		lastFinishedTime = initializationTime = Date.now();
		callback_id = requestAnimationFrame(function (){timeBasedAnimation(myCanvas_trial, context_trial, random_speed_array[current_part], random_percentage_array[current_repetition][current_trial_per_repetition])});
	
	// Irregular movement
	}else if(random_speed_array[current_part] < 0){

		/* Reset the flags and counters */
		firstFrame_irregular = true;
		// Start from the first path in th random array
		path_counter = 0; coordinate_counter = undefined;
		distance_travelled_totally = 0;
		coordinatesPlus  = false; coordinatesMinus = false;

		// console.log("irregular movement trial");
		// console.log("speed = " + random_speed_array[current_part]);
		// console.log("path_counter" + path_counter);
		// console.log("trajectory = " + trial_subscript[path_counter]);
		// console.log("correct_answer = " + random_percentage_array[current_repetition][current_trial_per_repetition]);
		initializationTime_irregular = lastFinishedTime_irregular = Date.now();
		callback_id = requestAnimationFrame(function (){irregularTBA(cvs_tra_tri_3, ctx_tra_tri_3, random_speed_array[current_part], random_percentage_array[current_repetition][current_trial_per_repetition])});
	}
	
	// btn_vis = true;
	training_trial_count++;
	// document.getElementById("trial_stimuli").style.display = 'none';
	// document.getElementById("vision_focus").style.display = 'block';
	
	// console.log("correct_answer = " + random_percentage_array[current_repetition][current_trial_per_repetition]);
}

function nextTrial(button){
	// console.log("current_failed_times = " + current_failed_times);
	/* Record time taken and answer given */
	measurements['time_taken_type_ms'] = type_milliseconds;
    // console.log("time_taken_type_ms: " + type_milliseconds);
	// Record the time taken
	end_answer_time = Date.now();
	// console.log("end_answer_time: " + end_answer_time);
	submit_milliseconds = end_answer_time - start_type_time;
	// Push answer time taken into dataflow
    measurements['time_taken_submit_ms'] = submit_milliseconds;
    // console.log("time_taken_submit_ms: " + submit_milliseconds);

    // Reset the answered flag
    trial_answered = false;
    
    //Get given answer
  	trial_given_answer = $('#trial_answer_given').val();

  	// console.log("given_answer = " + trial_given_answer);
 //  	console.log("speed = " + random_speed_array[current_part]);
	// console.log("path_counter = " + path_counter);
	// console.log("trajectory = " + trial_subscript[path_counter]);
  	/* ---------------------------------------------------------------------------------------------------------------------------------------*/

  	/* Reset done button and text area, clean afterimage blocks and reset canvas */
  	// Reset the Done button and clear the answer text area
	resetDoneButtonAnswerArea(document.getElementById("trial_done_button"), document.getElementById("trial_answer_given"));
	// In case that the subject clicks on the Done button before the end of Blink() -> after images block
	// Cancel the current animation
	cancelAnimationFrame(callback_id);
	// clean all the setTimeout()
	for(let i=0; i<=4; i++){
		clearTimeout(timeout_id[i]);
	}

	// Clear the previous draw
	context_trial.clearRect(0,0,myCanvas_trial.width,myCanvas_trial.height);
	ctx_tra_tri_3.clearRect(0,0,cvs_tra_tri_3.width, cvs_tra_tri_3.height);

	/* Reset canvas according to the movement kinds */
	// Linear motion
	if(random_speed_array[current_part] > 0){
		resetCanvasDisplay(myCanvas_trial, myCanvas_blank, myCanvas_afterimage, myCanvas_afterimage_1, myCanvas_afterimage_2, myCanvas_afterimage_3);
		hideCanvasSet(cvs_tra_tri_3, cvs_tra_tri_ai_b, cvs_tra_tri_ai, cvs_tra_tri_ai_1, cvs_tra_tri_ai_2, cvs_tra_tri_ai_3);

	// Irregular movement
	}else if(random_speed_array[current_part] < 0){
		resetCanvasDisplay(cvs_tra_tri_3, cvs_tra_tri_ai_b, cvs_tra_tri_ai, cvs_tra_tri_ai_1, cvs_tra_tri_ai_2, cvs_tra_tri_ai_3);
		hideCanvasSet(myCanvas_trial, myCanvas_blank, myCanvas_afterimage, myCanvas_afterimage_1, myCanvas_afterimage_2, myCanvas_afterimage_3);
	}

	// Reset flag
	checkDoneButtonFire = false;

	// Reset the bar positiion
	barChart_horizontal.x_static = barchart_travel_distance_pixel/2;
	barChart_horizontal.x_low_speed = 1/4 * (barchart_travel_distance_pixel_needed_horizontal+barchart_length);
	barChart_horizontal.x_high_speed = 0;
	barChart_horizontal.direction_low_speed = 1;
	barChart_horizontal.direction_high_speed = 1;
	/* ---------------------------------------------------------------------------------------------------------------------------------------*/

	/* Verify attention check */
	// When the current trial is attention check (exact answer = 0)
	if(random_percentage_array[current_repetition][current_trial_per_repetition] == 0){
		// The acceptable range of given answer is from 0% to 10%
		if(trial_given_answer >= 0 && trial_given_answer <= 10){
			console.log("Attention check passed");
		}else{
			current_failed_times++;
			measurements['attention_check_errors_number'] = current_failed_times;
			console.log("current_failed_times = " + current_failed_times);
		}
	}

	// When the failed times up to the threshold, failed times >=6
	if(current_failed_times > attentioncheck_threshold){
		// Kick out the participant
		excluded = true;
		// Block the participant at the page "Attention check failed page"
		$('#new_visualization').hide();
		$('#attentioncheck').show();
		$(":button").hide();
		// Add info in Ajax
  		measurements['excluded'] = excluded;

  		$.ajax({
	        url: 'ajax/excluded.php',
	        type: 'POST',
	        data: JSON.stringify(measurements),
	        contentType: 'application/json',
      	});
	}
	/* ---------------------------------------------------------------------------------------------------------------------------------------*/

	/* Push parameters into ajax and submit answers */
	measurements['color_condiiton'] = condition_color;
	measurements['speed_condiiton'] = random_speed_array[0] + " | " + random_speed_array[1] + " | " + random_speed_array[2] + " | " + random_speed_array[3];

	measurements['trial_number'] = current_trial + 1;
	measurements['current_experiment_part'] = current_part + 1;
	measurements['current_repetition_per_part'] = current_repetition + 1;
	measurements['current_trial_per_repetition'] = current_trial_per_repetition + 1;

	measurements['oneCM'] = oneCM;
	measurements['soccerball_diameter_cm'] = soccerballDiameter_cm;
	measurements['soccerball_diameter_pixel'] = soccerballDiameter;

	measurements['barchart_length_cm'] = barchart_length_cm;
	measurements['barchart_length_pixel'] = barchart_length;
	measurements['barchart_width_cm'] = barchart_width_cm;
	measurements['barchart_width_pixel'] = barchart_width;

	// if(movingFlag){
	// 	if(random_speed_array[current_part] == 30){
	// 		measurements['vis_travel_distance_cm'] = barchart_travel_distance_cm;
	// 		measurements['vis_travel_distance_pixel'] = barchart_travel_distance_pixel;
	// 	}else if(random_speed_array[current_part] == 15){
	// 		measurements['vis_travel_distance_cm'] = barchart_travel_distance_cm/2;
	// 		measurements['vis_travel_distance_pixel'] = barchart_travel_distance_pixel/2;
	// 	}
	// }else if(!movingFlag){
	// 	measurements['vis_travel_distance_cm'] = 0;
	// 	measurements['vis_travel_distance_pixel'] = 0;
	// }

	// linear motion, speed = 30
	if(random_speed_array[current_part] == original_speed_array[3]){
		measurements['vis_travel_distance_cm'] = barchart_travel_distance_cm;
		measurements['vis_travel_distance_pixel'] = barchart_travel_distance_pixel;
	// linear motion, speed = 15
	}else if(random_speed_array[current_part] == original_speed_array[2]){
		measurements['vis_travel_distance_cm'] = barchart_travel_distance_cm/2;
		measurements['vis_travel_distance_pixel'] = barchart_travel_distance_pixel/2;
	// irregular movement
	}else if(random_speed_array[current_part] < 0){
		measurements['vis_travel_distance_cm'] = distance_travelled_totally/oneCM;
		measurements['vis_travel_distance_pixel'] = distance_travelled_totally;
	}

	// Add a colone to record movment kinds and current trajectory
	if(random_speed_array[current_part] > 0){
		measurements['movement_kind'] = "Linear motion";
		if(random_speed_array[current_part] == original_speed_array[3]){
			measurements['current_trajectory'] = "Linear - 48cm round trip";
		}else if(random_speed_array[current_part] == original_speed_array[2]){
			measurements['current_trajectory'] = "Linear - 24cm round trip";
		}
	}else if(random_speed_array[current_part] < 0){
		measurements['movement_kind'] = "Irregular motion";
		if(random_speed_array[current_part] == original_speed_array[1]){
			measurements['current_trajectory'] = "Slow trial movement - Path_" + trial_subscript[path_counter];
		}else if(random_speed_array[current_part] == original_speed_array[0]){
			measurements['current_trajectory'] = "Fast trial movement - Path_" + trial_subscript[path_counter];
		}
		
	}


	measurements['vis_display_duration'] = display_millisecond;
  	
	measurements['speed_duration'] = random_speed_array_string[current_part];
	measurements['current_speed_cm_s'] = Math.abs(random_speed_array[current_part]);

	measurements['correct_answer'] = random_percentage_array[current_repetition][current_trial_per_repetition] * 100;
	measurements['given_answer'] = trial_given_answer;

	$.ajax({
      url: 'ajax/results_format.php',
      type: 'POST',
      data: JSON.stringify(measurements),
      contentType: 'application/json',
      success: function (data) {
      // console.log(measurements);
      }
  	});	

	if(excluded){
  		$('#new_visualization').hide().promise().done(() => $('#attentioncheck').show());
  		$(":button").hide();
  	}
	/* ---------------------------------------------------------------------------------------------------------------------------------------*/

	/* Count balance */
	if(current_trial == (threshold_trial-1) ){
		endExperiment = true;
	}
	if( (current_repetition == (threshold_repetitions-1)) && (current_trial_per_repetition == (threshold_trial_per_repetition-1)) ){
		endTrialRepetition = true;
	}
	if(!endTrialRepetition){
		if( current_trial_per_repetition == (threshold_trial_per_repetition-1) ){
			endTrialPerRepetition = true;
		}
		if(endTrialPerRepetition){
			current_repetition++;
			current_trial_per_repetition = 0;
			endTrialPerRepetition = false;
		}else{
			current_trial_per_repetition++;
		}
	}
	// Trial count self plus one
	if(!endExperiment){
		current_trial++;
	}
	/* ---------------------------------------------------------------------------------------------------------------------------------------*/

	/* Prepare the next trial afterimage blocks and stimuli */
	if(!endTrialRepetition){
		// console.log("original_speed_array: " + original_speed_array);
		// console.log("random_speed_array: " + random_speed_array);
		// console.log("correct_answer = " + random_percentage_array[current_repetition][current_trial_per_repetition]);
		// console.log("speed = " + random_speed_array[current_part]);
		/* Do not arrive at the end of the experiment, should go on, prepare the stimuli and call for animation according to the movement kinds */
		// Linear motion
		if(random_speed_array[current_part] > 0){
			// Prepare the trial stimuli and call for animation
			start_answer_time = lastFinishedTime = initializationTime = Date.now();
			displayed_one_time = false;
			// console.log("start_answer_time: " + start_answer_time);
			callback_id = requestAnimationFrame(function (){timeBasedAnimation(myCanvas_trial, context_trial, random_speed_array[current_part], random_percentage_array[current_repetition][current_trial_per_repetition])});

		// Irregular movement
		}else if(random_speed_array[current_part] < 0){
			/* Reset the flags and counters */
			firstFrame_irregular = true;
			// Go to next path in th random array
			path_counter ++;
			if(path_counter == 21){
				path_counter = 0;
			} 
			coordinate_counter = undefined;
			distance_travelled_totally = 0;
			coordinatesPlus  = false; coordinatesMinus = false;

			// console.log("irregular movement trial");
			// console.log("speed = " + random_speed_array[current_part]);
			initializationTime_irregular = lastFinishedTime_irregular = Date.now();
			callback_id = requestAnimationFrame(function (){irregularTBA(cvs_tra_tri_3, ctx_tra_tri_3, random_speed_array[current_part], random_percentage_array[current_repetition][current_trial_per_repetition])});
		}

	}else if(endTrialRepetition && !endExperiment){
		// Disable the experiment div, enable the confidence survey
		document.getElementById("trial_stimuli").style.display = 'none';
		document.getElementById("confidence_survey").style.display = 'block';
		
		// Reset the endTrialPart flag
		endTrialRepetition = false;
		// Reset current_repetition and the current_trial_per_repetition
		current_repetition = undefined;
		current_trial_per_repetition = undefined;
		
	}else if(endTrialRepetition && endExperiment){
		console.log("end experiment");
		
		// Get the end experiment time
		endExperiment_time = Date.now();

		// Disable the experiment div, enable the confidence survey
		document.getElementById("trial_stimuli").style.display = 'none';
		document.getElementById("confidence_survey").style.display = 'block';
	}
	/* ---------------------------------------------------------------------------------------------------------------------------------------*/	
}

function checkConfidenceSurveyRadio(radio){
	confidence_survey_answered = true;
	confidence_survey_answer = radio.value;
	if(confidence_survey_answered){
		document.getElementById("confidence_survey_button").style.display = 'block';
	}else{
		document.getElementById("confidence_survey_button").style.display = 'none';
	}
}

$('.condifence_survey_radio').on('input', function() {
    confidence_survey_answered = true;
    confidence_survey_answer = $(this).val();
    if (confidence_survey_answered) {
    	document.getElementById("confidence_survey_button").style.display = 'block';
	}else{
		document.getElementById("confidence_survey_button").style.display = 'none';
	}
});

function cleanConfidenceSurveyRadio(){
	let x = document.getElementsByClassName("condifence_survey_radio");
	for(let i=0; i<x.length; i++){
		if(x[i].checked){
			x[i].checked = false;
		}
	}
	confidence_survey_answer = -1;
	confidence_survey_answered = false;
	document.getElementById("confidence_survey_button").style.display = 'none';
}

function submitConfidenceSurvey(button){
	// Submit the confidence survery answer
	if(current_part == 0){
		measurements['confidence_survey_answer_1'] = confidence_survey_answer;
	}else if(current_part == 1){
		measurements['confidence_survey_answer_2'] = confidence_survey_answer;
	}else if(current_part == 2){
		measurements['confidence_survey_answer_3'] = confidence_survey_answer;
	}else if(current_part == 3){
		measurements['confidence_survey_answer_4'] = confidence_survey_answer;
	}
	$.ajax({
      url: 'ajax/questionnaire_confidence.php',
      type: 'POST',
      data: JSON.stringify(measurements),
      contentType: 'application/json',
      success: function (data) {
      	// console.log(measurements);
      }
  	});

	// Reset the radio button
	cleanConfidenceSurveyRadio();

	// If the experiment is still going on
  	if(!endTrialPart){
  		document.getElementById("confidence_survey").style.display = 'none';
  		document.getElementById("training_part").style.display = 'block';
  	// If the whole process finished, go into post_questionnaire.php
  	}else if(endTrialPart){
  		document.getElementById("confidence_survey").style.display = 'none';
  		$('#new_visualization').hide();
  		$('#postquestionnaire').show();
  	}
}

</script>