<div class="row">
	<div class="col">
		<h2>Thank you</h2>

		<div id = "thankyou_div_1" style="display: block;">
			<p>You have finished the entire experiment, thank you so much for your participation!</p>
			<p>Do you have any comment about the study, for example concerning the clarity of the instructions or technical issues you might have experienced? (optional)</p>
			<div>
          		<textarea class="form-control" id="general_comments" rows="4" cols="80" placeholder="Any comments? (optional)" style="width: 1100px;"></textarea><br>
          		<input type="button" value="Submit and Get Paid" id = "thankyou_btn" style="background-color: #006400; color: #FFFFFF; float: right;" onclick="getPaid(this)">
      		</div>
		</div>

		<div id = "thankyou_div_2" style="display: none;">
			<p>You have completed the experiment.</p>
			<p>Click <a href="<?php echo $COMPLETION_URL;?>" target="_BLANK"><strong>here</strong></a> to report your task completion to the Prolific system.</p>
		</div>
		
	</div>	
</div>

<script type="text/javascript">
// Hide the next button
document.addEventListener("DOMContentLoaded", function(){
	let nextbutton = document.getElementById("btn_<?php echo $id;?>");
	nextbutton.style.display = 'none';
});

function getPaid(button){
	// Get comments
	measurements['optionalComments'] = $("#general_comments").val();
	// Send comments
	$.ajax({
      url: 'ajax/questionnaire_confidence.php',
      type: 'POST',
      data: JSON.stringify(measurements),
      contentType: 'application/json',
      success: function (data) {
      	// console.log(measurements);
      }
    });

    //Get stop time for the whole experiment
    getpaid_time = Date.now();
    // Calculate the duration
    wholeexperiment_timetaken_ms = getpaid_time - welcome_time;
    wholeexperiment_timetaken_min = Math.round(wholeexperiment_timetaken_ms/1000/60);

    measurements['start_time'] = welcome_time;
    measurements['stop_time'] = getpaid_time;
    measurements['duration_ms'] = wholeexperiment_timetaken_ms;
    measurements['duration_min'] = wholeexperiment_timetaken_min;

    // Calculate the real experiment duration
    realExperiment_timetaken_ms = endExperiment_time - startTraining_time;
    realExperiment_timetaken_min = Math.round(realExperiment_timetaken_ms/1000/60);

    measurements['start_training_time'] = welcome_time;
    measurements['end_experiment_time'] = getpaid_time;
    measurements['real_duration_ms'] = realExperiment_timetaken_ms;
    measurements['real_duration_min'] = realExperiment_timetaken_min;
    
    // Send the whole time taken
	$.ajax({
      url: 'ajax/time_taken.php',
      type: 'POST',
      data: JSON.stringify(measurements),
      contentType: 'application/json',
      success: function (data) {
      	// console.log(measurements);
      }
    });

    // Hide the div 1
    document.getElementById("thankyou_div_1").style.display = 'none';
    // Display the div 2
    document.getElementById("thankyou_div_2").style.display = 'block';
}

</script>