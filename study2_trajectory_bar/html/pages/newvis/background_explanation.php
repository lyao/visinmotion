<style type="text/css">

</style>


<div class="row">
  <div class="col">
    <h2>Background Explanation</h2>

    <div class="section" style = "text-align: center;">
      <img src="../html/img/png/bge_character_bar.png" alt="Soccer stadium image from TV" width="200" height="300" style="margin-left: 250px; margin-right: 250px;">
    </div>

    <div class = "section">
      <p>
        <b>Imagine this scenario:</b> 
        <br/>
        You are playing a video game where you control and move a character. The character starts out with full health but can lose health throughout the game. The character’s health state is displayed in a bar over its head. The <span style = "font-weight: bold; color: #E90738;">red</span> part indicates the character’s current health compared to how much health your character has already lost (in <span style = "font-weight: bold; color: #C3C1C1;">grey</span>).  We call this health indicator a bar chart.
      </p>
    </div>

    <div class = "section">
      <div id="bge_div" style="width: 200px; height: 150px; text-align: center; float: left; padding-top: 20px;">
        <canvas id = "bge_canvas1"></canvas>
      </div>
      <div style="width: 900px; height: 150px; float: right;">
        <ul>
          <li>
            The bar has two sections, the <span style = "font-weight: bold; color: #E90738;">red</span> represents the current health and the <span style = "font-weight: bold; color: #C3C1C1;">grey</span> represents lost health. The whole bar represents 100 percent of possible health.
          </li>
          <li>
            In the image on the left, your character is at 60% health and has lost 40% of its health (in <span style = "font-weight: bold; color: #C3C1C1;">grey</span>). 
          </li>
        </ul>
      </div>
    </div>
    
  </div>
</div>

<script type="text/javascript">
// Force the subject stays on this page for 5s, to read the sentences
// For formal study, waitTime_bge = 4; when test, waitTime_bge = 0 to avoid waiting
var waitTime_bge = 4;
var btn_bge = false;

// Disable the next button, set the attributes
document.addEventListener("DOMContentLoaded", function(){
  let nextButton = document.getElementById("btn_<?php echo $id;?>");
  nextButton.style.border = "none";
  nextButton.style.background = "#EDEDED";
  nextButton.style.color = "#A3A3A3";
  nextButton.disabled = true;
});

// Excute per second, insert the new button value, to let the subject know how much time left
var callback_id_bge = setInterval(function(){
  if(btn_bge){
                  if(waitTime_bge == 0){
                    let nextButton = document.getElementById("btn_<?php echo $id;?>");
                    btn_<?php echo $id;?>.innerHTML = "I understand. Go to Task Desciption.";
                    nextButton.style.background = "#006400";
                    nextButton.style.color = "#FFFFFF";
                    nextButton.disabled = false;
                  }else{
                    btn_<?php echo $id;?>.innerHTML = "You can click the button after "+ waitTime_bge +"s";
                    waitTime_bge--;
                  }
  }
}, 1000);
  
//Check the next button be pressed and initialized the static explanation donut group
$(document).on('click','#btn_<?php echo $id;?>',function(){
    clearInterval(callback_id_bge);
    btn_tskd[i_understand + 1] = true;
});
</script>

