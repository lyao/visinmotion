// Below are basic parameters especially concerning the size of the window.
// This is to make sure that the entire screen fits within the browser window of all participants.
// If these are changed, then the values indicated in the css file html/css/main.css for the selector #experiment need to be changed as well.
export const MIN_WINDOW_WIDTH = window.innerWidth;
export const MIN_WINDOW_HEIGHT = window.innerHeight;
export const CANVAS_WIDTH = window.innerWidth;



// These are constants for layout purposes.

export const BIG_SPACE = 100;
export const MEDIUM_SPACE = 40;
export const SMALL_SPACE = 10;

// Add your own parameters below.