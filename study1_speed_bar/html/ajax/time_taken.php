<?php

	$data = json_decode(file_get_contents('php://input'), true);

	$agree_filename = "../../results/format/duration/time_taken.csv";
  $exists = file_exists($agree_filename);
  $agree_file = fopen($agree_filename, "a+");

  if (!$exists){
    fwrite($agree_file, "timestamp,participant_id, study_id,session_id, color_condiiton,speed_condiiton, start_time, stop_time, duration_ms, duration_min");
  }

  fwrite($agree_file,
    PHP_EOL .
    date(DateTime::ISO8601) . ',' .
    $data["participant_id"] . ',' .

    $data["study_id"] . ',' .
    $data["session_id"]. ',' .

    $data["color_condiiton"] . ',' .
    $data["speed_condiiton"] . ',' .

    $data["start_time"] . ',' .
    $data["stop_time"] . ',' .
    $data["duration_ms"] . ',' .
    $data["duration_min"]

  );

  fclose($agree_file);
	exit;

?>