<div class="row">
  <div class="col">
    <h2>Welcome Page</h2>
    <p>You are being invited to participate in a study titled "Visualization in Motion". This study is conducted by Lijie Yao and Petra Isenberg from Inria (France), and Anastasia Bezerianos from LISN (France).</p>
	<p>The purpose of this study is to understand the accurate perception of data in moving charts. This experiment has two sections:</p>
	<ul>
		<li>a training section</li>
		<li>an experiment section</li>
	</ul>

	<p>Please take part in this study only in case that you <span style="font-weight: bold;">do not have problem with moving images on screens and do not have motion sensitivity (vestibular disorder)</span>.</p>

	<p>The minimum screen size for this study is <span style="font-weight: bold;">13"3-inches</span>. Normally most laptops (except the small Chromebooks, the old small MacBooks, and someother small-screen laptops) and desktops meet this requirement, however, all smartphones and most tablets will <span style="text-decoration: underline; ">not</span> meet the requirement.</p>

	<p>The whole experiment will take you <?php echo $EXP_DURATION ?> minutes to complete. Please <span style="font-weight: bold;">close unnecessary pages and reminders</span> for the following <?php echo $EXP_DURATION ?> minutes. You will be compensated <?php echo $EXP_PAYMENT?> GBP as long as you pass the calibration phase, pass the attention test trials, and complete the study.</p>

	<p>Your participation in this study is entirely voluntary and you can withdraw at any time.</p>

	<p>Please take part in this experiment <span style="font-weight: bold;">only one time</span>, if you have already completed it once, please leave the experiment. We will <span style="text-decoration: underline; ">not</span> pay for the participants who reload the page after finishing the calibration tool, so please do not reload the page during the experiment.</p>

	<p>We thank you for your interest in participating in this study.</p>

	<br><br><br>

    <p id="<?php echo $id;?>_endarea" style="font-weight: bold;">
    	Please click the button below to proceed to the informed consent:
     </p>
  </div>
</div>

<script type="text/javascript">
// Record the whole experiment start time & finish time, to calculate the experiment duration
var welcome_time, getpaid_time, wholeexperiment_timetaken_ms, wholeexperiment_timetaken_min;
// Force the subject stays on this page for 5s, to read the sentences
var waitTime_wel = 4;
var btn_wel = false;

// Disable the next button, set the attributes
document.addEventListener("DOMContentLoaded", function(){
	let nextButton = document.getElementById("btn_<?php echo $id;?>");
	nextButton.style.border = "none";
	nextButton.style.background = "#EDEDED";
	nextButton.style.color = "#A3A3A3";
	nextButton.disabled = true;
});

// Excute per second, insert the new button value, to let the subject know how much time left
var callback_id_wel = setInterval(function(){
									if(waitTime_wel == 0){
										let nextButton = document.getElementById("btn_<?php echo $id;?>");
										btn_<?php echo $id;?>.innerHTML = "Go to Consent Form";
										nextButton.style.background = "#006400";
										nextButton.style.color = "#FFFFFF";
										nextButton.disabled = false;
									}else{
										btn_<?php echo $id;?>.innerHTML = "You can click the button after "+ waitTime_wel +"s";
										waitTime_wel--;
									}
								}, 1000);

// Once the next button be clicked, clear the setInterval
$(document).on('click','#btn_<?php echo $id;?>',function(){
	clearInterval(callback_id_wel);
	btn_wel = true;
	welcome_time = Date.now();
});



</script>
