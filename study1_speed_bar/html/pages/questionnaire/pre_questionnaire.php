<!-- <style>
  div.participant_gender_textarea{
    visibility: hidden;
  }
</style>  -->

<style type="text/css">
  #preqn_sketch{
    text-align: center;
    margin: auto;
  }
  #preqn_img{
    text-align: center ;
    margin: auto;
  }

</style>

<div id="row">
  <div id="col">
 <!--  <div class="ratings cml_field"><h2 class="legend">May I have your age?</h2>
    <div class="cml_row">

      <input type="radio" id="participant_age_1" name="participant_age" value="18-24" onclick = "getAgeValue(this)">
      <label for="participant_age_1">18-24&nbsp;&nbsp;&nbsp;&nbsp;</label>

      <input type="radio" id="participant_age_2" name="participant_age" value="25-34" onclick = "getAgeValue(this)">
      <label for="participant_age_2">25-34&nbsp;&nbsp;&nbsp;&nbsp;</label>

      <input type="radio" id="participant_age_3" name="participant_age" value="35-44" onclick = "getAgeValue(this)">
      <label for="participant_age_3">35-44&nbsp;&nbsp;&nbsp;&nbsp;</label>

      <input type="radio" id="sparticipant_age_4" name="participant_age" value="45-54" onclick = "getAgeValue(this)">
      <label for="sparticipant_age_4">45-54&nbsp;&nbsp;&nbsp;&nbsp;</label>

      <input type="radio" id="participant_age_5" name="participant_age" value="55-64" onclick = "getAgeValue(this)">
      <label for="participant_age_5">55-64&nbsp;&nbsp;&nbsp;&nbsp;</label>

    </div>
  </div>

  <div class="ratings cml_field"><h2 class="legend">Do you self identify as:</h2>
    <div class="cml_row">
      <input type="radio" id="self_gender_identify_1" name="self_gender_identify" value="Female" onclick = "getGenderValue(this)">
      <label for="self_gender_identify_1">Female&nbsp;&nbsp;&nbsp;&nbsp;</label>

      <input type="radio" id="self_gender_identify_2" name="self_gender_identify" value="Male" onclick = "getGenderValue(this)">
      <label for="self_gender_identify_2">Male&nbsp;&nbsp;&nbsp;&nbsp;</label>

      <input type="radio" id="self_gender_identify_3" name="self_gender_identify" value="Not_listed" onclick = "getGenderValue(this)">
      <label for="self_gender_identify_3">Other&nbsp;&nbsp;&nbsp;&nbsp;</label>

      <input type="radio" id="self_gender_identify_4" name="self_gender_identify" value="Perfer_not_to_answer" onclick = "getGenderValue(this)">
      <label for="self_gender_identify_4">Prefer not to answer&nbsp;&nbsp;&nbsp;&nbsp;</label>
    </div>

    <div class="participant_gender_textarea" id="participant_gender_textarea">
      <textarea name="participant_gender" id="participant_gender" oninput="getGenderTextarea(this)" placeholder="Please enter your gender identify here." style="width:300px; text-align: center" required autofocus=""></textarea> 
    </div>
  </div> -->

    <h2>Pre-questionnaire</h2>
    <div class = "section" id = "preqn_sketch">
      <canvas id = "preqn_canvas1"></canvas>
    </div>
     
    <div class="ratings cml_field">
      <h2 class="legend">How experienced are you with reading bar charts like in the examples above?</h2>
      <div class="cml_row">
      
        <table>
          <thead>
            <tr>
              <th></th>
              
              <th class="likert">1</th>
              
              <th class="likert">2</th>
              
              <th class="likert">3</th>
              
              <th class="likert">4</th>
              
              <th class="likert">5</th>
              
              <th class="likert">6</th>
              
              <th class="likert">7</th>
              
              <th class="likert">8</th>
              
              <th class="likert">9</th>

              <th class="likert">10</th>
              
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Not at all experienced&nbsp;&nbsp;&nbsp;</td>
              
              <td class="likert"><input name="how_experienced_are_you_with_reading_these_charts" type="radio" value="1" class="how_experienced_are_you_with_reading_these_charts validates-required validates">
        </td>
              
              <td class="likert"><input name="how_experienced_are_you_with_reading_these_charts" type="radio" value="2" class="how_experienced_are_you_with_reading_these_charts validates-required validates">
        </td>
              
              <td class="likert"><input name="how_experienced_are_you_with_reading_these_charts" type="radio" value="3" class="how_experienced_are_you_with_reading_these_charts validates-required validates">
        </td>
              
              <td class="likert"><input name="how_experienced_are_you_with_reading_these_charts" type="radio" value="4" class="how_experienced_are_you_with_reading_these_charts validates-required validates">
        </td>
              
              <td class="likert"><input name="how_experienced_are_you_with_reading_these_charts" type="radio" value="5" class="how_experienced_are_you_with_reading_these_charts validates-required validates">
        </td>
              
              <td class="likert"><input name="how_experienced_are_you_with_reading_these_charts" type="radio" value="6" class="how_experienced_are_you_with_reading_these_charts validates-required validates">
        </td>
              
              <td class="likert"><input name="how_experienced_are_you_with_reading_these_charts" type="radio" value="7" class="how_experienced_are_you_with_reading_these_charts validates-required validates">
        </td>
              
              <td class="likert"><input name="how_experienced_are_you_with_reading_these_charts" type="radio" value="8" class="how_experienced_are_you_with_reading_these_charts validates-required validates">
        </td>
              
              <td class="likert"><input name="how_experienced_are_you_with_reading_these_charts" type="radio" value="9" class="how_experienced_are_you_with_reading_these_charts validates-required validates">
        </td>
              
              <td class="likert"><input name="how_experienced_are_you_with_reading_these_charts" type="radio" value="10" class="how_experienced_are_you_with_reading_these_charts validates-required validates">
        </td>
              
              <td>&nbsp;&nbsp;Very experienced</td>
            </tr>
          </tbody>
        </table>
    </div>
    </div>

    <hr>

    <div class = "section" id = "preqn_img">
      <div>
        <img src="../html/img/jpg/watching_soccerball_match.jpg" width = "400" height="250" style="float: left;margin-left: 100px;margin-right: 50px;">
      </div>
       <div>
        <img src="../html/img/png/video_game.png" width = "400" height="250" style="margin-left: 50px; margin-right: 100px;">
      </div>
    </div>
    
    <div class="ratings cml_field" id = "preqn_second_qs">
      <h2 class="legend">How often do you watch team sports on TV or play video games (eg. WoW, FIFA...)?</h2>
      <div class="cml_row">
      
        <table>
          <thead>
            <tr>
              <th></th>
              
              <th class="likert">1</th>
              
              <th class="likert">2</th>
              
              <th class="likert">3</th>
              
              <th class="likert">4</th>
              
              <th class="likert">5</th>
              
              <th class="likert">6</th>
              
              <th class="likert">7</th>
              
              <th class="likert">8</th>
              
              <th class="likert">9</th>

              <th class="likert">10</th>
              
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Never&nbsp;&nbsp;&nbsp;</td>
              
              <td class="likert"><input name="how_often_do_you_watch_ballgames_videogames" type="radio" value="1" class="how_often_do_you_watch_ballgames_videogames validates-required validates">
        </td>
              
              <td class="likert"><input name="how_often_do_you_watch_ballgames_videogames" type="radio" value="2" class="how_often_do_you_watch_ballgames_videogames validates-required validates">
        </td>
              
              <td class="likert"><input name="how_often_do_you_watch_ballgames_videogames" type="radio" value="3" class="how_often_do_you_watch_ballgames_videogames validates-required validates">
        </td>
              
              <td class="likert"><input name="how_often_do_you_watch_ballgames_videogames" type="radio" value="4" class="how_often_do_you_watch_ballgames_videogames validates-required validates">
        </td>
              
              <td class="likert"><input name="how_often_do_you_watch_ballgames_videogames" type="radio" value="5" class="how_often_do_you_watch_ballgames_videogames validates-required validates">
        </td>
              
              <td class="likert"><input name="how_often_do_you_watch_ballgames_videogames" type="radio" value="6" class="how_often_do_you_watch_ballgames_videogames validates-required validates">
        </td>
              
              <td class="likert"><input name="how_often_do_you_watch_ballgames_videogames" type="radio" value="7" class="how_often_do_you_watch_ballgames_videogames validates-required validates">
        </td>
              
              <td class="likert"><input name="how_often_do_you_watch_ballgames_videogames" type="radio" value="8" class="how_often_do_you_watch_ballgames_videogames validates-required validates">
        </td>
              
              <td class="likert"><input name="how_often_do_you_watch_ballgames_videogames" type="radio" value="9" class="how_often_do_you_watch_ballgames_videogames validates-required validates">
        </td>
              
              <td class="likert"><input name="how_often_do_you_watch_ballgames_videogames" type="radio" value="10" class="how_often_do_you_watch_ballgames_videogames validates-required validates">
        </td>
              
              <td>&nbsp;&nbsp;Everyday</td>
            </tr>
          </tbody>
        </table>
    </div>
    </div>

  </div>
</div>



<script type="text/javascript">

// Disable the next button, set the attributes
document.addEventListener("DOMContentLoaded", function(){
  let nextButton = document.getElementById("btn_<?php echo $id;?>");
  nextButton.style.border = "none";
  nextButton.style.background = "#EDEDED";
  nextButton.style.color = "#A3A3A3";
  nextButton.disabled = true;
});

// Check if the frist question be answered 
$('.how_experienced_are_you_with_reading_these_charts').on('input', function() {
    experienced_answered = true;
    experienced_answer = $(this).val();

    // console.log("experienced_answer: " + experienced_answer);

    if (experienced_answered && frequency_answered){
      let nextButton = document.getElementById("btn_<?php echo $id;?>");
      btn_<?php echo $id;?>.innerHTML = "Submit and Go to the Background Explanation";
      nextButton.style.background = "#006400";
      nextButton.style.color = "#FFFFFF";
      nextButton.disabled = false;
    }
});

// Check if the second question be answered
$('.how_often_do_you_watch_ballgames_videogames').on('input', function() {
    frequency_answered = true;
    frequency_answer =$(this).val();

    // console.log("frequency_answer: " + frequency_answer);

    if (experienced_answered && frequency_answered){
      let nextButton = document.getElementById("btn_<?php echo $id;?>");
      btn_<?php echo $id;?>.innerHTML = "Submit and Go to the Background Explanation";
      nextButton.style.background = "#006400";
      nextButton.style.color = "#FFFFFF";
      nextButton.disabled = false;
    } 
});

// Once click the submit button, push the answers into ajax and submit them
$(document).on('click','#btn_<?php echo $id;?>',function(){
    // measurements['participant_age'] = age_answer;
    // measurements['participant_gender'] = gender_answer;
    measurements['color_condiiton'] = condition_color;
    measurements['speed_condiiton'] = random_speed_array[0] + "- " + random_speed_array[1] + "- " + random_speed_array[2];
    measurements['how_experienced_are_you_with_reading_these_charts'] = experienced_answer;
    measurements['how_often_do_you_watch_ballgames_videogames'] = frequency_answer;

    $.ajax({
      url: 'ajax/questionnaire_confidence.php',
      type: 'POST',
      data: JSON.stringify(measurements),
      contentType: 'application/json',
      success: function (data) {
        // console.log(measurements);
      }
    });

    btn_bge = true;
});
</script>

<script type="text/javascript">
  /* Variables to record age and gender*/
  // var age_answered = false;
  // var gender_answered = false;
  // var age_answer = -1;
  // var gender_answer = -1;

  /* Functions to check age and gender*/
  /*
  function getAgeValue(theRadio){
    age_answered = true;
    age_answer = theRadio.value;
    // console.log("age_answered: "+ age_answered);
    // console.log("age_answer: "+ age_answer);
  }

  function getGenderValue(theRadio){
    var value = theRadio.value;
    // console.log("radion value: "+value);
    if(value == "Not_listed"){
      document.getElementById("participant_gender_textarea").style.visibility = "visible";
      // console.log("participant_gender_textarea visible");
      gender_answered = true;
      // gender_answer = $("#participant_gender_textarea").val();
    }else{
      document.getElementById("participant_gender_textarea").style.visibility = "hidden";
      gender_answered = true;
      gender_answer = value;
    }
    // console.log("gender_answered: "+ gender_answered);
    // console.log("gender_answer: "+ gender_answer);
  }

  function getGenderTextarea(theText){
    gender_answer = theText.value;
    // console.log("gender_answer: "+ gender_answer);
  }
  */
</script>