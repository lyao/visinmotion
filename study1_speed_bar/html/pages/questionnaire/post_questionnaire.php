<div id="row">
  <h2> Post-questionnaire </h2>
  <p>As the last step in this study please fill out a quick final questionnaire.</p>
  <hr>

  <div class="ratings cml_field">
    <h2 class="legend">How difficult did you find the task with no movement</h2>
      <div class="cml_row">
        <table>
          <thead>
            <tr>
              <th></th>
              <th class="likert">1</th>
              <th class="likert">2</th>
              <th class="likert">3</th>
              <th class="likert">4</th>
              <th class="likert">5</th>
              <th class="likert">6</th>
              <th class="likert">7</th>
              <th class="likert">8</th>
              <th class="likert">9</th>
              <th class="likert">10</th>  
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Not at all difficult&nbsp;&nbsp;&nbsp;</td>
              <td class="likert"><input name="how_difficult_no_movement_task" type="radio" value="1" class="how_difficult_no_movement_task validates-required validates">
              </td>
              <td class="likert"><input name="how_difficult_no_movement_task" type="radio" value="2" class="how_difficult_no_movement_task validates-required validates">
              </td>
              <td class="likert"><input name="how_difficult_no_movement_task" type="radio" value="3" class="how_difficult_no_movement_task validates-required validates">
              </td>
              <td class="likert"><input name="how_difficult_no_movement_task" type="radio" value="4" class="how_difficult_no_movement_task validates-required validates">
              </td>
              <td class="likert"><input name="how_difficult_no_movement_task" type="radio" value="5" class="how_difficult_no_movement_task validates-required validates">
              </td>
              <td class="likert"><input name="how_difficult_no_movement_task" type="radio" value="6" class="how_difficult_no_movement_task validates-required validates">
              </td> 
              <td class="likert"><input name="how_difficult_no_movement_task" type="radio" value="7" class="how_difficult_no_movement_task validates-required validates">
              </td> 
              <td class="likert"><input name="how_difficult_no_movement_task" type="radio" value="8" class="how_difficult_no_movement_task validates-required validates">
              </td>
              <td class="likert"><input name="how_difficult_no_movement_task" type="radio" value="9" class="how_difficult_no_movement_task validates-required validates">
              </td>
              <td class="likert"><input name="how_difficult_no_movement_task" type="radio" value="10" class="how_difficult_no_movement_task validates-required validates">
              </td>             
              <td>&nbsp;&nbsp;Very difficult</td>
            </tr>
          </tbody>
        </table>
      </div>
  </div>
  
  <hr>

  <div class="ratings cml_field">
    <h2 class="legend">How difficult did you find the task with slow movement</h2>
      <div class="cml_row">
        <table>
          <thead>
            <tr>
              <th></th>
              
              <th class="likert">1</th>
              
              <th class="likert">2</th>
              
              <th class="likert">3</th>
              
              <th class="likert">4</th>
              
              <th class="likert">5</th>
              
              <th class="likert">6</th>
              
              <th class="likert">7</th>
              
              <th class="likert">8</th>
              
              <th class="likert">9</th>

              <th class="likert">10</th>
              
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Not at all difficult&nbsp;&nbsp;&nbsp;</td>
              
              <td class="likert"><input name="how_difficult_slow_movement_task" type="radio" value="1" class="how_difficult_slow_movement_task validates-required validates">
              </td>
              
              <td class="likert"><input name="how_difficult_slow_movement_task" type="radio" value="2" class="how_difficult_slow_movement_task validates-required validates">
              </td>
              
              <td class="likert"><input name="how_difficult_slow_movement_task" type="radio" value="3" class="how_difficult_slow_movement_task validates-required validates">
              </td>
              
              <td class="likert"><input name="how_difficult_slow_movement_task" type="radio" value="4" class="how_difficult_slow_movement_task validates-required validates">
              </td>
              
              <td class="likert"><input name="how_difficult_slow_movement_task" type="radio" value="5" class="how_difficult_slow_movement_task validates-required validates">
              </td>
              
              <td class="likert"><input name="how_difficult_slow_movement_task" type="radio" value="6" class="how_difficult_slow_movement_task validates-required validates">
              </td>
              
              <td class="likert"><input name="how_difficult_slow_movement_task" type="radio" value="7" class="how_difficult_slow_movement_task validates-required validates">
              </td>
              
              <td class="likert"><input name="how_difficult_slow_movement_task" type="radio" value="8" class="how_difficult_slow_movement_task validates-required validates">
              </td>
              
              <td class="likert"><input name="how_difficult_slow_movement_task" type="radio" value="9" class="how_difficult_slow_movement_task validates-required validates">
              </td>

              <td class="likert"><input name="how_difficult_slow_movement_task" type="radio" value="10" class="how_difficult_slow_movement_task validates-required validates">
              </td>
              
              <td>&nbsp;&nbsp;Very difficult</td>
            </tr>
          </tbody>
        </table>
      </div>
  </div>

  <hr>

  <div class="ratings cml_field">
    <h2 class="legend">How difficult did you find the task with fast movement</h2>
      <div class="cml_row"> 
        <table>
          <thead>
            <tr>
              <th></th>
              
              <th class="likert">1</th>
              
              <th class="likert">2</th>
              
              <th class="likert">3</th>
              
              <th class="likert">4</th>
              
              <th class="likert">5</th>
              
              <th class="likert">6</th>
              
              <th class="likert">7</th>
              
              <th class="likert">8</th>
              
              <th class="likert">9</th>

              <th class="likert">10</th>
              
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>Not at all difficult&nbsp;&nbsp;&nbsp;</td>
              
              <td class="likert"><input name="how_difficult_fast_movement_task" type="radio" value="1" class="how_difficult_fast_movement_task validates-required validates">
              </td>
              
              <td class="likert"><input name="how_difficult_fast_movement_task" type="radio" value="2" class="how_difficult_fast_movement_task validates-required validates">
              </td>
              
              <td class="likert"><input name="how_difficult_fast_movement_task" type="radio" value="3" class="how_difficult_fast_movement_task validates-required validates">
              </td>
              
              <td class="likert"><input name="how_difficult_fast_movement_task" type="radio" value="4" class="how_difficult_fast_movement_task validates-required validates">
              </td>
              
              <td class="likert"><input name="how_difficult_fast_movement_task" type="radio" value="5" class="how_difficult_fast_movement_task validates-required validates">
              </td>
              
              <td class="likert"><input name="how_difficult_fast_movement_task" type="radio" value="6" class="how_difficult_fast_movement_task validates-required validates">
              </td>
              
              <td class="likert"><input name="how_difficult_fast_movement_task" type="radio" value="7" class="how_difficult_fast_movement_task validates-required validates">
              </td>
              
              <td class="likert"><input name="how_difficult_fast_movement_task" type="radio" value="8" class="how_difficult_fast_movement_task validates-required validates">
              </td>
              
              <td class="likert"><input name="how_difficult_fast_movement_task" type="radio" value="9" class="how_difficult_fast_movement_task validates-required validates">
              </td>

              <td class="likert"><input name="how_difficult_fast_movement_task" type="radio" value="10" class="how_difficult_fast_movement_task validates-required validates">
              </td>
              
              <td>&nbsp;&nbsp;Very difficult</td>
            </tr>
          </tbody>
        </table>
      </div>
  </div>

  <hr>

  <div class="ratings cml_field">
    <h2 class="legend">Describe your strategy in solving the tasks with motion: </h2>
      <div class="form-group">
          <textarea class="form-control" id="strategy_description" rows="4" cols="80" placeholder="How do you read the chart when it's moving? (optional)"></textarea>
      </div>

  </div>

<script type="text/javascript">

// Disable the next button, set the attributes
document.addEventListener("DOMContentLoaded", function(){
  let nextButton = document.getElementById("btn_<?php echo $id;?>");
  nextButton.style.border = "none";
  nextButton.style.background = "#EDEDED";
  nextButton.style.color = "#A3A3A3";
  nextButton.disabled = true;
});

$('.how_difficult_no_movement_task').on('input', function() {
    no_movement_answered = true;
    no_movement_answer = $(this).val();
    // make button active as soon as 3 radio buttons have been checkhed
    if (no_movement_answered && slow_movement_answered && fast_movement_answered){
      let nextButton = document.getElementById("btn_<?php echo $id;?>");
      btn_<?php echo $id;?>.innerHTML = "Submit and Go to the End";
      nextButton.style.background = "#006400";
      nextButton.style.color = "#FFFFFF";
      nextButton.disabled = false;
    } 
});

$('.how_difficult_slow_movement_task').on('input', function() {
    slow_movement_answered = true;
    slow_movement_answer = $(this).val();
    // make button active as soon as 3 radio buttons have been checkhed
    if (no_movement_answered && slow_movement_answered && fast_movement_answered){
      let nextButton = document.getElementById("btn_<?php echo $id;?>");
      btn_<?php echo $id;?>.innerHTML = "Submit and Go to the End";
      nextButton.style.background = "#006400";
      nextButton.style.color = "#FFFFFF";
      nextButton.disabled = false;
    } 
});

$('.how_difficult_fast_movement_task').on('input', function() {
    fast_movement_answered = true;
    fast_movement_answer = $(this).val();
    // make button active as soon as 3 radio buttons have been checkhed
    if (no_movement_answered && slow_movement_answered && fast_movement_answered){
      let nextButton = document.getElementById("btn_<?php echo $id;?>");
      btn_<?php echo $id;?>.innerHTML = "Submit and Go to the End";
      nextButton.style.background = "#006400";
      nextButton.style.color = "#FFFFFF";
      nextButton.disabled = false;
    } 
});

// When click on the next button, submit all radio button answers and text area
$('body').on('next', function(e, type){
  if (type === '<?php echo $id;?>'){
    measurements['how_difficult_no_movement_task'] = no_movement_answer;
    measurements['how_difficult_slow_movement_task'] = slow_movement_answer;
    measurements['how_difficult_fast_movement_task'] = fast_movement_answer;
    measurements['strategy_description'] = $("#strategy_description").val();

    $.ajax({
      url: 'ajax/questionnaire_confidence.php',
      type: 'POST',
      data: JSON.stringify(measurements),
      contentType: 'application/json',
    });
  }
});
</script>
</div>