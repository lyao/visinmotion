<?php

	$data = json_decode(file_get_contents('php://input'), true);

	$agree_filename = "../../results/format/agreed/consent_form.csv";
  $exists = file_exists($agree_filename);
  $agree_file = fopen($agree_filename, "a+");

  if (!$exists){
    fwrite($agree_file, "timestamp,participant_id, study_id,session_id, color_condiiton, speed_condiiton, browser_name, browser_version, os, consent_form_agreed");
  }

  fwrite($agree_file,
    PHP_EOL .
    date(DateTime::ISO8601) . ',' .
    $data["participant_id"] . ',' .

    $data["study_id"] . ',' .
    $data["session_id"]. ',' .

    $data["color_condiiton"] . ',' .
    $data["speed_condiiton"] . ',' .

    $data['browser_name']   . ',' .
    $data["browser_version"]. ',' .
    $data["os"] . ',' .

    $data["consent_form_agreed"]
   
  );

  fclose($agree_file);
	exit;

?>