<?php

	$data = json_decode(file_get_contents('php://input'), true);

	$questionnaire_confidence_filename = "../../results/format/survey/" . $data["participant_id"] . '_'. 'questionnaire_confidence' . '.csv';
  $exists = file_exists($questionnaire_confidence_filename);
  $questionnaire_confidence_file = fopen($questionnaire_confidence_filename, "a+");

  if (!$exists){
    fwrite($questionnaire_confidence_file, "participant_id,timestamp, study_id,session_id, color_condiiton, speed_condiiton, browser_name, browser_version, os, how_experienced_are_you_with_reading_these_charts, how_often_do_you_watch_ballgames_videogames, confidence_survey_answer_1, confidence_survey_answer_2, confidence_survey_answer_3, confidence_survey_answer_4, how_difficult_slow_linear_movement_task, how_difficult_slow_curve_movement_task, how_difficult_fast_linear_movement_task, how_difficult_fast_curve_movement_task, strategy_description, general_comments");
  }

  fwrite($questionnaire_confidence_file,
    PHP_EOL .
    $data["participant_id"] . ',' .
    date(DateTime::ISO8601) . ',' .
    $data["study_id"] . ',' .
    $data["session_id"] . ',' .

    $data["color_condiiton"] . ',' .
    $data["speed_condiiton"] . ',' .

    $data['browser_name']   . ',' .
    $data["browser_version"]. ',' .
    $data["os"] . ',' .

    $data["how_experienced_are_you_with_reading_these_charts"] . ',' .
    $data["how_often_do_you_watch_ballgames_videogames"] . ',' .

    $data["confidence_survey_answer_1"] . ',' .
    $data["confidence_survey_answer_2"] . ',' .
    $data["confidence_survey_answer_3"] . ',' .
    $data["confidence_survey_answer_4"] . ',' .

    $data["how_difficult_slow_linear_movement_task"] . ',' .
    $data["how_difficult_slow_curve_movement_task"]. ',' .
    $data["how_difficult_fast_linear_movement_task"]. ',' .
    $data["how_difficult_fast_curve_movement_task"]. ',' .

    $data["strategy_description"]. ',' .
    $data["optionalComments"]
  );

  fclose($questionnaire_confidence_file);
	exit;

?>