<?php

	$data = json_decode(file_get_contents('php://input'), true);

	$agree_filename = "../../results/format/duration/time_taken.csv";
  $exists = file_exists($agree_filename);
  $agree_file = fopen($agree_filename, "a+");

  if (!$exists){
    fwrite($agree_file, "timestamp,participant_id, study_id,session_id, color_condiiton,speed_condiiton, start_time, stop_time, duration_ms, start_training_time, end_experiment_time, real_duration_ms, duration_min, real_duration_min");
  }

  fwrite($agree_file,
    PHP_EOL .
    date(DateTime::ISO8601) . ',' .
    $data["participant_id"] . ',' .

    $data["study_id"] . ',' .
    $data["session_id"]. ',' .

    $data["color_condiiton"] . ',' .
    $data["speed_condiiton"] . ',' .

    $data["start_time"] . ',' .
    $data["stop_time"] . ',' .
    $data["duration_ms"] . ',' .

    $data["start_training_time"] . ',' .
    $data["end_experiment_time"] . ',' .
    $data["real_duration_ms"] . ',' .

    $data["duration_min"]. ',' .
    $data["real_duration_min"]

  );

  fclose($agree_file);
	exit;

?>