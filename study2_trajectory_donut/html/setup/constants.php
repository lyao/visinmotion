<?php
	/* General parameters of your experiment */

	// The csv file url array
	$csv_url_slow_trial = array(); $csv_url_fast_trial = array();
	$csv_url_slow_training = array(); $csv_url_fast_training = array();	

	// The path count, 21 path for trial and 14 for training (training part needs at least 6 traces)
	$slow_path_count_trial = 21; $fast_path_count_trial = 21;
	$slow_path_count_training = 14; $fast_path_count_training = 14;

	// The array to record the path for slow motion and fast motion separately
	$slow_path_trial = array(); $fast_path_trial = array();
	$slow_path_training = array(); $fast_path_training = array();

	// The array to record points number for each path，slow motion = 125, fast motion = 300;
	$path_point_count_slow_trial = array(); $path_point_count_fast_trial = array();
	$path_point_count_slow_training = array(); $path_point_count_fast_training = array();

	// Record the extreme X and Y for each path
	$x_min_slow_trial = array(); $y_min_slow_trial = array(); $x_min_fast_trial = array(); $y_min_fast_trial = array();
	$x_min_slow_training = array(); $y_min_slow_training = array(); $x_min_fast_training = array(); $y_min_fast_training = array();

	$x_max_slow_trial = array(); $y_max_slow_trial = array(); $x_max_fast_trial = array(); $y_max_fast_trial = array();
	$x_max_slow_training = array(); $y_max_slow_training = array(); $x_max_fast_training = array(); $y_max_fast_training = array();
	
	// The maximum x and y length allowed
	$x_cm = 18; $y_cm = 7;

	/* First, we get the url of each csv file */
	// Trials - slow motion
	for($i=0; $i<$slow_path_count_trial; $i++){ 
		$csv_url_slow_trial[$i] = "csv/slow/trial/". ($i+1) .".csv";
	}
	// Trials - fast motion
	for($i=0; $i<$fast_path_count_trial; $i++){ 
		$csv_url_fast_trial[$i] = "csv/fast/trial/". ($i+1) .".csv";
	}
	// Training - slow motion
	for($i=0; $i<$slow_path_count_training; $i++){ 
		$csv_url_slow_training[$i] = "csv/slow/training/". ($i+1) .".csv";
	}
	// Training - fast motion
	for($i=0; $i<$fast_path_count_training; $i++){ 
		$csv_url_fast_training[$i] = "csv/fast/training/". ($i+1) .".csv";
	}

	/* Second, we calculate the coordinate points of each path (Perhaps it can be omitted, since each slow path has 125 points, each fast path has 300 points) */
	// Calculate the points number for each path
	// Trials - slow motion - 125
	for($i=0; $i<$slow_path_count_trial; $i++){
		$path_point_count_slow_trial[$i] = count(getOnePath($csv_url_slow_trial[$i], false));
	}
	// Trials - fast motion - 300
	for($i=0; $i<$fast_path_count_trial; $i++){
		$path_point_count_fast_trial[$i] = count(getOnePath($csv_url_fast_trial[$i], true));
	}
	// Training - slow motion - 125
	for($i=0; $i<$slow_path_count_training; $i++){ 
		$path_point_count_slow_training[$i] = count(getOnePath($csv_url_slow_training[$i], false));
	}
	// Training - fast motion - 300
	for($i=0; $i<$fast_path_count_training; $i++){ 
		$path_point_count_fast_training[$i] = count(getOnePath($csv_url_fast_training[$i], true));
	}
	// print_r($path_point_count_slow_trial);
	// print_r($path_point_count_fast_trial);
	// print_r($path_point_count_slow_training);
	// print_r($path_point_count_fast_training);

	/* Thirdly, we get the path one by one and push them into arraries */
	/* Get the path from each csv file */
	// Slow - trial
	for($i=0; $i<$slow_path_count_trial; $i++){
		for($j=0; $j<$path_point_count_slow_trial[$i]; $j++){
	 		$slow_path_trial["path_".$i] = getOnePath($csv_url_slow_trial[$i], false);
	 	}
	}
	// Fast - trial
	for($i=0; $i<$fast_path_count_trial; $i++){
		for($j=0; $j<$path_point_count_fast_trial[$i]; $j++){
	 		$fast_path_trial["path_".$i] = getOnePath($csv_url_fast_trial[$i], true);
	 	}
	}
	// Slow - training
	for($i=0; $i<$slow_path_count_training; $i++){
		for($j=0; $j<$path_point_count_slow_training[$i]; $j++){
	 		$slow_path_training["path_".$i] = getOnePath($csv_url_slow_training[$i], false);
	 	}
	}
	// Fast - training
	for($i=0; $i<$fast_path_count_training; $i++){
		for($j=0; $j<$path_point_count_fast_training[$i]; $j++){
	 		$fast_path_training["path_".$i] = getOnePath($csv_url_fast_training[$i], true);
	 	}
	}
	// print_r($slow_path_trial)."  ,   ";
	// print_r($fast_path_trial)."  ,   ";
	// print_r($slow_path_training)."  ,   ";
	// print_r($fast_path_training)."  ,   ";

	/* Fourthly, find out the minimum x and minimum y value for each path separately, Remove excess white space, avoid of staying at a corner */
	// Trial - slow motion
	for($i=0; $i<$slow_path_count_trial; $i++){
		$x_min_slow_trial[$i] = getMinofColumn($slow_path_trial["path_".$i], "x");
		$y_min_slow_trial[$i] = getMinofColumn($slow_path_trial["path_".$i], "y");
	}
	// print_r($x_min_slow_trial);
	// print_r($y_min_slow_trial);

	for($i=0; $i<$slow_path_count_trial; $i++){
		for($j=0; $j<$path_point_count_slow_trial[$i]; $j++){ 
			$slow_path_trial["path_".$i]["coordinate_".$j]["x"] = $slow_path_trial["path_".$i]["coordinate_".$j]["x"] - $x_min_slow_trial[$i];
			$slow_path_trial["path_".$i]["coordinate_".$j]["y"] = $slow_path_trial["path_".$i]["coordinate_".$j]["y"] - $y_min_slow_trial[$i];
		}
	}
	// print_r($slow_path_trial);
	
	// Trial - fast motion
	for($i=0; $i<$fast_path_count_trial; $i++){
		$x_min_fast_trial[$i] = getMinofColumn($fast_path_trial["path_".$i], "x");
		$y_min_fast_trial[$i] = getMinofColumn($fast_path_trial["path_".$i], "y");
	}
	// print_r($x_min_fast_trial);
	// print_r($y_min_fast_trial);

	for($i=0; $i<$fast_path_count_trial; $i++){
		for($j=0; $j<$path_point_count_fast_trial[$i]; $j++){
	 		$fast_path_trial["path_".$i]["coordinate_".$j]["x"] = $fast_path_trial["path_".$i]["coordinate_".$j]["x"] - $x_min_fast_trial[$i];
	 		$fast_path_trial["path_".$i]["coordinate_".$j]["y"] = $fast_path_trial["path_".$i]["coordinate_".$j]["y"] - $y_min_fast_trial[$i];
	 	}
	}
	// print_r($fast_path_trial);

	// Training - slow motion
	for($i=0; $i<$slow_path_count_training; $i++){
		$x_min_slow_training[$i] = getMinofColumn($slow_path_training["path_".$i], "x");
		$y_min_slow_training[$i] = getMinofColumn($slow_path_training["path_".$i], "y");
	}
	// print_r($x_min_slow_training);
	// print_r($y_min_slow_training);

	for($i=0; $i<$slow_path_count_training; $i++){
		for($j=0; $j<$path_point_count_slow_training[$i]; $j++){ 
			$slow_path_training["path_".$i]["coordinate_".$j]["x"] = $slow_path_training["path_".$i]["coordinate_".$j]["x"] - $x_min_slow_training[$i];
			$slow_path_training["path_".$i]["coordinate_".$j]["y"] = $slow_path_training["path_".$i]["coordinate_".$j]["y"] - $y_min_slow_training[$i];
		}
	}
	// print_r($slow_path_training);

	// Training - fast motion
	for($i=0; $i<$fast_path_count_training; $i++){
		$x_min_fast_training[$i] = getMinofColumn($fast_path_training["path_".$i], "x");
		$y_min_fast_training[$i] = getMinofColumn($fast_path_training["path_".$i], "y");
	}
	// print_r($x_min_fast_training);
	// print_r($y_min_fast_training);

	for($i=0; $i<$fast_path_count_training; $i++){
		for($j=0; $j<$path_point_count_fast_training[$i]; $j++){
	 		$fast_path_training["path_".$i]["coordinate_".$j]["x"] = $fast_path_training["path_".$i]["coordinate_".$j]["x"] - $x_min_fast_training[$i];
	 		$fast_path_training["path_".$i]["coordinate_".$j]["y"] = $fast_path_training["path_".$i]["coordinate_".$j]["y"] - $y_min_fast_training[$i];
	 	}
	}
	// print_r($fast_path_training);

	// Fifthly, finding out the maximum x and maximum y value for each path separately, morphing the trajectories, to let them flatten on the setting canvas area L*W = 18*7, unit in CM, and make them 1.5 cm from the top and side edges  
	// Trial - slow motion
	for($i=0; $i<$slow_path_count_trial; $i++){
		$x_max_slow_trial[$i] = getMaxofColumn($slow_path_trial["path_".$i], "x");	
		$y_max_slow_trial[$i] = getMaxofColumn($slow_path_trial["path_".$i], "y");
	}
	// print_r($x_max_slow_trial);
	// print_r($y_max_slow_trial);
	
	for($i=0; $i<$slow_path_count_trial; $i++){
		for($j=0; $j<$path_point_count_slow_trial[$i]; $j++){ 
			$slow_path_trial["path_".$i]["coordinate_".$j]["x"] = $slow_path_trial["path_".$i]["coordinate_".$j]["x"] / $x_max_slow_trial[$i] * $x_cm + 1.5;
			$slow_path_trial["path_".$i]["coordinate_".$j]["y"] = $slow_path_trial["path_".$i]["coordinate_".$j]["y"] / $y_max_slow_trial[$i] * $y_cm + 1.5;
		}
	}
	// print_r($slow_path_trial);

	// Trial - fast motion
	for($i=0; $i<$fast_path_count_trial; $i++){
		$x_max_fast_trial[$i] = getMaxofColumn($fast_path_trial["path_".$i], "x");
		$y_max_fast_trial[$i] = getMaxofColumn($fast_path_trial["path_".$i], "y");
	}
	// print_r($x_max_fast_trial);
	// print_r($y_max_fast_trial);

	for($i=0; $i<$fast_path_count_trial; $i++){
		for($j=0; $j<$path_point_count_fast_trial[$i]; $j++){
	 		$fast_path_trial["path_".$i]["coordinate_".$j]["x"] = $fast_path_trial["path_".$i]["coordinate_".$j]["x"] / $x_max_fast_trial[$i] * $x_cm + 1.5;
	 		$fast_path_trial["path_".$i]["coordinate_".$j]["y"] = $fast_path_trial["path_".$i]["coordinate_".$j]["y"] / $y_max_fast_trial[$i] * $y_cm + 1.5;
	 	}
	}
	// print_r($fast_path_trial);
	
	// Training - slow motion
	for($i=0; $i<$slow_path_count_training; $i++){
		$x_max_slow_training[$i] = getMaxofColumn($slow_path_training["path_".$i], "x");	
		$y_max_slow_training[$i] = getMaxofColumn($slow_path_training["path_".$i], "y");
	}
	// print_r($x_max_slow_training);
	// print_r($y_max_slow_training);
	
	for($i=0; $i<$slow_path_count_training; $i++){
		for($j=0; $j<$path_point_count_slow_training[$i]; $j++){ 
			$slow_path_training["path_".$i]["coordinate_".$j]["x"] = $slow_path_training["path_".$i]["coordinate_".$j]["x"] / $x_max_slow_training[$i] * $x_cm + 1.5;
			$slow_path_training["path_".$i]["coordinate_".$j]["y"] = $slow_path_training["path_".$i]["coordinate_".$j]["y"] / $y_max_slow_training[$i] * $y_cm + 1.5;
		}
	}
	// print_r($slow_path_training);
	
	// Training - fast motion
	for($i=0; $i<$fast_path_count_training; $i++){
		$x_max_fast_training[$i] = getMaxofColumn($fast_path_training["path_".$i], "x");
		$y_max_fast_training[$i] = getMaxofColumn($fast_path_training["path_".$i], "y");
	}
	// print_r($x_max_fast_training);
	// print_r($y_max_fast_training);

	for($i=0; $i<$fast_path_count_training; $i++){
		for($j=0; $j<$path_point_count_fast_training[$i]; $j++){
	 		$fast_path_training["path_".$i]["coordinate_".$j]["x"] = $fast_path_training["path_".$i]["coordinate_".$j]["x"] / $x_max_fast_training[$i] * $x_cm + 1.5;
	 		$fast_path_training["path_".$i]["coordinate_".$j]["y"] = $fast_path_training["path_".$i]["coordinate_".$j]["y"] / $y_max_fast_training[$i] * $y_cm + 1.5;
	 	}
	}
	// print_r($fast_path_training);

	// Verify
	// for($i=0; $i<$fast_path_count_training; $i++){
	// 	$x_max_fast_training[$i] = getMaxofColumn($fast_path_training["path_".$i], "x");
	// 	$y_max_fast_training[$i] = getMaxofColumn($fast_path_training["path_".$i], "y");
	// }
	// print_r($x_max_fast_training);
	// print_r($y_max_fast_training);

	// for($i=0; $i<$fast_path_count_training; $i++){
	// 	$x_min_fast_training[$i] = getMinofColumn($fast_path_training["path_".$i], "x");
	// 	$y_min_fast_training[$i] = getMinofColumn($fast_path_training["path_".$i], "y");
	// }
	// print_r($x_min_fast_training);
	// print_r($y_min_fast_training);

	// for($i=0; $i<$slow_path_count_training; $i++){
	// 	$x_max_slow_training[$i] = getMaxofColumn($slow_path_training["path_".$i], "x");
	// 	$y_max_slow_training[$i] = getMaxofColumn($slow_path_training["path_".$i], "y");
	// }
	// print_r($x_max_slow_training);
	// print_r($y_max_slow_training);

	// for($i=0; $i<$slow_path_count_training; $i++){
	// 	$x_min_slow_training[$i] = getMinofColumn($slow_path_training["path_".$i], "x");
	// 	$y_min_slow_training[$i] = getMinofColumn($slow_path_training["path_".$i], "y");
	// }
	// print_r($x_min_slow_training);
	// print_r($y_min_slow_training);

	// for($i=0; $i<$slow_path_count_trial; $i++){
	// 	$x_min_slow_trial[$i] = getMinofColumn($slow_path_trial["path_".$i], "x");
	// 	$y_min_slow_trial[$i] = getMinofColumn($slow_path_trial["path_".$i], "y");
	// }
	// print_r($x_min_slow_trial);
	// print_r($y_min_slow_trial);
	
	// for($i=0; $i<$fast_path_count_trial; $i++){
	// 	$x_min_fast_trial[$i] = getMinofColumn($fast_path_trial["path_".$i], "x");
	// 	$y_min_fast_trial[$i] = getMinofColumn($fast_path_trial["path_".$i], "y");
	// }
	// print_r($x_min_fast_trial);
	// print_r($y_min_fast_trial);
	
	// for($i=0; $i<$slow_path_count_trial; $i++){
	// 	$x_max_slow_trial[$i] = getMaxofColumn($slow_path_trial["path_".$i], "x");	
	// 	$y_max_slow_trial[$i] = getMaxofColumn($slow_path_trial["path_".$i], "y");
	// }
	// print_r($x_max_slow_trial);
	// print_r($y_max_slow_trial);
	
	// for($i=0; $i<$fast_path_count_trial; $i++){
	// 	$x_max_fast_trial[$i] = getMaxofColumn($fast_path_trial["path_".$i], "x");
	// 	$y_max_fast_trial[$i] = getMaxofColumn($fast_path_trial["path_".$i], "y");
	// }
	// print_r($x_max_fast_trial);
	// print_r($y_max_fast_trial);


	/* Function to open the csv file, and then push the xs, ys, ys, ye for 1 path value into a 2D array */
	// @param $file_url is the path of aim file
	// @param $flag used to identify the slow path(false) or the fast path(true)
	function getOnePath($file_url, $flag){
		// Open the csv file
		$file=fopen($file_url,"r") or exit("Unable to open ". $file_url);
		// Array to record the coordinates for 1 path
		$arr=array();
		// Counter to calculate the array length
		$i=0;

		// Read the file line by line until the end of the file
		while(! feof($file)){
			// Read a single line from the opened file
			$line = fgets($file);

			// Separate the line content by commas, save them as an array of string
			$str = explode(",",$line);

			// When is the slow path
			if(! $flag){
				
				if($i != 0 && $i <= 125){
					// In the kind of key => value
					$arr["coordinate_".($i-1)]["x"] = floatval($str[4]);
					$arr["coordinate_".($i-1)]["y"] = floatval($str[5]);
				}

		    	$i++;

		    // When is the fast path
	    	}else{
		    	
				if($i != 0 && $i <= 300){
					// In the kind of key => value
					$arr["coordinate_".($i-1)]["x"] = floatval($str[4]);
					$arr["coordinate_".($i-1)]["y"] = floatval($str[5]);
				}

		    	$i++;
	    	}
	  
		}
		// Close the opened file
		fclose($file);

		return $arr;
	}

	// Function to calculate the max value of a column in the 2D array
	// @param $arr is the aim array
	// @param $column is the aim column 
	function getMaxofColumn($arr, $column){
		$max = -10000;
		foreach ($arr as $key => $val) {
	  		$max = max($max , $val[$column]);
		}
		return $max;
	}

	// Function to calculate the min value of a column in the 2D array
	// @param $arr is the aim array
	// @param $column is the aim column
	// @param $max is the max value of this column 
	function getMinofColumn($arr, $column){
		$min = 10000;
		foreach ($arr as $key => $val) {
		  	$min = min($min , $val[$column]);
		}
		return $min;
	}

	/* ---------------------------------------------------------------------------------------------------------------------------------------*/
	

	// number of conditions
	static $NUM_CONDITIONS = 4; // assuming fully crossed factors

	// The structure of your factors (used for naming condition parameters)
	// change only values (values to the right of => arrow operators) but not the keys (factor1, factor2, name, levels ...) because those are referenced elsewhere.
	// Add or remove factors as needed
	static $FACTORS = array("factor1", "factor2");
	static $FACTOR_LEVELS = array(
		"factor1" => array(
					"name" => "factor1", // change her the factor name
					"levels" => array("f1_level1", "f1_level2") // change these level names
				),
		"factor2" => array(
					"name" => "factor2", // change here the factor name
					"levels" => array("f2_level1", "f2_level2") // change here the level names
				)
	);

	
	static $EXCLUDE_RELOADERS = 1; // 1 = yes, 0 = no

/* Prolific specific data */

	// Completion URL. Fill in the one provided in their inteface
	static $COMPLETION_URL = "https://app.prolific.co/submissions/complete?cc=CFE2312B";
	
	// The information below is meant to be filled in automatically in the consent form. Adjust to the value corresponding to your experiment.
	// Experiment duration in minutes
	static $EXP_DURATION = 25;
	// Payment to participants in British Pound
	static $EXP_PAYMENT = 3.75;



	static $ATTENTION_QUESTION_OPTIONS = array(
											'malaria' => "People getting malaria",
											'tuberculosis' => "People getting tuberculosis",
											'cold' => "People getting the common cold",
											'HIV' => "People getting infected with HIV",
											'flu' => "People getting exposed to flu viruses"
										);
	// indicate which of the array keys has the right answer as value
	static $ATT_CORRECT_ANSWER = "cold";

	// column header for the individual log files (for the main results file the header row is automatically extracted from the data)
	$LOG_HEADER = 'participant_id,
							study_id,
							session_id,
							condition,
							' . $FACTOR_LEVELS["factor1"]["name"] . ',
							' . $FACTOR_LEVELS["factor2"]["name"] . ',
							timestamp_0,
							browser_name,
							browser_version,
							os,
							timestamp_1,
							timestamp_2,
							timestamp_3,
							timestamp_4,
							response,
							timestamp_5,
							timestamp_6,
							timestamp_7';
?>
