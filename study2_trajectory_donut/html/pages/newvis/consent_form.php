<div class="row">
    <div class="col">
      <h2>Consent Form</h2>
      <?php
        $warning_text='<strong>You need to scroll</strong> the page to see the remaining content.';
        include 'components/warning.php';
      ?>
      <div class="consent">
        <p>
          <b>Introduction</b><br>
          The purpose of the "Visualizing in Motion" study is to understand the accurate perception of data in moving charts.
        </p>

        <p>
          <b>Who can participate </b><br>
          Anyone who
          <ol>
            <li>without any sort of problem with moving images on screens,</li>
            <li>without motion sensitivity (vestibular disorder),</li>
            <li>with normal or corrected-to-normal vision (that means: you wear your glasses if you need them),</li>
            <li>of legal age (18 years in most countries),</li>
            <li>is using a screen of at least 13"3-inches in diagonal (almost all laptop and desktop screens meet our requirement but smartphones and most tablets will be too small).</li>
          </ol>
        </p>

        <p>
          <b>What is involved in the study</b><br>
          Participants will answer questions regarding the chart they see displayed on the screen. Participation requires completing a demographic questionnaire that follows this consent form.  
        </p>

        <p>
          <b>Voluntariness of participation</b><br>
          Participation is voluntary. Participants can stop the study at any time without giving a reason and their data will not be included in our analyses. Withdrawal of participation in the research project will be without consequences of any kind. 
        </p>

        <p>
          <b>Anonymity and Confidentiality</b><br>
          This study is anonymous - we do not require participants to provide any information that could identify them in the consent form nor in the questionnaire. This means that we will not be able to withdraw data from the analyses as soon as participants have submitted their answers. 
        </p>

        <p>
          <b>Risks/Discomforts</b><br>
          We believe there are no known risks associated with this research study, if you are not subject to motion sensitivity. If you are, we again ask you not to proceed with the study. However, as with any online related activity, the risk of a breach is always possible. We will minimize any risks by requiring no personally identifiable information such as names, addresses, phone numbers or email addresses and storing the raw data on password-protected computers. This study will be published in an academic research paper. The anonymized answers to the study will be made publicly available on an open science platform.  
        </p>

        <p>
          <b>Duration</b><br>
          The whole experiment will take the participants <?php echo $EXP_DURATION ?> minutes to complete.
        </p>

        <p>
          <b>Compensation</b><br>
          Pilot participants will receive no compensation for their participation. Participants recruited through a crowd-sourcing platform (Prolific) will be compensated <?php echo $EXP_PAYMENT?> GBP, as long as they: pass the calibration phase, pass the attention test trials, complete the study only one time, and do not reload the page after finishing the calibration phase. The amount was chosen based on the duration of a pilot study and will translate to 10.2 euros per hour (in Prolific this converts to £9.00 per hour).
        </p>

        <p>
          <b>Contact details</b><br>
          Please feel free to send us direct message from Prolific if you wish to know more about the study, or if you have any questions.
          <br/>
        <!--   <table>
            <tr>
              <td>Lijie Yao</td>
              <td>PhD Student</td>
              <td>AVIZ Research Team</td>
            </tr>
            <tr>
              <td>Inria Saclay Île-de-France</td>
              <td><a href="mailto:lijie.yao@inria.fr?cc=petra.isenberg@inria.fr;Anastasia.bezerianos@lri.fr&Subject=Visualization%20in%20Motion%20Experiment%20Question" target="_top">lijie.yao@inria.fr</td>
              <td>                  </td>
            </tr>
           
            <tr>
              <td>Petra Isenberg</td>
              <td>Research Scientist</td>
              <td>AVIZ Research Team</td>
            </tr>
            <tr>
              <td>Inria Saclay Île-de-France</td>
              <td><a href="mailto:petra.isenberg@inria.fr?cc=lijie.yao@inria.fr;Anastasia.bezerianos@lri.fr&Subject=Visualization%20in%20Motion%20Experiment%20Question" target="_top">petra.isenberg@inria.fr</td>
              <td>+33 01 74 85 42 90</td>
            </tr>

            <tr>
              <td>Anastasia Bezerianos</td>
              <td>Assistant Professor</td>
              <td>Interaction / ILDA Research Team</td>
            </tr>
            <tr>
              <td>LISN - Université Paris-Saclay Île-de-France&nbsp;&nbsp;&nbsp;&nbsp;</td>
              <td><a href="mailto:Anastasia.bezerianos@lri.fr?cc=petra.isenberg@inria.fr;lijie.yao@inria.fr&Subject=Visualization%20in%20Motion%20Experiment%20Question" target="_top">Anastasia.bezerianos@lri.fr&nbsp;&nbsp;&nbsp;&nbsp;</td>
              <td>+33 01 69 15 68 23</td>
            </tr>
          </table> -->
        </p>

        <p>
          <b>Please confirm that you wish to participate in this study</b><br>
          By clicking the “I agree. Start the calibration.” button below, you certify that you have read and understood the above information, that you meet the various selection criteria, that your questions have been answered satisfactorily and that you have been advised that you are free to withdraw from this research at any time, without consequence.
        </p>
      </div>
    </div>
</div>

<script type="text/javascript">
// Force the subject stays on this page for 10s, to read the sentences
// For formal study, waitTime_con = 4; when test, waitTime_con = 0 to avoid waiting
var waitTime_con = 4;

// Disable the next button, set the attributes
document.addEventListener("DOMContentLoaded", function(){
  let nextButton = document.getElementById("btn_<?php echo $id;?>");
  nextButton.style.border = "none";
  nextButton.style.background = "#EDEDED";
  nextButton.style.color = "#A3A3A3";
  nextButton.disabled = true;
});

// Excute per second, insert the new button value, to let the subject know how much time left
var callback_id_con = setInterval(function(){
  if(btn_wel){
                  if(waitTime_con == 0){
                    let nextButton = document.getElementById("btn_<?php echo $id;?>");
                    btn_<?php echo $id;?>.innerHTML = "I agree. Start the calibration.";
                    nextButton.style.background = "#006400";
                    nextButton.style.color = "#FFFFFF";
                    nextButton.disabled = false;
                  }else{
                    btn_<?php echo $id;?>.innerHTML = "You can click the button after "+ waitTime_con +"s";
                    waitTime_con--;
                  }
  }
}, 1000);

// Once the next button be clicked, clear the setInterval
$(document).on('click','#btn_<?php echo $id;?>',function(){
  clearInterval(callback_id_con);
  btn_cali[0] = true;

  // Push agreement into ajax
  measurements['consent_form_agreed'] = "true";
  measurements['color_condiiton'] = condition_color;
  measurements['speed_condiiton'] = random_speed_array[0] + " | " + random_speed_array[1] + " | " + random_speed_array[2] + " | " + random_speed_array[3];

  // Submit
  $.ajax({
      url: 'ajax/consent_form.php',
      type: 'POST',
      data: JSON.stringify(measurements),
      contentType: 'application/json',
      success: function (data) {
        // console.log(measurements);
      }
  }); 
});
</script>
