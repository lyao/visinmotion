<style type="text/css">
	#td_drawn{
		position: relative;
		text-align: center;
		margin: auto;
	}

	#td_canvas2{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#td_blank{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#td_afterimage1{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#td_afterimage2{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#td_afterimage3{
		position: relative;
		text-align: center;
		margin: auto;
	}
	#td_afterimage4{
		position: relative;
		text-align: center;
		margin: auto;
	}
</style>

<div id="row">
	<div id="col">
		<h1>Task Description</h1>

		<div class = "section">
			<p>Here is an explanation of the types of questions you will have to answer in the study.</p>
		</div>

		<div class = "section" id = "td_div_see" style="display: block;">
			<b>What you will see:</b><br>

			<div id = "td_div_1" style="display: block;">
				<ul id = "td_div_see_li_1" style="display: block;">
					<li>You will always see <span style = "font-weight: bold;">a donut chart composed of two slices</span>, one slice is in <span style = "font-weight: bold; color: #E90738;">red</span>, and the other slice in <span style = "font-weight: bold; color: #C3C1C1;">grey</span>. For simplicity, there will be no soccer ball in the middle and no labels around the donut.</li>
				</ul>
				<ul id = "td_div_see_li_2" style="display: none;">
					<li>You will see <span style = "font-weight: bold;">different percentages (%)</span> for your team (in <span style = "font-weight: bold; color: #E90738;">red</span>) in the experiment. For example, here are some of these:</li>
				</ul>

				<div id = "td_div" style="width: 1100px; text-align: center; display: none;">
					<canvas id = "td_canvas1"></canvas>
					<div style="width: 1100px; position: relative; margin-bottom: 20px; display: inline-flex;">
						<div id = "td_lab_1" style = "width: 220px; text-align: center;">10% for <span style = "font-weight: bold; color: #E90738;">red</span></div>
						<div id = "td_lab_2" style = "width: 220px; text-align: center;">30% for <span style = "font-weight: bold; color: #E90738;">red</span></div>
						<div id = "td_lab_3" style = "width: 220px; text-align: center;">50% for <span style = "font-weight: bold; color: #E90738;">red</span></div>
						<div id = "td_lab_4" style = "width: 220px; text-align: center;">70% for <span style = "font-weight: bold; color: #E90738;">red</span></div>
						<div id = "td_lab_5" style = "width: 220px; text-align: center;">90% for <span style = "font-weight: bold; color: #E90738;">red</span></div>
					</div>
				</div>
			</div>								

			<div id = "td_div_2" style="display: none;">
				<div id = "td_li_1" style="display: none;">
					<ul id = "td_div_see_li_3" style="display: none;">
						<li>In the experiment we will only show you the donut for <span style = "font-weight: bold;">a brief amount of time</span>.</li>
					</ul>

					<ul id = "td_div_see_li_4" style="display: none;">
						<li>We will first give you <span style = "font-weight: bold;">a start point</span> that will show you from where our donut chart will start, like the image below: </li>
					</ul>

					<ul id = "td_div_see_li_5" style="display: none;">
						<li>Then the donut chart will display after the start point.</li>
					</ul>

					<ul id = "td_div_see_li_6" style="display: none;">
						<li>Sometimes the donut chart will be <span id = "td_div_2_speed" style = "font-weight: bold;"></span> on the screen, like the one below (in this explanation, the donut will be displayed in a loop, but in the experiment, it will be displayed only 1 time):</li>
					</ul>
				</div>

				<div id = "td_li_2" style="display: none;">
					<ul id = "td_div_see_li_7" style="display: none;">
						<li>After the donut chart disappears you will see a very brief set of 4 images flashing on the screen. We call these 4 images <span style = "font-weight: bold;">"masking images"</span>. The purpose of the masking images is to make sure that you see the donut only for a specific amount of time. You <span style = "font-weight: bold;">do not need to pay attention to them or try to see them clearly</span>. They are very fast on purpose.</li>
					</ul>

					<ul id = "td_div_see_li_8" style="display: none;">
						<li>For the <span id = "td_div_2_movement" style = "font-weight: bold;"></span>, the masking images look like this (in this explanation, the masking images will be displayed in a loop, but in the experiment, they will be displayed only 1 time):</li>
					</ul>
				</div>

				<div id = "td_li_3" style="display: none;">
					<ul>
						<li>When the donut chart will be <span id = "td_div_2_speed_2" style = "font-weight: bold;"></span> on the screen, together it will look like this (in this explanation, the combination will be displayed in a loop, but in the experiment, it will be displayed only 1 time):</li>
					</ul>
				</div>

			</div>
		</div>

		<div class = "section" id = "td_div_do" style="display: none;">
			<b>What we want you to do:</b><br>

			<ul id = "td_div_do_li_1" style="display: none">
				<li>We want you to tell us <span style = "font-weight: bold;">what percentage (%)</span> your team (in <span style = "font-weight: bold; color: #E90738;">red</span>) had the ball. For example, you might estimate that your team (in <span style = "font-weight: bold; color: #E90738;">red</span>) currently has 1/3 of the time. Since 1/3 is almost 33%, you can say your team (in <span style = "font-weight: bold; color: #E90738;">red</span>) had the ball for 33% of the time. If you notice that the red slice looks a bit bigger than 33% you might give an 	answer of 40%.</li>
			</ul>
			<ul id = "td_div_do_li_2" style="display: none">
				<li>Although the exact answer here is 33% we just want you to make a <span style = "font-weight: bold;">quick estimate</span>. Reasonable answers here might range between 23-43%.</li>
			</ul>
			<ul id = "td_div_do_li_3" style="display: none;">
				<li>Enter your answer as <span style = "font-weight: bold;">a whole number</span> (for example: answer 40 but not 40.5)  in the text field and then click the “Done” button.</li>
			</ul>
		</div>
		<br>

		<div class = "section" id = "td_div_interface" style="display: none;">
				
			<div id= "td_visionfocus" style="display: none;">
				<img src="../html/img/gif/visionfocus.gif" id = "td_img_vf" style="width: 1000px;">
			</div>

			<div id= "td_stimuli1" style="display: none;">
				<img src="../html/img/gif/stimuli_linear_slow_motion.gif" id = "td_img_linear_slow" style="width: 1000px;">
			</div>

			<div id= "td_stimuli2" style="display: none;">
				<img src="../html/img/gif/stimuli_linear_fast_motion.gif" id = "td_img_linear_fast" style="width: 1000px; margin-bottom: 30px;">
			</div>

			<div id= "td_stimuli3" style="display: none;">
				<img src="../html/img/gif/stimuli_irregular_slow_motion.gif" id = "td_img_irregular_slow" style="width: 1000px;">
			</div>

			<div id= "td_stimuli4" style="display: none;">
				<img src="../html/img/gif/stimuli_irregular_fast_motion.gif" id = "td_img_irregular_fast" style="width: 1000px;">
			</div>

			<div id = "td_afterimage1" style="display: none;">
				<img src="../html/img/gif/afterimage.gif" id = "td_img_maskimage_linear" style="width: 1000px; margin-bottom: 10px;">
			</div>

			<div id = "td_afterimage2" style="display: none;">
				<img src="../html/img/gif/afterimage2.gif" id = "td_img_maskimage_irregular" style="width: 1000px; margin-bottom: 10px;">
			</div>
				
			<div id = "td_together1" style="display: none;">
				<img src="../html/img/gif/linear_slow_afterimage.gif" id = "td_img_linear_slow_maskimage" style="width: 1000px;">
			</div>
				
			<div id = "td_together2" style="display: none;">
				<img src="../html/img/gif/linear_fast_afterimage.gif" id = "td_img_linear_fast_maskimage" style="width: 1000px; margin-bottom: 30px;">
			</div>

			<div id = "td_together3" style="display: none;">
				<img src="../html/img/gif/irregular_slow_afterimage.gif" id = "td_img_irregular_slow_maskimage" style="width: 1000px;">
			</div>

			<div id = "td_together4" style="display: none;">
				<img src="../html/img/gif/irregular_fast_afterimage.gif" id = "td_img_irregular_fast_maskimage" style="width: 1000px;">
			</div>
	
		</div>

		<div class = "section" id = "td_div_interface_img" style = "display: none;">
			<p style="font-weight: bold;">What the interface screenshot looks like:</p>
			<div style="text-align: center; width: 1100px;">
				<img id = "interface_screenshot" src="../html/img/png/interface_screenshot.png" width = "400" height="160">

				<!-- when subject trys to click the interface -->
				<div class="modal fade" id="interface_screenshot_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
			
							<div class="modal-body" style="text-align: left;">
								<p>Hey, this is just a screenshot of our interface.</p>
								<p>This screenshot is to let you know what our interface looks like.</p>
								<p>You do not need to enter something or click the button on this screenshot.</p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-success" data-dismiss="modal">Close</button>			
							</div>
						</div><!-- /.modal-content -->
					</div><!-- /.modal -->
				</div>

			</div>		
		</div>

		<div class = "section" id = "td_btn" style="display: block;">
			<button id = "I_understand_btn" onclick="nextDiv(this)">You can click the button after 2s</button>
		</div>

		<div class = "section" id = "td_endarea" style = "display: none; margin-top: 50px;">
			<p style="font-weight: bold;">Please click the button below to proceed to the training: </p>
		</div>

	</div>
</div>

<script type="text/javascript">
// Flag to let the button disabled
var btn_tskd = new Array();
for(i=0; i<20; i++){
	// All flags == false
	btn_tskd[i] = false;
}
// Button active wait duration
var waitTime_tskd = 1;
// Callback id
var callback_id_tskd;
// Excute per second, insert the new button value, to let the subject know how much time left
function activeIUnderstand(flag){
	if(flag){
		if(waitTime_tskd == 0){
			// console.log("should change");
			let nextButton = document.getElementById("I_understand_btn");
			nextButton.innerHTML = "I understand";
			nextButton.style.background = "#006400";
            nextButton.style.color = "#FFFFFF";
            nextButton.disabled = false;
		}else{
			// console.log("if else");
			let nextButton = document.getElementById("I_understand_btn");
			nextButton.innerHTML = "You can click the button after "+ waitTime_tskd +"s";
            waitTime_tskd--;
		}

		// console.log("activeIUnderstand works");
		// console.log("waitTime_tskd = " + waitTime_tskd);

	}
}

function disableIUnderstand(){
	let nextButton = document.getElementById("I_understand_btn");
	nextButton.innerHTML = "You can click the button after 2s";
	nextButton.style.background = "#EDEDED";
    nextButton.style.color = "#A3A3A3";
    nextButton.disabled = true;
}

// Disable the next button
disableIUnderstand();

// If the subject click on the interface screenshot
document.getElementById("interface_screenshot").addEventListener("click", function(){$('#interface_screenshot_modal').modal();})
// Record the "I understand" button pressed times
var i_understand = -1;

// btn_tskd[i_understand + 1] = true;
// Active the first button
callback_id_tskd = setInterval("activeIUnderstand(btn_tskd[i_understand + 1])", 1000);

// // Record the callback_id
// var callback_id_td = new Array();

// // Parameters to record the canvas and context
// var canvas2_td, context2_td;
// // After image part, composed by 5 canvas, 4 free-drawn, 1 blank
// var canvas2_td_blank, context2_td_blank;
// var canvas2_td_afterimage_1;
// var canvas2_td_afterimage_2;
// var canvas2_td_afterimage_3;
// var canvas2_td_afterimage_4;

// Disable the next button for the moment, set the attributes
document.addEventListener("DOMContentLoaded", function(){
	let nextButton = document.getElementById("btn_<?php echo $id;?>");
	nextButton.style.border = "none";
	nextButton.style.background = "#006400";
	nextButton.style.color = "#FFFFFF";
	nextButton.style.display = 'none';
});

// // Parameter to record the time
// var initializationTime_td, lastFinishedTime_td, currentTime_td, deltaTime_td, oneLoopTime_1_td, oneLoopTime_2_td, donutStopTime;
// // Flag
// var donut_stop = false;

// function static(canvas, ctx, speed, percentage){
// 	// Get the current time
// 	currentTime_td = Date.now();
// 	// Calculate the delta time (ms) between last time call and present time call
// 	deltaTime_td = currentTime_td - lastFinishedTime_td;
// 	// Calculate the target section length and the other section length
// 	let t_l = percentage*barchart_length; 
// 	let o_l = barchart_length - t_l;

// 	if(deltaTime_td > 0 && deltaTime_td < 1600){
// 		// Clear the previous draw
// 		ctx.clearRect(0,0,canvas.width,canvas.height);
// 		// Redraw with the new coordinates
// 		barChart_horizontal.draw(speed, ctx, o_l, t_l);
				
// 		// It's time to display the afterimage or the blank
// 	}else if(deltaTime_td >= 1600 && deltaTime_td < 2600){
// 		// Clear the previous draw
// 		ctx.clearRect(0,0,canvas.width,canvas.height);
// 	}else{
// 		lastFinishedTime_td = Date.now();
// 	}

// 	callback_id_td[0] = requestAnimationFrame(function (){static(canvas, ctx, speed, percentage)});
// }

// function lowSpeed(canvas, ctx, speed, percentage){
// 	// Get the current time
// 	currentTime_td = Date.now();
// 	// Calculate the delta time (ms) between last time call and present time call
// 	deltaTime_td = currentTime_td - lastFinishedTime_td;
// 	oneLoopTime_1_td = currentTime_td - initializationTime_td;

// 	// Calculate the target section length and the other section length
// 	let t_l = percentage*barchart_length; 
// 	let o_l = barchart_length - t_l;
	
// 	// Clear the previous draw
// 	ctx.clearRect(0,0,canvas.width,canvas.height);
// 	// Redraw with the new coordinates
// 	barChart_horizontal.draw(speed, ctx, o_l, t_l);

// 	if ( barChart_horizontal.direction_low_speed == 1 ){
// 		barChart_horizontal.x_low_speed += speed*oneCM/1000*frequencyDelta*deltaTime_td;
// 	}else if ( barChart_horizontal.direction_low_speed == 0 ){
// 		barChart_horizontal.x_low_speed -= speed*oneCM/1000*frequencyDelta*deltaTime_td;
// 	} 

// 	// The range of X position
// 	let start_x = 1/4 * (barchart_travel_distance_pixel_needed_horizontal+barchart_length);
// 	let stop_x = 3/4 * barchart_travel_distance_pixel_needed_horizontal - 1/4 * barchart_length;

// 	// Set the right border to change the moving direction
// 	if ( barChart_horizontal.x_low_speed > stop_x){
// 		barChart_horizontal.x_low_speed = stop_x;
// 		barChart_horizontal.direction_low_speed = 0;
// 	}else if(barChart_horizontal.x_low_speed <= start_x){
// 		if(oneLoopTime_1_td>1600 && oneLoopTime_1_td<2600){
// 			// Clear the previous draw
// 			ctx.clearRect(0,0,canvas.width,canvas.height);
// 		}else if(oneLoopTime_1_td>2600){	
// 			barChart_horizontal.x_low_speed = start_x;
// 			barChart_horizontal.direction_low_speed = 1;
// 			initializationTime_td = Date.now();
// 		}
// 	}
// 	lastFinishedTime_td = Date.now();
// 	callback_id_td[1] = requestAnimationFrame(function (){lowSpeed(canvas, ctx, speed, percentage)});
// }

// function highSpeed(canvas, ctx, speed, percentage){
// 	// Get the current time
// 	currentTime_td = Date.now();
// 	// Calculate the delta time (ms) between last time call and present time call
// 	deltaTime_td = currentTime_td - lastFinishedTime_td;
// 	oneLoopTime_1_td = currentTime_td - initializationTime_td;

// 	// Calculate the target section length and the other section length
// 	let t_l = percentage*barchart_length; 
// 	let o_l = barchart_length - t_l;
	
// 	// Clear the previous draw
// 	ctx.clearRect(0,0,canvas.width,canvas.height);
// 	// Redraw with the new coordinates
// 	barChart_horizontal.draw(speed, ctx, o_l, t_l);

// 	if ( barChart_horizontal.direction_high_speed == 1 ){
// 		barChart_horizontal.x_high_speed += speed*oneCM/1000*frequencyDelta*deltaTime_td;
// 	}else if ( barChart_horizontal.direction_high_speed == 0 ){
// 		barChart_horizontal.x_high_speed -= speed*oneCM/1000*frequencyDelta*deltaTime_td;
// 	}

// 	// The range of X position
// 	let start_x = 0;
// 	let stop_x = barchart_travel_distance_pixel;   	

// 	// Set the right border to change the moving direction
// 	if ( barChart_horizontal.x_high_speed > stop_x){
// 		barChart_horizontal.x_high_speed = stop_x;
// 		barChart_horizontal.direction_high_speed = 0;
// 	}else if(barChart_horizontal.x_high_speed <= start_x){
// 		if(oneLoopTime_1_td>1600 && oneLoopTime_1_td<2600){
// 			// Clear the previous draw
// 			ctx.clearRect(0,0,canvas.width,canvas.height);
// 		}else if(oneLoopTime_1_td>2600){	
// 			barChart_horizontal.x_high_speed = start_x;
// 			barChart_horizontal.direction_high_speed = 1;
// 			initializationTime_td = Date.now();
// 		}
// 	}

// 	lastFinishedTime_td = Date.now();
// 	callback_id_td[2] = requestAnimationFrame(function (){highSpeed(canvas, ctx, speed, percentage)});
// }

function nextDiv(button){
	// For what you will see
	if(i_understand == -1){
		// Displaying the second sentence
		document.getElementById("td_div_see_li_2").style.display = 'block';
		// Displaying the percentage donut groups
		document.getElementById("td_div").style.display = 'block';

		// Clear the timer
		clearInterval(callback_id_tskd);
		// Deactive the button
		disableIUnderstand();
		// Reset the wait time
		waitTime_tskd = 1;
		// Reset the flag
		btn_tskd[i_understand + 1] = false;

		// i_understang slef plus one
		i_understand++;

		// Let the next flag active
		btn_tskd[i_understand + 1] = true;
		// Active the next button
		callback_id_tskd = setInterval("activeIUnderstand(btn_tskd[i_understand + 1])", 1000);

	}else if(i_understand == 0){
		// Percentage explanation hidden
		document.getElementById("td_div_1").style.display = 'none';
		document.getElementById("td_div_2").style.display = 'block';
		document.getElementById("td_li_1").style.display = 'block';
		document.getElementById("td_div_see_li_3").style.display = 'block';

		// Clear the timer
		clearInterval(callback_id_tskd);
		// Deactive the button
		disableIUnderstand();
		// Reset the wait time
		waitTime_tskd = 1;
		// Reset the flag
		btn_tskd[i_understand + 1] = false;

		// i_understang slef plus one
		i_understand++;

		// Let the next flag active
		btn_tskd[i_understand + 1] = true;
		// Active the next button
		callback_id_tskd = setInterval("activeIUnderstand(btn_tskd[i_understand + 1])", 1000);

	}else if(i_understand == 1){
		// Insert the discription
		// document.getElementById("td_div_2_speed").innerHTML = "moving at a low speed and do the linear motion";
		document.getElementById("td_div_see_li_4").style.display = 'block';
		// Display the vision focus gif
		document.getElementById("td_div_interface").style.display = 'block';
		document.getElementById("td_visionfocus").style.display = 'block';

		// // Clear the previous draw
		// context2_td.clearRect(0,0,canvas2_td.width,canvas2_td.height);
		// // resetcanvas
		// document.getElementById("td_afterimage4").style.display = 'none';
		// document.getElementById("td_afterimage1").style.display = 'none';
		// document.getElementById("td_afterimage2").style.display = 'none';
		// document.getElementById("td_afterimage3").style.display = 'none';
		// document.getElementById("td_canvas2").style.display = 'block';
		// document.getElementById("td_blank").style.display = 'none';

		// // Sketch the static donut
  //   	lastFinishedTime_td = Date.now();
		// callback_id_td[0] = requestAnimationFrame(function (){static(canvas2_td_blank, context2_td, 0, 0.25)});

		// Clear the timer
		clearInterval(callback_id_tskd);
		// Deactive the button
		disableIUnderstand();
		// Reset the wait time
		waitTime_tskd = 1;
		// Reset the flag
		btn_tskd[i_understand + 1] = false;

		// i_understang slef plus one
		i_understand++;

		// Let the next flag active
		btn_tskd[i_understand + 1] = true;
		// Active the next button
		callback_id_tskd = setInterval("activeIUnderstand(btn_tskd[i_understand + 1])", 1000);

	}else if(i_understand == 2){
		// // cancel animation
		// cancelAnimationFrame(callback_id_td[0]);
		// // Clear the previous draw
		// context2_td.clearRect(0,0,canvas2_td.width,canvas2_td.height);
		// // resetcanvas
		// document.getElementById("td_afterimage4").style.display = 'none';
		// document.getElementById("td_afterimage1").style.display = 'none';
		// document.getElementById("td_afterimage2").style.display = 'none';
		// document.getElementById("td_afterimage3").style.display = 'none';
		// document.getElementById("td_canvas2").style.display = 'block';
		// document.getElementById("td_blank").style.display = 'none';

		// Hide the vision focus gif
		document.getElementById("td_visionfocus").style.display = 'none';

		// Hide the first two sentences in "What you will see"
		document.getElementById("td_div_see_li_3").style.display = 'none';
		document.getElementById("td_div_see_li_4").style.display = 'none';

		// Go to bar chart display sectences
		document.getElementById("td_div_see_li_5").style.display = 'block';

		// Insert the discription
		// document.getElementById("td_div_2_speed").innerHTML = "moving at a low speed";

		// // Reset the bar positiion
		// barChart_horizontal.x_static = barchart_travel_distance_pixel/2;
		// barChart_horizontal.x_low_speed = 1/4 * (barchart_travel_distance_pixel_needed_horizontal+barchart_length);
		// barChart_horizontal.x_high_speed = 0;
		// barChart_horizontal.direction_low_speed = 1;
		// barChart_horizontal.direction_high_speed = 1;
		// // Sketch the donut at low speed
		// initializationTime_td = lastFinishedTime_td = Date.now();
		// callback_id_td[1] = requestAnimationFrame(function (){lowSpeed(canvas2_td_blank, context2_td, 15, 0.25)});

		// Clear the timer
		clearInterval(callback_id_tskd);
		// Deactive the button
		disableIUnderstand();
		// Reset the wait time
		waitTime_tskd = 1;
		// Reset the flag
		btn_tskd[i_understand + 1] = false;
		
		// i_understang slef plus one
		i_understand++;

		// Let the next flag active
		btn_tskd[i_understand + 1] = true;
		// Active the next button
		callback_id_tskd = setInterval("activeIUnderstand(btn_tskd[i_understand + 1])", 1000);

	}else if(i_understand == 3){
		// // cancel animation
		// cancelAnimationFrame(callback_id_td[1]);
		// // Clear the previous draw
		// context2_td.clearRect(0,0,canvas2_td.width,canvas2_td.height);
		// // resetcanvas
		// document.getElementById("td_afterimage4").style.display = 'none';
		// document.getElementById("td_afterimage1").style.display = 'none';
		// document.getElementById("td_afterimage2").style.display = 'none';
		// document.getElementById("td_afterimage3").style.display = 'none';
		// document.getElementById("td_canvas2").style.display = 'block';
		// document.getElementById("td_blank").style.display = 'none';

		// Insert the discription
		document.getElementById("td_div_2_speed").innerHTML = "moving at a low speed and in a linear motion";
		// Display the bar chart movement & speed description sentence
		document.getElementById("td_div_see_li_6").style.display = 'block';

		// Display the linear slow motion stimuli gif
		document.getElementById("td_stimuli1").style.display = 'block';

		// // Reset the bar positiion
		// barChart_horizontal.x_static = barchart_travel_distance_pixel/2;
		// barChart_horizontal.x_low_speed = 1/4 * (barchart_travel_distance_pixel_needed_horizontal+barchart_length);
		// barChart_horizontal.x_high_speed = 0;
		// barChart_horizontal.direction_low_speed = 1;
		// barChart_horizontal.direction_high_speed = 1;

		// // Sketch the donut at high speed
		// initializationTime_td = lastFinishedTime_td = Date.now();
		// callback_id_td[2] = requestAnimationFrame(function (){highSpeed(canvas2_td_blank, context2_td, 30, 0.25)});

		// Clear the timer
		clearInterval(callback_id_tskd);
		// Deactive the button
		disableIUnderstand();
		// Reset the wait time
		waitTime_tskd = 1;
		// Reset the flag
		btn_tskd[i_understand + 1] = false;
		
		// i_understang slef plus one
		i_understand++;

		// Let the next flag active
		btn_tskd[i_understand + 1] = true;
		// Active the next button
		callback_id_tskd = setInterval("activeIUnderstand(btn_tskd[i_understand + 1])", 1000);

	// The afterimage block display
	}else if(i_understand == 4){
		// // cancel animation
		// cancelAnimationFrame(callback_id_td[2]);
		// // Clear the previous draw
		// context2_td.clearRect(0,0,canvas2_td.width,canvas2_td.height);

		// // Motion explanation list hidden
		// document.getElementById("td_li_1").style.display = 'none';
		// // Afterimage explanation list displays
		// document.getElementById("td_li_2").style.display = 'block';

		// // resetcanvas, afterimage
		// document.getElementById("td_afterimage4").style.display = 'none';
		// document.getElementById("td_afterimage1").style.display = 'block';
		// document.getElementById("td_afterimage2").style.display = 'none';
		// document.getElementById("td_afterimage3").style.display = 'none';
		// document.getElementById("td_canvas2").style.display = 'none';
		// document.getElementById("td_blank").style.display = 'none';

		// Insert the discription
		document.getElementById("td_div_2_speed").innerHTML = "moving at a low speed and in an irregular movement";

		// Hide the linear slow motion stimuli gif
		document.getElementById("td_stimuli1").style.display = 'none';
		// Display the irregular slow motion stimuli gif
		document.getElementById("td_stimuli3").style.display = 'block';

		// Clear the timer
		clearInterval(callback_id_tskd);
		// Deactive the button
		disableIUnderstand();
		// Reset the wait time
		waitTime_tskd = 1;
		// Reset the flag
		btn_tskd[i_understand + 1] = false;

		// i_understang slef plus one
		i_understand++;

		// Let the next flag active
		btn_tskd[i_understand + 1] = true;
		// Active the next button
		callback_id_tskd = setInterval("activeIUnderstand(btn_tskd[i_understand + 1])", 1000);

	// Together = donut chart + afterimage block
	}else if(i_understand == 5){

		// // Afterimage explanation list hidden
		// document.getElementById("td_li_2").style.display = 'none';
		// // Insert the discription
		// document.getElementById("td_div_2_speed_2").innerHTML = "staying static";
		// // Together explanation list displays
		// document.getElementById("td_li_3").style.display = 'block';

		// // resetcanvas, static afterimage
		// document.getElementById("td_afterimage4").style.display = 'none';
		// document.getElementById("td_afterimage1").style.display = 'none';
		// document.getElementById("td_afterimage2").style.display = 'block';
		// document.getElementById("td_afterimage3").style.display = 'none';
		// document.getElementById("td_canvas2").style.display = 'none';
		// document.getElementById("td_blank").style.display = 'none';

		// Insert the discription
		document.getElementById("td_div_2_speed").innerHTML = "moving at a high speed and in a linear motion";
		// Hide the irregular slow motion stimuli gif
		document.getElementById("td_stimuli3").style.display = 'none';
		// Display the linear fast motion stimuli gif
		document.getElementById("td_stimuli2").style.display = 'block';

		// Clear the timer
		clearInterval(callback_id_tskd);
		// Deactive the button
		disableIUnderstand();
		// Reset the wait time
		waitTime_tskd = 1;
		// Reset the flag
		btn_tskd[i_understand + 1] = false;

		// i_understang slef plus one
		i_understand++;

		// Let the next flag active
		btn_tskd[i_understand + 1] = true;
		// Active the next button
		callback_id_tskd = setInterval("activeIUnderstand(btn_tskd[i_understand + 1])", 1000);

	}else if(i_understand == 6){

		// // Insert the discription
		// document.getElementById("td_div_2_speed_2").innerHTML = "moving at a low speed";

		// // resetcanvas, low speed afterimage
		// document.getElementById("td_afterimage4").style.display = 'none';
		// document.getElementById("td_afterimage1").style.display = 'none';
		// document.getElementById("td_afterimage2").style.display = 'none';
		// document.getElementById("td_afterimage3").style.display = 'block';
		// document.getElementById("td_canvas2").style.display = 'none';
		// document.getElementById("td_blank").style.display = 'none';

		// Insert the discription
		document.getElementById("td_div_2_speed").innerHTML = "moving at a high speed and in an irregular movement";
		// Hide the linear fast motion stimuli gif
		document.getElementById("td_stimuli2").style.display = 'none';
		// Display the irregular fast motion stimuli gif
		document.getElementById("td_stimuli4").style.display = 'block';

		// Clear the timer
		clearInterval(callback_id_tskd);
		// Deactive the button
		disableIUnderstand();
		// Reset the wait time
		waitTime_tskd = 1;
		// Reset the flag
		btn_tskd[i_understand + 1] = false;

		// i_understang slef plus one
		i_understand++;

		// Let the next flag active
		btn_tskd[i_understand + 1] = true;
		// Active the next button
		callback_id_tskd = setInterval("activeIUnderstand(btn_tskd[i_understand + 1])", 1000);

	}else if(i_understand == 7){
		// // Insert the discription
		// document.getElementById("td_div_2_speed_2").innerHTML = "moving at a high speed";
		
		// // resetcanvas, high speed afterimage
		// document.getElementById("td_afterimage4").style.display = 'block';
		// document.getElementById("td_afterimage1").style.display = 'none';
		// document.getElementById("td_afterimage2").style.display = 'none';
		// document.getElementById("td_afterimage3").style.display = 'none';
		// document.getElementById("td_canvas2").style.display = 'none';
		// document.getElementById("td_blank").style.display = 'none';

		// Hide the irregular fast motion stimuli gif
		document.getElementById("td_stimuli4").style.display = 'none';
		// Hide the bar chart movement & speed description sentence
		document.getElementById("td_li_1").style.display = 'none';
		// Display the masking images description sentences
		document.getElementById("td_li_2").style.display = 'block';
		// Display the masking images description first sentence
		document.getElementById("td_div_see_li_7").style.display = 'block';

		// Clear the timer
		clearInterval(callback_id_tskd);
		// Deactive the button
		disableIUnderstand();
		// Reset the wait time
		waitTime_tskd = 1;
		// Reset the flag
		btn_tskd[i_understand + 1] = false;

		// i_understang slef plus one
		i_understand++;

		// Let the next flag active
		btn_tskd[i_understand + 1] = true;
		// Active the next button
		callback_id_tskd = setInterval("activeIUnderstand(btn_tskd[i_understand + 1])", 1000);

	// For what we want you to do
	}else if(i_understand == 8){
		// // cancel animation
		// cancelAnimationFrame(callback_id_td[6]);
		// // Clear the previous draw
		// context2_td.clearRect(0,0,canvas2_td.width,canvas2_td.height);

		// document.getElementById("td_drawn").style.display = 'none';
		// document.getElementById("td_div_interface").style.display = 'none';
		// document.getElementById("td_div_see").style.display = 'none';
		// document.getElementById("td_div_do").style.display = 'block';
		// document.getElementById("td_div_do_li_1").style.display = 'block';

		// Insert the discription
		document.getElementById("td_div_2_movement").innerHTML = "linear motion";
		// Display the masking images description second sentence
		document.getElementById("td_div_see_li_8").style.display = 'block';
		// Display the linear motion afterimage gif
		document.getElementById("td_afterimage1").style.display = 'block';

		// Clear the timer
		clearInterval(callback_id_tskd);
		// Deactive the button
		disableIUnderstand();
		// Reset the wait time
		waitTime_tskd = 1;
		// Reset the flag
		btn_tskd[i_understand + 1] = false;

		// i_understang slef plus one
		i_understand++;

		// Let the next flag active
		btn_tskd[i_understand + 1] = true;
		// Active the next button
		callback_id_tskd = setInterval("activeIUnderstand(btn_tskd[i_understand + 1])", 1000);

	}else if(i_understand == 9){
		// Insert the discription
		document.getElementById("td_div_2_movement").innerHTML = "irregular movement";
		// Hide the linear motion afterimage gif
		document.getElementById("td_afterimage1").style.display = 'none';
		// Display the irregular motion afterimage gif
		document.getElementById("td_afterimage2").style.display = 'block';

		// Clear the timer
		clearInterval(callback_id_tskd);
		// Deactive the button
		disableIUnderstand();
		// Reset the wait time
		waitTime_tskd = 1;
		// Reset the flag
		btn_tskd[i_understand + 1] = false;

		// i_understang slef plus one
		i_understand++;

		// Let the next flag active
		btn_tskd[i_understand + 1] = true;
		// Active the next button
		callback_id_tskd = setInterval("activeIUnderstand(btn_tskd[i_understand + 1])", 1000);

	}else if(i_understand == 10){
		// Hide the irregular motion afterimage gif
		document.getElementById("td_afterimage2").style.display = 'none';
		// Hide the masking image sentences
		document.getElementById("td_li_2").style.display = 'none';
		// Insert the discription
		document.getElementById("td_div_2_speed_2").innerHTML = "moving at a low speed and in a linear motion";
		// Display the together  description sentences
		document.getElementById("td_li_3").style.display = 'block';
		// Display the linear slow motion afterimage gif
		document.getElementById("td_together1").style.display = 'block';

		// Clear the timer
		clearInterval(callback_id_tskd);
		// Deactive the button
		disableIUnderstand();
		// Reset the wait time
		waitTime_tskd = 1;
		// Reset the flag
		btn_tskd[i_understand + 1] = false;

		// i_understang slef plus one
		i_understand++;

		// Let the next flag active
		btn_tskd[i_understand + 1] = true;
		// Active the next button
		callback_id_tskd = setInterval("activeIUnderstand(btn_tskd[i_understand + 1])", 1000);

	}else if(i_understand == 11){
		// Insert the discription
		document.getElementById("td_div_2_speed_2").innerHTML = "moving at a low speed and in an irregular movement";
		// Hide the linear slow motion afterimage gif
		document.getElementById("td_together1").style.display = 'none';
		// Display the irregular slow motion afterimage gif
		document.getElementById("td_together3").style.display = 'block';

		// Clear the timer
		clearInterval(callback_id_tskd);
		// Deactive the button
		disableIUnderstand();
		// Reset the wait time
		waitTime_tskd = 1;
		// Reset the flag
		btn_tskd[i_understand + 1] = false;

		// i_understang slef plus one
		i_understand++;

		// Let the next flag active
		btn_tskd[i_understand + 1] = true;
		// Active the next button
		callback_id_tskd = setInterval("activeIUnderstand(btn_tskd[i_understand + 1])", 1000);

	}else if(i_understand == 12){
		// Insert the discription
		document.getElementById("td_div_2_speed_2").innerHTML = "moving at a high speed and in a linear motion";
		// Hide the irregular slow motion afterimage gif
		document.getElementById("td_together3").style.display = 'none';
		// Display the linear fast motion afterimage gif
		document.getElementById("td_together2").style.display = 'block';

		// Clear the timer
		clearInterval(callback_id_tskd);
		// Deactive the button
		disableIUnderstand();
		// Reset the wait time
		waitTime_tskd = 1;
		// Reset the flag
		btn_tskd[i_understand + 1] = false;

		// i_understang slef plus one
		i_understand++;

		// Let the next flag active
		btn_tskd[i_understand + 1] = true;
		// Active the next button
		callback_id_tskd = setInterval("activeIUnderstand(btn_tskd[i_understand + 1])", 1000);

	}else if(i_understand == 13){
		// Insert the discription
		document.getElementById("td_div_2_speed_2").innerHTML = "moving at a high speed and in an irregular movement";
		// Hide the linear fast motion afterimage gif
		document.getElementById("td_together2").style.display = 'none';
		// Display the irregular fast motion afterimage gif
		document.getElementById("td_together4").style.display = 'block';

		// Clear the timer
		clearInterval(callback_id_tskd);
		// Deactive the button
		disableIUnderstand();
		// Reset the wait time
		waitTime_tskd = 1;
		// Reset the flag
		btn_tskd[i_understand + 1] = false;

		// i_understang slef plus one
		i_understand++;

		// Let the next flag active
		btn_tskd[i_understand + 1] = true;
		// Active the next button
		callback_id_tskd = setInterval("activeIUnderstand(btn_tskd[i_understand + 1])", 1000);

	// End of what you will see, start of what we want you to do
	}else if(i_understand == 14){
		// Hide the irregular fast motion afterimage gif
		document.getElementById("td_together4").style.display = 'none';
		// Hide the gifs div
		document.getElementById("td_div_interface").style.display = 'none';
		// Hide the second section
		document.getElementById("td_div_see").style.display = 'none';
		// Display the third section
		document.getElementById("td_div_do").style.display = 'block';
		// Display the third section, first setence
		document.getElementById("td_div_do_li_1").style.display = 'block';

		// Clear the timer
		clearInterval(callback_id_tskd);
		// Deactive the button
		disableIUnderstand();
		// Reset the wait time
		waitTime_tskd = 1;
		// Reset the flag
		btn_tskd[i_understand + 1] = false;

		// i_understang slef plus one
		i_understand++;

		// Let the next flag active
		btn_tskd[i_understand + 1] = true;
		// Active the next button
		callback_id_tskd = setInterval("activeIUnderstand(btn_tskd[i_understand + 1])", 1000);

	}else if(i_understand == 15){
		// Display the third section, second setence
		document.getElementById("td_div_do_li_2").style.display = 'block';

		// Clear the timer
		clearInterval(callback_id_tskd);
		// Deactive the button
		disableIUnderstand();
		// Reset the wait time
		waitTime_tskd = 1;
		// Reset the flag
		btn_tskd[i_understand + 1] = false;

		// i_understang slef plus one
		i_understand++;

		// Let the next flag active
		btn_tskd[i_understand + 1] = true;
		// Active the next button
		callback_id_tskd = setInterval("activeIUnderstand(btn_tskd[i_understand + 1])", 1000);

	}else if(i_understand == 16){
		// Display the third section, second setence
		document.getElementById("td_div_do_li_3").style.display = 'block';

		// Clear the timer
		clearInterval(callback_id_tskd);
		// Deactive the button
		disableIUnderstand();
		// Reset the wait time
		waitTime_tskd = 1;
		// Reset the flag
		btn_tskd[i_understand + 1] = false;

		// i_understang slef plus one
		i_understand++;

		// Let the next flag active
		btn_tskd[i_understand + 1] = true;
		// Active the next button
		callback_id_tskd = setInterval("activeIUnderstand(btn_tskd[i_understand + 1])", 1000);

	// The interface part
	}else if(i_understand == 17){
		document.getElementById("td_div_interface_img").style.display = 'block';

		// Clear the timer
		clearInterval(callback_id_tskd);
		// Deactive the button
		disableIUnderstand();
		// Reset the wait time
		waitTime_tskd = 1;
		// Reset the flag
		btn_tskd[i_understand + 1] = false;

		// i_understang slef plus one
		i_understand++;

		// Let the next flag active
		btn_tskd[i_understand + 1] = true;
		// Active the next button
		callback_id_tskd = setInterval("activeIUnderstand(btn_tskd[i_understand + 1])", 1000);

	}else if(i_understand == 18){
		// Clear the timer
		clearInterval(callback_id_tskd);
		// Reset the flag
		btn_tskd[i_understand + 1] = false;
		
		// Hidden the "I understand" button
		document.getElementById("td_btn").style.display = 'none';

		// endarea
		document.getElementById("td_endarea").style.display = 'block';
		let nextButton = document.getElementById("btn_<?php echo $id;?>");
		nextButton.style.display = 'block';
	}
}

// Once the "go to training" button be clicked, record the current time
$(document).on('click','#btn_<?php echo $id;?>',function(){
	startTraining_time = Date.now();
});
</script>